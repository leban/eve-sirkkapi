
var Sirkka = function() {
    var self = this;

    //Lookup('sirkuslajit')
    Lookup('ryhma')
    Lookup('venue')

    // teos by group

    var shows = new Array();
    var showid = new Object();

    var group = '?where=group_id=='+UGID

    Api('GET','teos'+group, null, function(data){

        console.log(data)

            $.each( data._items, function ( index, item ) {
                shows.push( item.nimi );
                showid[item.nimi] = item._id
        } );
        

        $(document).on('focusin','input.teos',
        function() {
            $('input.teos').typeahead( { 
            source:shows,
            updater: function(item) {
                return this.$element.val().replace(/[^,]*$/,'')+item+', ';
            },
            matcher: function (item) {
              var tquery = extractor(this.query);
              if(!tquery) return false;
              return ~item.toLowerCase().indexOf(tquery.toLowerCase())
            },
            highlighter: function (item) {
              var query = extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
              return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
                return '<strong>' + match + '</strong>'
                })
              }
             }                                
           );
        })
        .on('change','input.teos', function() {
             var res = $(this).val();
             $('input.teos_id').val(_val['showid'][res]).change();

             });

    })


    var Type = function(name) {
        this.typeName = name;
    };

    availableType = ko.observableArray([
        new Type("Kotimaan esitys"),
        new Type("Kv esitys"),
        new Type("Kotimainen vieras"),
        new Type("Ulkom. vieras"),
        new Type("Yleisötyö"),
        new Type("Yhteisösrkus"),
        ])

        var now = $.formatDateTime("d. m. yy", new Date());

        self.pvm = ko.observable(now)
        self.aika = ko.observable()
        self.userid = ko.observable(UID)
        self.www = ko.observable()
        self.main_id = ko.observable()

        self.teos = {
            // user_id : ko.observable(),
            nimi_fi: ko.observable(),
            nimi_en: ko.observable(),
            _id : ko.observable(),
            group_id : ko.observable(),
            sirkuslajit : ko.observable(),
            markkinointi_fi : ko.observable(),
            markkinointi_en : ko.observable(),
            teoksenkuvaus_fi: ko.observable(),
            teoksenkuvaus_en: ko.observable(),
            kuvaus_fi : ko.observable(),
            kuvaus_en : ko.observable(),
            koreografi: ko.observable(),
            esittaja : ko.observable(),
            vapaatxt : ko.observable(),
            julkaistu : ko.observable(),
            kuva : ko.observable(),
            kieli : ko.observable(),
            muu : ko.observable(),
            kokoperhe : ko.observable(),
            sensiilta : ko.observable(),
            muumika : ko.observable(),
            muutjasenet : ko.observable(),
            perinteinen : ko.observable(),
            sirkka : ko.observable(),
            esitystyyppi : ko.observable(),
            kantaesitys : ko.observable(),
            musiikki : ko.observable(),
            kesto : ko.observable(),
            valiaika : ko.observable(),
            esitys : ko.observable(),
            kuvaaja : ko.observable(),
            tyoryhma : ko.observable(),
            nykysirkus : ko.observable(),
            ulko : ko.observable(),
            workshop : ko.observable(),
        }

        self.venue_id = ko.observable()
        self.venue = {
            url : ko.observable(),
            kaupunki : ko.observable(),
            paikka : ko.observable(),
            postinro : ko.observable(),
            osoite: ko.observable(),
            main_id : ko.observable(),
            maa : ko.observable(),
            ensiilta : ko.observable(),
            _id : ko.observable(),
            pvm : ko.observable(),
        }

        self.group_id = ko.observable(UGID)

        self.ryhma = {
            tyylilaji : ko.observable(),
            kuvassa : ko.observable(),
            julkaistu : ko.observable(),
            kuva : ko.observable(),
            _id : ko.observable(),
            muu : ko.observable(),
            nimi : ko.observable(),
            kokoperhe : ko.observable(),
            muumika : ko.observable(),
            verkkosivu : ko.observable(),
            kuvaaja : ko.observable(),
            perinteinen : ko.observable(),
            kuvaus : ko.observable(),
            yhteyshenkilo : ko.observable(),
            jasenet : ko.observable(),
            tuotannot : ko.observable(),
            perustettu : ko.observable(),
            muitatietoja : ko.observable(),
            userid : ko.observable(),
            loppunut : ko.observable(),
            organisaatiomuoto : ko.observable(),
            nykysirkus : ko.observable(),
        }


    if(ID) {

        // edit

        Api('GET', 'esitys/'+ID+embed, null, function(data) {
                    var showdate = $.formatDateTime("d. m. yy hh:ii", new Date(data.pvm*1000));
                    self.pvm( showdate )
                    self.aika(data.aika)
                    self.userid(UID)
                    self.www(data.www)
//                    self.main_id.nimi(data.main_id.nimi)
                    if(data.main_id != null) {
                            self.teos.nimi_fi = ko.observable(data.main_id.nimi)
                            self.teos.nimi_en = ko.observable(data.main_id.nimi_en)
                            self.teos.sirkuslajit = ko.observable(data.main_id.sirkuslajit)
                            var mark = JSON.parse(data.main_id.markkinointi)
                            self.teos.markkinointi_fi(mark.fi)
                            self.teos.esittaja(data.main_id.esittaja)
                            txt = JSON.parse(data.main_id.vapaatxt)
                            self.teos.vapaatxt(txt.fi)
                            self.teos.julkaistu(data.main_id.julkaistu)
                            self.teos.kuva(data.main_id.kuva)
                            var kieli = JSON.parse(data.main_id.kieli)
                            self.teos.kieli(kieli.fi)
                            self.teos.muu(data.main_id.muu)
                            //self.main_id.user_id(data.main_id.user_id)
                            self.teos.kokoperhe(data.main_id.kokoperhe)
                            self.teos.sensiilta(data.main_id.sensiilta)
                            var muu = JSON.parse(data.main_id.muumika)
                            self.teos.muumika(muu.fi)
                            self.teos.muutjasenet(data.main_id.muutjasenet)
                            self.teos.koreografi(data.main_id.koreografi)
                            self.teos.perinteinen(data.main_id.perinteinen)
                            self.teos.sirkka(data.main_id.sirkka)
                            self.teos.esitystyyppi(data.main_id.esitystyyppi)
                            self.teos.kantaesitys(data.main_id.kantaesitys)
                            self.teos.musiikki(data.main_id.musiikki)
                            self.teos.kesto(data.main_id.kesto)
                            var kuvaus = JSON.parse(data.main_id.teoksenkuvaus)
                            self.teos.teoksenkuvaus_fi(kuvaus.fi)
                            self.teos.teoksenkuvaus_en(kuvaus.en)
                            self.teos.valiaika(data.main_id.valiaika)
                            //self.main_id._id(data.main_id._id)
                            self.teos.esitys(data.main_id.esitys)
                            self.teos.teoksenkuvaus_en(kuvaus.en)
                            self.teos.markkinointi_en(mark.en)
                            self.teos.kuvaaja(data.main_id.kuvaaja)
                            self.teos.tyoryhma(data.main_id.tyoryhma)
                            self.teos.nykysirkus(data.main_id.nykysirkus)
                            //self.main_id.group_id(data.main_id.group_id)
                            self.teos.ulko(data.main_id.ulko)
                            self.teos.workshop(data.main_id.workshop)
                    }

                    if (data.venue_id != null) {
                            self.venue_id = data.venue_id._id
                            self.venue.url(data.venue_id.url)
                            self.venue.kaupunki(data.venue_id.kaupunki)
                            self.venue.paikka(data.venue_id.paikka)
                            self.venue.postinro(data.venue_id.postinro)
                            self.venue.osoite(data.venue_id.osoite)
                            self.venue.main_id(data.venue_id.main_id)
                            self.venue.maa(data.venue_id.maa)
                            self.venue._id(data.venue_id._id)
                            self.venue.pvm(data.venue_id.pvm)
                        }
                    
                    if(data.group_id != null) {
                        self.group_id = data.group_id
                        self.ryhma.tyylilaji(data.group_id.tyylilaji)
                        self.ryhma.kuvassa(data.group_id.kuvassa)
                        self.ryhma.julkaistu(data.group_id.julkaistu)
                        self.ryhma.kuva(data.group_id.kuva)
                        self.ryhma._id(data.group_id.id)
                        self.ryhma.muu(data.group_id.muu)
                        self.ryhma.nimi(data.group_id.nimi)
                        self.ryhma.kokoperhe(data.group_id.kokoperhe)
                        self.ryhma.muumika(data.group_id.muumika)
                        self.ryhma.verkkosivu(data.group_id.verkkosivu)
                        self.ryhma.kuvaaja(data.group_id.kuvaaja)
                        self.ryhma.perinteinen(data.group_id.perinteinen)
                        self.ryhma.kuvaus(data.group_id.kuvaus)
                        self.ryhma.yhteyshenkilo(data.group_id.yhteyshenkilo)
                        self.ryhma.jasenet(data.group_id.jasenet)
                        self.ryhma.tuotannot(data.group_id.tuotannot)
                        self.ryhma.perustettu(data.group_id.perustettu)
                        self.ryhma.muitatietoja(data.group_id.muitatietoja)
                        self.ryhma.userid(data.group_id.userid)
                        self.ryhma.loppunut(data.group_id.loppunut)
                        self.ryhma.organisaatiomuoto(data.group_id.organisaatiomuoto)
                        self.ryhma.nykysirkus(data.group_id.nykysirkus)
                        //self.group_id._id(data.group_id._id)

                    } 

        } )
    }
    

    self.saveRow = function(row) {

        var save = {
            pvm: self.pvm(),
            aika: self.aika(),
            www: self.www(),
            main_id: self.main_id(),
            venue_id: self.venue_id(),
            group_id: self.group_id()
        }

        if(ID) {
            Api('PATCH', 'teos/'+ID, save, function() {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)

            })

        } else {
            Api('POST', 'teos/', save, function(data) {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.href = '/teos.html?ID='+data._id;
                }, 2000)

            })
        }

    }



    self.addSirkka = function() {

    }

 
    self.cloneSirkka = function() {
        }

    self.removeSirkka = function(data) {
    }
 

   self.addTaho = function() {
        
   }

    self.saveSirkka = function () {

            console.log(ko.toJSON(self.uusi))

            // _ryhma = {
            //     nimi: val.nimi,
            //     oranisaatiomuoto: val.group_id.oranisaatiomuoto,
            //     yhteyshenkilo:val.group_id.yhteyshenkilo,
            //     muumika: val.group_id.muumika,
            // }

            Api('POST', 'ryhma/', self.uusi, function() {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.href = 'users.html';
                }, 2000)
            } )
    
    }

    self.save = function(form) {


    };
};



viewModel = new Sirkka();

ko.applyBindings(viewModel, $('#sirkka')[0]);