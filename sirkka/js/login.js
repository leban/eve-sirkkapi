
var Login = function() {

    ko.virtualElements.allowedBindings.group_id = true;


    var self = this;
    var pass = '';
    var md5pass = '';
    var salt = '';

    //getAPI(baseURL+embed,'GET');

    self.login = ko.observable('').extend({ required: true })
    self.password  = ko.observable('').extend({ required: true })    

    // self.retype.extend( areSame: { params: self.password, 
    //     message: "Salasanat eivät täsmää" })

    //ko.mapping.fromJS(data, {}, self.esitys);


    self.log = function () {


        Api('GET','users?projection={"password":1,"_id":1,"group_id":1}&where=username=='+self.login(),null, function(data) {

                    console.log(data)

                    if( data.length > 0 ) {
                    
                                        md5pass = data[0].password.split(':')[0]
                                        salt = data[0].password.split(':')[1]
                                        pass = $.md5(self.password()+salt)
                                        if( pass != md5pass) {
                                            console.log(pass+'!='+md5pass)
                                            alert('Väärä käyttäjätunnus tai salasana!')
                                        } else {
                                            var UID = data[0]._id
                                            var UGID = data[0].group_id
                                            Cookies.set('UID', UID)
                                            Cookies.set('UGID', UGID)
                                            
                                            Cookies.get('UGID')
                    
                                            if(UGID == AGID){
                                                
                                                window.location.href = 'admin.html';
                    
                                            } else {
                                                
                                                window.location.href = 'user.html';
                                                
                                                }
                                        }
                                } else {
                                    alert('Käyttäjätunnusta ei löydy')
                                    return false;
                                }            
                                
            })
        }


}

var Admin = function() {

    if(UID != null) {

        Api('GET','users/'+UID, null, function(data) {

                    self.username = ko.observable(data.username).extend({ required: true })
                    self.email  = ko.observable(data.email).extend({email:true})
                    self.puhelin = ko.observable()
                    self.password  = ko.observable('').extend(
                        { passwordComplexity:
                            { onlyIf: function() { return self.password != ''}}
                        })            
                    self.retype = ko.observable('').extend({ areSame:self.password });
        })


    } else {
        return false

    }

    self.saveUusi = function () {

            // console.log(self.group_id())

            var md5pass = $.md5(self.password()+salt)

            self.uusi = {
                    name: self.name,
                    puhelin: self.puhelin,
                    username: self.username,
                    email: self.email,
                    password: md5pass+':'+salt,
                    group_id: parseInt(self.group_id())
            }

        if( ID == 0 && self.admin ){

            Api('POST','users/', self.uusi, function(data) {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.href = 'users.html';
                }, 2000)
            } )
            
            } else if(UID != null) {

            Api('PATCH','users/'+UID, self.uusi, function() {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
            })

        }
    }

}    

// ko.applyBindings(new Login(), $('#uusi')[0]);



if ( /admin/.test(self.location.href) ) {

    ko.applyBindings(new Admin(), $('#login')[0]);

} else {

    ko.applyBindings(new Login(), $('#user')[0]);
}
 