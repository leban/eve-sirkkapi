

var Teos = function() {

    Lookup('ryhma');
    Lookup('toimijat');
    Lookup('sirkuslajit');


    if(UGID == AGID) {
        self.admin = ko.observable(true)

    } else {
        self.admin = ko.observable(false)
    }

    self.Shows = ko.observableArray([]);

    self.nimi=ko.observable();
    self.updated = ko.observable();
    self.nimi_en=ko.observable();
    self.esittaja=ko.observable();
    self.kuva=ko.observable('');
    self.fileInput = ko.observable();
    self.kuvaaja=ko.observable();
    self.kuva2=ko.observable('');
    self.fileInput2 = ko.observable();
    self.kuvaaja2=ko.observable('');
    self.kuva3=ko.observable('');
    self.fileInput3 = ko.observable();
    self.kuvaaja3 =ko.observable();
    self.markkinointi_fi=ko.observable();
    self.markkinointi_en=ko.observable();
    self.tyoryhma_fi=ko.observable();
    self.tyoryhma_en=ko.observable();
    self.teoksenkuvaus_fi=ko.observable();
    self.teoksenkuvaus_en=ko.observable();
    self.esitystyyppi=ko.observable();
    self.kesto=ko.observable();
    self.valiaika=ko.observable();
    self.kantaesitys=ko.observable();
    self.sensiilta=ko.observable();
    self.sirkuslajit_fi=ko.observable();
    self.sirkuslajit_en=ko.observable();
    self.kieli_fi=ko.observable();
    self.kieli_en=ko.observable();
    self.musiikki=ko.observable();
    self.vapaatxt_fi=ko.observable();
    self.vapaatxt_en=ko.observable();
    self.nykysirkus=ko.observable();
    self.perinteinen=ko.observable();
    self.kokoperhe=ko.observable();
    self.muu=ko.observable();
    self.muumika_fi=ko.observable();
    self.muumika_en=ko.observable();
    self.sirkka=ko.observable(0);
    self.esitys=ko.observable(0);
    self.muutjasenet=ko.observable();
    self.julkaistu=ko.observable(0);
    self.kunta=ko.observable();
    self.koreografi=ko.observable();
    self.tuottaja=ko.observable();
    self.paikka=ko.observable();
    self.group_id=ko.observable(UGID);
    self.promo=ko.observable(0);
    self.limited=ko.observable(0) 

    if(TID > 0) {

//julkaistu??

        Api('GET','esitys?where=main_id=='+TID+'&embedded={"venue_id":1}&sort=-pvm',null, 
            function(data){
                self.Shows.pushAll(data)
            })

        ko.computed( function(){

            return Api('GET','teos/'+TID,null, function(data) {

                    self.nimi(data.nimi)
                    self.updated(data._updated)
                    self.nimi_en(data.nimi_en)
                    self.esittaja(data.esittaja);
                    if(data.kuva) {
                        self.kuva(data.kuva);}
                    self.fileInput();
                    self.kuvaaja(data.kuvaaja);
                    if(data.kuva2) {
                        self.kuva2(data.kuva2);}
                    self.fileInput2();
                    self.kuvaaja2(data.kuvaaja2);
                    if(data.kuva3) {
                        self.kuva3(data.kuva3);}
                    self.fileInput3();
                    self.kuvaaja3(data.kuvaaja3);
                    var mark = JSON.parse(data.markkinointi)
                    self.markkinointi_fi(mark.fi);
                    self.markkinointi_en(mark.en);
                    try {
                        var tyoryh = JSON.parse(data.tyoryhma)
                        self.tyoryhma_fi(tyoryh.fi)
                        self.tyoryhma_en(tyoryh.en)
                    } catch(e) {
                        self.tyoryhma_fi(data.tyoryhma);
                        self.tyoryhma_en(data.tyoryhma)
                    }
                    var kuvaus = JSON.parse(data.teoksenkuvaus)
                    self.teoksenkuvaus_fi(kuvaus.fi);
                    self.teoksenkuvaus_en(kuvaus.en);
                    self.esitystyyppi(data.esitystyyppi);
                    self.kesto(data.kesto);
                    self.valiaika(data.valiaika);
                    self.kantaesitys(data.kantaesitys);
                    self.sensiilta(data.sensiilta);
                    self.sirkuslajit_fi(JSON.parse(data.sirkuslajit).fi);
                    self.sirkuslajit_en(JSON.parse(data.sirkuslajit).en);
                    var kieli = JSON.parse(data.kieli)
                    self.kieli_fi(kieli.fi);
                    self.kieli_en(kieli.en);
                    self.musiikki(data.musiikki);
                    var txt = JSON.parse(data.vapaatxt)
                    self.vapaatxt_fi(txt.fi);
                    self.vapaatxt_en(txt.en);
                    self.nykysirkus(data.nykysirkus);
                    self.perinteinen(data.perinteinen);
                    self.kokoperhe(data.kokoperhe);
                    self.muu(data.muu);
                    var muux = JSON.parse(data.muu)
                    self.muumika_fi(muux.fi);
                    self.muumika_en(muux.en);
                    self.sirkka(data.sirkka);
                    self.esitys(data.esitys);
                    self.muutjasenet(data.muutjasenet);
                    self.julkaistu(data.julkaistu);
                    self.kunta(data.kunta);
                    self.koreografi(data.koreografi);
                    self.tuottaja(data.tuottaja);
                    self.paikka(data.paikka);
                    if(data.group_id > 0) {
                        self.group_id(data.group_id);
                    } 
                    self.promo(data.promo);
                    self.limited(parseInt(data.limited));
            })

        },self)
    }
    self.saveUusi = function () {

            self.uusi = {
                user_id: parseInt(UID),    
                nimi:self.nimi(),
                nimi_en: self.nimi_en(),
                esittaja:self.esittaja(),
                kuva:self.fileInput(),
                kuvaaja:self.kuvaaja(),
                kuva2:self.fileInput2(),
                kuvaaja2:self.kuvaaja2(),
                kuva2:self.fileInput3(),
                kuvaaja3:self.kuvaaja3(),
                markkinointi:ko.toJSON({fi:self.markkinointi_fi(), en: self.markkinointi_en()}),
                tyoryhma:ko.toJSON({fi:self.tyoryhma_fi(), en: self.tyoryhma_en()}),
                // tyoryhma:self.tyoryhma(),
                //teoksenkuvaus:ko.toJSON({fi:self.teoksenkuvaus_fi(), en: self.teoksenkuvaus_en()}),
                esitystyyppi:self.esitystyyppi(),
                kesto:self.kesto(),
                valiaika:self.valiaika(),
                kantaesitys:self.kantaesitys(),
                sensiilta:self.sensiilta(),
                sirkuslajit:ko.toJSON({fi:self.sirkuslajit_fi(), en: self.sirkuslajit_en()}),
                kieli:ko.toJSON({fi:self.kieli_fi(), en: self.kieli_en()}),
                musiikki:self.musiikki(),
                vapaatxt:ko.toJSON({fi:self.vapaatxt_fi(), en: self.vapaatxt_en()}),
                nykysirkus:self.nykysirkus(),
                perinteinen:self.perinteinen(),
                kokoperhe:self.kokoperhe(),
                muu:self.muu(),
                muumika:ko.toJSON({fi:self.muumika_fi(), en: self.muumika_en()}),
                sirkka:self.sirkka(),
                esitys:self.esitys(),
                muutjasenet:self.muutjasenet(),
                julkaistu:self.julkaistu(),
                kunta:self.kunta(),
                koreografi:self.koreografi(),
                tuottaja:self.tuottaja(),
                paikka:self.paikka(),
                group_id:parseInt(self.group_id()),
                promo:self.promo(),
                limited:parseInt(self.limited)
            }

            // var data = new FormData();
            // data.append('file',self.fileInput.data)
            // data.append('json', JSON.stringify(self.uusi))

            console.log(JSON.stringify(self.uusi))

        if(ID > 0) {

            Api('PATCH','teos/'+TID, self.uusi, function(data){
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
            })

        } else {

      //   $.ajax({
      //   method: 'PUT',
      //   url: baseURL+'teos',
      //   async: false,
      //   //dataType: "json",
      //   contentType: "multipart/form-data; charset='utf-8'",
      //   //data: data,
      //   data: JSON.stringify(self.uusi),
      //   success: function (data) {
      //       console.log(data)
            
      //   },
      //   error: function (xhr, type, exception) {
      //       console.log(xhr.responseText)
      //   }
      // });


            Api('POST','teos/',self.uusi, function(data){
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.href = 'teokset.html';
                }, 2000)
            })            
            
        }
    }

}




var Teokset = function() {

    //ko.virtualElements.allowedBindings.group_id = true;

    Lookup('ryhma')

    var self = this;


    if(UGID == AGID ) {
        self.admin = ko.observable(true)
    } else {
        self.admin = ko.observable(false)
    }

    var group = ''

    if(GID) {
        var group = '&where={"group_id":'+GID+',"julkaistu":1}'
    }

    self.teokset = ko.observableArray();

    Api('GET','teos?sort=nimi'+group, null, function(data){

        //console.log(data)

        //self.artists.pushAll(data._items)

            $.each(data, function(index, item) {
                self.teokset.push(
                    ko.mapping.fromJS(item)
                )
            })     
    })


    self.saveRow = function(row) {

        patch = {group_id:parseInt(row.group_id())}

        Api('PATCH','teos/'+row._id(), patch, function() {
                console.log('saved')
            })

        // var user = $.ajax({
        //     method: 'PATCH',
        //     url: baseURL + 'users/'+row._id(),
        //     //async: false,
        //     dataType: "json",
        //     contentType: "application/json",
        //     data: JSON.stringify(patch),
        //     success: function (user) {
        //         },
        //     error: function (xhr, type, exception) {
        //         console.log(xhr)

        //         }
        //     });

        return true;
    }

    self.delRow = function(row) {

        Api('DELETE','teos/'+row._id(), null, function(){
            console.log('Deleted')
        })

        // var user = $.ajax({
        //     method: 'DELETE',
        //     url: baseURL + 'users/'+row._id(),
        //     //async: false,
        //     dataType: "json",
        //     contentType: "application/json",
        //     //data: JSON.stringify(patch),
        //     success: function (user) {
        //         console.log('del:'+row._id())
        //         },
        //     error: function (xhr, type, exception) {
        //         console.log(xhr)

        //         }
        //     });

        return true;
    }


   self.addRow = function() {

   }

    self.saveRow = function () {

    }

}

$(document).one('click','#ui-id-2', function() {
    // ko.applyBindings(new Teokset(), $('#teokset')[0]);

    ko.applyBindings(new Teos(), $('#teos')[0]);


})
 


 