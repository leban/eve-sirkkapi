var Admin = function() {

    if(UID != null) {

        Api('GET','users/'+UID, null, function(data) {

                    self.username = ko.observable(data.username).extend({ required: true })
                    self.email  = ko.observable(data.email).extend({email:true})
                    self.puhelin = ko.observable()
                    self.password  = ko.observable('').extend(
                        { passwordComplexity:
                            { onlyIf: function() { return self.password != ''}}
                        })            
                    self.retype = ko.observable('').extend({ areSame:self.password });
        })


    } else {
        return false

    }

    self.saveUusi = function () {

            // console.log(self.group_id())

            var md5pass = $.md5(self.password()+salt)

            self.uusi = {
                    name: self.name,
                    puhelin: self.puhelin,
                    username: self.username,
                    email: self.email,
                    password: md5pass+':'+salt,
                    group_id: parseInt(self.group_id())
            }

        if( ID == 0 && self.admin ){

            Api('POST','users/', self.uusi, function(data) {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.href = 'users.html';
                }, 2000)
            } )
            
            } else if(UID != null) {

            Api('PATCH','users/'+UID, self.uusi, function() {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
            })

        }
    }

}    

// ko.applyBindings(new Login(), $('#uusi')[0]);


var Users = function() {
    var self = this;

    //ko.virtualElements.allowedBindings.group_id = true;

    if(UGID == AGID) {
        self.admin = ko.observable(true)
    } else {
        return false;
    }

    Lookup('ryhma')

    self.users = ko.observableArray();

    Api('GET','users?embedded={"group_id":1}', null, function(data){
        self.users.pushAll(data);  
    })



    self.saveRow = function(row) {

        patch = {group_id:parseInt(row.group_id)}

        //console.log(patch)

        Api('PATCH','users/'+row._id, patch, function(data){
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
            })


        return true;
    }

    self.delRow = function(row) {

        Api('DELETE', '/users'+row._id, null, function(data) {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
        })

        return true;
    }


}


var User = function() {
    var self = this;
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghijklmnoprstuvwz";
    var string_length = 32;
    var salt = "";

    Lookup('ryhma');

    for(var i=0; i<string_length; i++){
      var randomPos = Math.floor( Math.random() * chars.length );
      salt += chars.substr(randomPos, 1);
    }

    if(UGID == AGID) {
        self.admin = ko.observable(true)
        } else {
        self.admin = ko.observable(false)
        }

    if(ID == 0 && self.admin) {
            self.name = ko.observable('').extend({ required: true })
            self.puhelin = ko.observable()
            self.username = ko.observable('').extend({ required: true })
            self.email  = ko.observable('').extend({email:true})
            self.password  = ko.observable('').extend(
                        { passwordComplexity:
                            { onlyIf: function() { return self.password != ''}}
                        })            
            self.retype = ko.observable('').extend({ areSame:self.password });
            self.group_id = ko.observable().extend({ required: true })

    } else if(ID != null ) {
        
            Api('GET', 'users/'+ID+'?embedded={"group_id":1}', null, function(data) {
                    self.name = ko.observable(data.name).extend({ required: true })
                    self.puhelin = ko.observable(data.puhelin)
                    self.username = ko.observable(data.username).extend({ required: true })
                    self.email  = ko.observable(data.email).extend({email:true})
                    self.password  = ko.observable('').extend(
                        { passwordComplexity:
                            { onlyIf: function() { return self.password != ''}}
                        })
                    self.retype = ko.observable('').extend( {areSame:self.password });
                    self.group_id = ko.observable(data.group_id).extend({ required: true })
        })
    } else if(UID != null) {

        Api('GET', 'users/'+UID+'?embedded={"group_id":1}', null, function(data) {
                    self.name = ko.observable(data.name).extend({ required: true })
                    self.puhelin = ko.observable(data.puhelin)
                    self.username = ko.observable(data.username).extend({ required: true })
                    self.email  = ko.observable(data.email).extend({email:true})
                    self.password  = ko.observable('').extend(
                        { passwordComplexity:
                            { onlyIf: function() { return self.password != ''}}
                        })
                    self.retype = ko.observable('').extend( {areSame:self.password });
                    self.group_id = ko.observable(data.group_id).extend({ required: true })
        })
    } 

    // self.retype.extend( areSame: { params: self.password, 
    //     message: "Salasanat eivät täsmää" })

    //ko.mapping.fromJS(data, {}, self.esitys);


    self.saveUusi = function () {

            // console.log(self.group_id())

            var md5pass = $.md5(self.password()+salt)

            self.uusi = {
                    name: self.name,
                    puhelin: self.puhelin,
                    username: self.username,
                    email: self.email,
                    password: md5pass+':'+salt,
                    group_id: parseInt(self.group_id())
            }

        if( ID == 0 && self.admin ){

            Api('POST','users/', self.uusi, function(data) {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.href = 'users.html';
                }, 2000)
            } )
            
            } else if(UID != null) {

            Api('PATCH','users/'+UID, self.uusi, function() {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
            })

        }
    }
}

$(document).ready(function() {

    // ko.applyBindings(new Users(), $('#users')[0]);

    
    ko.applyBindings(new User(), $('#user')[0]);
  })