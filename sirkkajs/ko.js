var baseURL = 'http://sirkusinfo.fi:5000/esitys/';
var embed = '?embedded={"main_id":1,"esid":1}';
var ID = '';

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    vars[key] = value;
    });
    return vars;
}

function getAPI (uri, method, data) {
            var request = {
                url: uri,
                type: method,
                contentType: "multipart/form-data",
                accepts: "application/json",
                cache: false,
                dataType: 'json',
                data: JSON.stringify(data),
                beforeSend: function (xhr) {
                    // xhr.setRequestHeader("Authorization", 
                    //     "Basic " + btoa(self.username + ":" + self.password));
                },
                error: function(jqXHR) {
                    console.log("ajax error " + jqXHR.status);
                }
            };
            console.log($.ajax(request) )
            return $.ajax(request);
        }

 function esitysVM() {
        var self = this;
        ID = getUrlVars()['ID'];
        if(!ID) {
            ID = '';
        }
        self.esitysURI = baseURL + ID + embed;
        // self.username = "miguel";
        // self.password = "python";
        self.nimi = ko.observable('nimi')
        self.esitys = ko.observable('esitys')
        self.user = ko.observable('user')
        self.ryhma = ko.observable('ryhma')
        self.kantaes = ko.observable('kantaes')
        self.tyoryhma = ko.observable('tyoryhma')
        self.yhteistyo = ko.observable('yhteistyo')
        self.yhteisesitys = ko.observable('yhteisesitys')
        self.maa = ko.observable('maa')
        self.kaupunki = ko.observable('kaupunki')
        self.tapahtuma = ko.observable('tapahtuma')
        self.paikka = ko.observable('paikka')
        self.esityskerrat = ko.observable('esityskerrat')
        self.ilmaisesitykset = ko.observable('ilmaisesitykset')
        self.myydyt = ko.observable('myydyt')
        self.ilmaiset  = ko.observable('ilmaiset')
        self.tyopaja = ko.observable('tyopaja')

        self.beginAdd = function() {
            alert("Add");
        }
        self.beginEdit = function(esitys) {
            alert("Edit: " + esitys.nimi());
        }
        self.remove = function(esitys) {
            alert("Remove: " + esitys.nimi());
        }
        self.markInProgress = function(esitys) {
            esitys.done(false);
        }
        self.markDone = function(esitys) {
            esitys.done(true);
        }

        getAPI(self.esitysURI, 'GET').done(function(data) {

            self.nimi(data.main_id.nimi)
            self.esitys(data.main_id.esitystyyppi)
            self.user(data.main_id.user_id)
            self.ryhma(data.main_id.esittaja)
            self.kantaes(data.main_id.kantaesitys)
            self.tyoryhma(data.main_id.tyoryhma)
            self.yhteistyo(data.main_id.yhteistyo)
            self.yhteisesitys(data.main_id.yhteisesitys)
            self.maa(data.esid.maa)
            self.kaupunki(data.esid.kaupunki)
            self.paikka(data.esid.paikka)
            self.tapahtuma(data.esid.paikka)
            self.esityskerrat(data.main_id.nimi)
            self.ilmaisesitykset(data.main_id.nimi)
            self.myydyt(data.main_id.myydyt)
            self.ilmaiset(data.main_id.yleiso)
            self.tyopaja(data.main_id.tyopaja)
        
        });


    }

ko.applyBindings(new esitysVM(), $('#main')[0]);
