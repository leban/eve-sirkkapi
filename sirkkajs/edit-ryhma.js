

/**** EDIT RYHMÄ *****/

var Group = function() {

    Lookup('toimijat')
    
    if(getUrlVars()['group_id']) {
        var GID = parseInt(getUrlVars()['group_id']);
        console.log(GID)
    }

     $(document).on('focusin','input.organisaatio',
        function() {
            var org = new Array([
                    'Yhdistys',
                    'Yritys',
                    'Osuuskunta',
                    'Avoin yhtiö',
                    'Toiminimi',
                    'Yleishyödyllinen yhteisö',
                    'Työryhmä'])

            $('input.organisaatio').typeahead( { 
            source:org,
            updater: function(item) {
                return this.$element.val().replace(/[^,]*$/,'')+item+' ';
            },
            matcher: function (item) {
              var tquery = extractor(this.query);
              console.log(item)
              if(!tquery) return false;
              console.log(~item.indexOf(tquery))
              return ~item.toLowerCase().indexOf(tquery.toLowerCase())
            },
            highlighter: function (item) {
              var query = extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
              return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
                return '<strong>' + match + '</strong>'
                })
              }
             }                                
           );
    })


    /* Taitelijalista */
    self.Artists = ko.observableArray();


    self.rm = function(row) {
        Api('PATCH','toimijat/'+row._id,{
            group_id:null
        }, function(data) {
            self.Artists.remove(row)
        })
    }

    self.added = ko.observable();

    self.add = function() {
        var name = $('input.toimijat').val().split(',')
        Api('GET', 'toimijat?where=sukunimi=='+name[0]+'&where=etunimi=='+name[1], null, function(data){
            console.log(data)
            Api('PATCH','toimijat/'+data[0]._id,
                {group_id: GID}, function(res){

                    Api('GET','toimijat?where=group_id=='+GID+'&sort=sukunimi', null, function(list) {
                        self.Arists.removeAll();
                        self.Arists.pushAll(list)
                    })
                })
        })

    }

        /* Teoslista */

    self.Shows = ko.observableArray();
    
            self.nimi=ko.observable();
            self.nimi_en=ko.observable();
            self.kuva=ko.observable();
            self.fileInput=ko.observable();
            self.updated=ko.observable();
            self.verkkosivu=ko.observable();
            self.kuvassa=ko.observable();
            self.kuvaaja=ko.observable();
            self.perustettu=ko.observable();
            self.organisaatiomuoto=ko.observable();
            self.yhteyshenkilo=ko.observable();
            self.kuvaus_fi=ko.observable();
            self.kuvaus_en=ko.observable();
            self.jasenet=ko.observable();
            self.tyylilaji=ko.observable();
            self.nykysirkus=ko.observable();
            self.perinteinen=ko.observable();
            self.kokoperhe=ko.observable();
            self.muu=ko.observable();
            self.muumika_fi=ko.observable();
            self.muumika_en=ko.observable();
            self.tuotannot=ko.observable();
            self.muitatietoja_fi=ko.observable();
            self.muitatietoja_en=ko.observable();
            self.kuva=ko.observable('');
            self.userid=ko.observable(0);
            self.julkaistu=ko.observable(false);
            self.loppunut=ko.observable();

    if(GID > 0) {

        Api('GET','toimijat?where=group_id=='+GID+'&sort=sukunimi', null, function(list) {
            self.Artists.pushAll(list)
        })

        Api('GET','teos?where=group_id=='+GID+'&sort=-sensiilta', null, function(list) {
            console.log(list)
            self.Shows.pushAll(list)
        })

    ko.computed(function(){

        return Api('GET','ryhma/'+GID, null,function(data) {

            self.nimi = ko.observable(data.nimi);
            self.nimi_en = ko.observable(data.nimi_en);
            self.updated = ko.observable(data._updated);
            self.kuva = ko.observable(data.kuva)
            self.verkkosivu = ko.observable(data.verkkosivu);
            self.kuvassa = ko.observable(data.kuvassa);
            self.kuvaaja = ko.observable(data.kuvaaja);
            self.perustettu = ko.observable(data.perustettu);
            self.organisaatiomuoto = ko.observable(data.organisaatiomuoto);
            self.yhteyshenkilo = ko.observable(data.yhteyshenkilo);
            self.kuvaus_fi = ko.observable(JSON.parse(data.kuvaus).fi);
            self.kuvaus_en = ko.observable(JSON.parse(data.kuvaus).en);
            self.jasenet = ko.observable(data.jasenet);
            self.tyylilaji = ko.observable(data.tyylilaji);
            self.nykysirkus = ko.observable(data.nykysirkus);
            self.perinteinen = ko.observable(data.perinteinen);
            self.kokoperhe = ko.observable(data.kokoperhe);
            self.muu = ko.observable(data.muu);
            self.muumika_fi = ko.observable(JSON.parse(data.muumika).fi);
            self.muumika_en = ko.observable(JSON.parse(data.muumika).en);
            self.tuotannot = ko.observable(data.tuotannot);
            self.muitatietoja_fi = ko.observable(JSON.parse(data.muitatietoja).fi);
            self.muitatietoja_en = ko.observable(JSON.parse(data.muitatietoja).en);
            self.kuva = ko.observable(data.kuva);
            self.userid = ko.observable(data.userid);
            self.julkaistu = ko.observable(data.julkaistu);
            self.loppunut = ko.observable(data.loppunut);

        })
    }, self)
}

    self.saveRow = function () {


            self.uusi = {
                nimi:self.nimi(),
                nimi:self.nimi_en(),
                verkkosivu:self.verkkosivu(),
                kuva:self.fileInput(),
                kuvassa:self.kuvassa(),
                kuvaaja:self.kuvaaja(),
                perustettu:self.perustettu().toString(),
                organisaatiomuoto:self.organisaatiomuoto(),
                yhteyshenkilo:self.yhteyshenkilo(),
                //kuvaus:'{"fi":"'+self.kuvaus_fi()+'","en":"'+self.kuvaus_en()+'"}',
                kuvaus: ko.toJSON({fi: self.kuvaus_fi(), en: self.kuvaus_en()}),
                jasenet:self.jasenet(),
                tyylilaji:self.tyylilaji(),
                nykysirkus:self.nykysirkus(),
                perinteinen:self.perinteinen(),
                kokoperhe:self.kokoperhe(),
                muu:self.muu(),
                //muumika:'{"fi":"'+self.muumika_fi()+'","en":"'+self.muumika_en()+'""}',
                muumika: ko.toJSON({fi: self.muumika_fi(), en:self.muumika_en()}),
                tuotannot:self.tuotannot(),
                //muitatietoja:'{"fi":"'+self.muitatietoja_fi()+'","en":"'+self.muitatietoja_en()+'"}',
                muitatietoja: ko.toJSON({fi: self.muitatietoja_fi(), en:self.muitatietoja_en()}),
                kuva:self.kuva(),
                userid:UID,
                julkaistu:self.julkaistu(),
                loppunut:self.loppunut(),
            }

        console.log(ko.toJSON(self.uusi))


        if(GID > 0) {


            Api('PATCH','ryhma/'+GID, self.uusi, function(data) {
                $('.alert').toggle();
                    window.setTimeout(function() {
                        location.reload();
                    }, 2000)

                location.reload()
            } )

        }

    }
}


    // ko.applyBindings(new Groups(), $('#groups')[0]);


            ko.applyBindings(new Group(), $('#group')[0]);





 
 