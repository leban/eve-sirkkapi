

var Artists = function() {

    //ko.virtualElements.allowedBindings.group_id = true;

    var self = this;

    self.artists = ko.observableArray();

    var data = $.ajax({
        method: 'GET',
        url: baseURL+'toimijat?sort=sukunimi,etunimi',
        async: false,
        dataType: "json",
        contentType: "multipart/form-data; charset='utf-8'",
        data: data,//JSON.stringify(data),
        success: function (data) {
            $.each(data._items, function(index, item) {
                self.artists.push(
                    ko.mapping.fromJS(item)
                )
            })   
        },
        error: function (xhr, type, exception) {
            // Do your thing
        }
    });
    
    //console.log(self.users() )

    var groups = new Array();
    var gid = new Object();

    var gdata = $.ajax({
        method: 'GET',
        url: baseURL+'ryhma/',
        async: false,
        dataType: "json",
        contentType: "multipart/form-data; charset='utf-8'",
        data: gdata,//JSON.stringify(data),
        success: function (gdata) {
                $.each( gdata._items, function ( index, item ) {
                    groups.push( item.nimi );
                    gid[item.nimi] = item._id;
                    });
        
                $(document).on('focusin','input.ryhma',
                function() {
                    $('input.ryhma').typeahead( { 
                    source:groups,
                    updater: function(item) {
                        return this.$element.val().replace(/[^,]*$/,'')+item;
                    },
                    matcher: function (item) {
                      var tquery = extractor(this.query);
                      if(!tquery) return false;
                      return ~item.toLowerCase().indexOf(tquery.toLowerCase())
                    },
                    highlighter: function (item) {
                      var query = extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
                      return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
                        return '<strong>' + match + '</strong>'
                        })
                      }
                     }                                
                   );
                })
                .on('change','input.ryhma', function() {
                     var res = $(this).val();
                     $(this).prev('input.group_id').val(gid[res]).change();

                     });

        },
        error: function (xhr, type, exception) {
            console.log(xhr)
            // Do your thing
        }
    });

    //getAPI(baseURL+embed,'GET');
    

    // self.ryhmat.pushAll(data.responseJSON._items)


    //ko.mapping.fromJS(data, {}, self.esitys);

    self.saveRow = function(row) {


        patch = {group_id:parseInt(row.group_id())}

        console.log(patch)

        var user = $.ajax({
            method: 'PATCH',
            url: baseURL + 'users/'+row._id(),
            //async: false,
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(patch),
            success: function (user) {
                },
            error: function (xhr, type, exception) {
                console.log(xhr)

                }
            });

        return true;
    }

    self.delRow = function(row) {

        var user = $.ajax({
            method: 'DELETE',
            url: baseURL + 'users/'+row._id(),
            //async: false,
            dataType: "json",
            contentType: "application/json",
            //data: JSON.stringify(patch),
            success: function (user) {
                console.log('del:'+row._id())
                },
            error: function (xhr, type, exception) {
                console.log(xhr)

                }
            });

        return true;
    }


   self.addUser = function() {

   }

    self.saveUser = function () {

    }

}

viewModel = new Artists();

ko.applyBindings(viewModel);
 