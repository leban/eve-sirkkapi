

var Artist = function() {

    if(!ID && UID) {

        ID = UID

    }

    Lookup('ryhma')
    Lookup('sirkuslajit')

    self.etunimi=ko.observable();
    self.sukunimi=ko.observable();
    self.updated = ko.observable();
    self.taiteilijanimet=ko.observable();
    self.syntymaaika=ko.observable();
    self.kansallisuus_fi=ko.observable();
    self.kansallisuus_en=ko.observable();
    self.verkkosivu=ko.observable();
    self.sahkoposti=ko.observable();
    self.puhelinnumero=ko.observable();
    self.ammatti_fi=ko.observable();
    self.ammatti_en=ko.observable();
    self.ilmaakrobaatti=ko.observable();
    self.akrobaatti=ko.observable();
    self.jongloori=ko.observable();
    self.klovni=ko.observable();
    self.taikuri=ko.observable();
    self.tasapainoilu=ko.observable();
    self.tulitaiteilija=ko.observable();
    self.muu=ko.observable();
    self.muumika_fi=ko.observable();
    self.muumika_en=ko.observable();
    self.ammatti_fi=ko.observable();
    self.ammatti_en=ko.observable();
    self.esiintyja=ko.observable();
    self.ohjaaja=ko.observable();
    self.tuottaja=ko.observable();
    self.sosiaalinen=ko.observable();
    self.koreografi=ko.observable();
    self.saveltaja=ko.observable();
    self.aanisuunnittelija=ko.observable();
    self.lavastaja=ko.observable();
    self.pukusuunnittelija=ko.observable();
    self.valosuunnittelija=ko.observable();
    self.videomediataiteilija=ko.observable();
    self.sirkusopettaja=ko.observable();
    self.muutyo=ko.observable();
    self.tyomika_fi=ko.observable();
    self.tyomika_en=ko.observable();
    self.vapaateksti_fi=ko.observable();
    self.vapaateksti_en=ko.observable();
    // self.furorpoeticus_fi=ko.observable();
    // self.furorpoeticus_en=ko.observable();
    self.koulutus_fi=ko.observable();
    self.koulutus_en=ko.observable();
    self.taiteellinentyo_fi=ko.observable();
    self.taiteellinentyo_en=ko.observable();
    self.opetustyo_fi=ko.observable();
    self.opetustyo_en=ko.observable();
    self.opettajavalmius_fi=ko.observable();
    self.opettajavalmius_en=ko.observable();
    self.kuva=ko.observable('');
    self.fileInput=ko.observable();
    self.kuva2=ko.observable('');
    self.fileInput2=ko.observable();
    self.kuva3=ko.observable('');
    self.fileInput3=ko.observable();
    self.kuvaaja=ko.observable('');
    self.kuvaaja2=ko.observable('');
    self.kuvaaja3=ko.observable('');
    self.userid=ko.observable(UID);
    self.julkaistu=ko.observable(0);
    self.group_id=ko.observable(UGID);
    

    if(ID > 0) {

    ko.computed(function () { 


        return Api('GET','toimijat?where=userid=='+ID, null, function(res) {

            console.log(res[0])

            if(res.length > 0) {

                var data = res[0]

                    self.etunimi(data.etunimi);
                    self.sukunimi(data.sukunimi);
                    self.updated(data._updated);
                    self.taiteilijanimet(data.taiteilijanimet);
                    self.syntymaaika(data.syntymaaika);
                    self.kansallisuus_fi(JSON.parse(data.kansallisuus).fi);
                    self.kansallisuus_en(JSON.parse(data.kansallisuus).en);
                    self.verkkosivu(data.verkkosivu);
                    self.sahkoposti(data.sahkoposti);
                    self.puhelinnumero(data.puhelinnumero);
                    self.ammatti_fi(JSON.parse(data.ammatti).fi);
                    self.ammatti_en(JSON.parse(data.ammatti).en);
                    self.ilmaakrobaatti(data.ilmaakrobaatti);
                    self.akrobaatti(data.akrobaatti);
                    self.jongloori(data.jongloori);
                    self.klovni(data.klovni);
                    self.taikuri(data.taikuri);
                    self.tasapainoilu(data.tasapainoilu)
                    self.tulitaiteilija(data.tulitaiteilija);
                    self.muu(data.muu);
                    self.muumika_fi(JSON.parse(data.muumika).fi);
                    self.muumika_en(JSON.parse(data.muumika).en);
                    self.esiintyja(data.esiintyja);
                    self.ohjaaja(data.ohjaaja);
                    self.tuottaja(data.tuottaja);
                    self.sosiaalinen(data.sosiaalinen);
                    self.koreografi(data.koreografi);
                    self.saveltaja(data.saveltaja);
                    self.aanisuunnittelija(data.aanisuunnittelija);
                    self.lavastaja(data.lavastaja);
                    self.pukusuunnittelija(data.pukusuunnittelija);
                    self.valosuunnittelija(data.valosuunnittelija);
                    self.videomediataiteilija(data.videomediataiteilija);
                    self.sirkusopettaja(data.sirkusopettaja);
                    self.muutyo(data.muutyo);
                    self.tyomika_fi(JSON.parse(data.tyomika).fi);
                    self.tyomika_en(JSON.parse(data.tyomika).en);
                    self.vapaateksti_fi(JSON.parse(data.vapaateksti).fi);
                    self.vapaateksti_en(JSON.parse(data.vapaateksti).en);
                    // self.furorpoeticus_fi(JSON.parse(data.furorpoeticus).fi);
                    // self.furorpoeticus_en(JSON.parse(data.furorpoeticus).en);
                    self.koulutus_fi(JSON.parse(data.koulutus).fi);
                    self.koulutus_en(JSON.parse(data.koulutus).en);
                    self.taiteellinentyo_fi(JSON.parse(data.taiteellinentyo).fi);
                    self.taiteellinentyo_en(JSON.parse(data.taiteellinentyo).en);
                    self.opetustyo_fi(JSON.parse(data.opetustyo).fi);
                    self.opetustyo_en(JSON.parse(data.opetustyo).en);
                    self.opettajavalmius_fi(JSON.parse(data.opettajavalmius).fi);
                    self.opettajavalmius_en(JSON.parse(data.opettajavalmius).en);
                    if(data.kuva) {
                        self.kuva(data.kuva);}
                    if(data.kuva2) {
                        self.kuva2(data.kuva2);
                    }
                    if(data.kuva3) {
                        self.kuva3(data.kuva3);
                    }
                    self.kuvaaja(data.kuvaaja);
                    self.kuvaaja2(data.kuvaaja2);
                    self.kuvaaja3(data.kuvaaja3);
                    self.userid(data.userid);
                    self.julkaistu(data.julkaistu);
                    self.group_id(data.group_id);

                    }
                }
            )
        }, self)
    }
    
    self.saveUusi = function () {

            self.uusi = {
                    etunimi:self.etunimi(),
                    sukunimi:self.sukunimi(),
                    taiteilijanimet:self.taiteilijanimet(),
                    syntymaaika:self.syntymaaika(),
                    kansallisuus:ko.toJSON({fi:self.kansallisuus_fi(), en: self.kansallisuus_en()}),
                    verkkosivu:self.verkkosivu(),
                    sahkoposti:self.sahkoposti(),
                    puhelinnumero:self.puhelinnumero(),
                    ammatti:ko.toJSON({fi:self.ammatti_fi(), en: self.ammatti_en()}),
                    ilmaakrobaatti:self.ilmaakrobaatti(),
                    akrobaatti:self.akrobaatti(),
                    tasapainoilu:self.tasapainoilu(),
                    jongloori:self.jongloori(),
                    klovni:self.klovni(),
                    taikuri:self.taikuri(),
                    tulitaiteilija:self.tulitaiteilija(),
                    muu:self.muu(),
                    muumika:ko.toJSON({fi:self.muumika_fi(),en: self.muumika_en()}),
                    esiintyja:self.esiintyja(),
                    ohjaaja:self.ohjaaja(),
                    tuottaja:self.tuottaja(),
                    sosiaalinen:self.sosiaalinen(),
                    sirkusopettaja:self.sirkusopettaja(),
                    koreografi:self.koreografi(),
                    saveltaja:self.saveltaja(),
                    aanisuunnittelija:self.aanisuunnittelija(),
                    lavastaja:self.lavastaja(),
                    pukusuunnittelija:self.pukusuunnittelija(),
                    valosuunnittelija:self.valosuunnittelija(),
                    videomediataiteilija:self.videomediataiteilija(),
                    sirkusopettaja:self.sirkusopettaja(),
                    vapaateksti:ko.toJSON({fi:self.vapaateksti_fi(),en: self.vapaateksti_en()}),
                    furorpoeticus:ko.toJSON({fi:self.furorpoeticus_fi(),en: self.furorpoeticus_en()}),
                    koulutus:ko.toJSON({fi:self.koulutus_fi(),en: self.koulutus_en()}),
                    taiteellinentyo:ko.toJSON({fi:self.taiteellinentyo_fi(),en: self.taiteellinentyo_en()}),
                    opetustyo:ko.toJSON({fi:self.opetustyo_fi(),en: self.opetustyo_en()}),
                    opettajavalmius:ko.toJSON({fi:self.opettajavalmius_fi(),en: self.opettajavalmius_en()}),
                    kuva:self.fileInput(),
                    kuva2:self.fileInput2(),
                    kuva3:self.fileInput3(),
                    kuvaaja:self.kuvaaja(),
                    kuvaaja2:self.kuvaaja(),
                    kuvaaja3:self.kuvaaja(),
                    userid:parseInt(UID),
                    julkaistu:self.julkaistu(),
                    group_id:parseInt(self.group_id())
            }


        if(ID > 0) {

            Api('PATCH','toimijat/'+ID, self.uusi, function(data){
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
            })

        } else {

            Api('POST','toimijat/',self.uusi, function(data){
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.href = 'users.html';
                }, 2000)
            })            
            
        }
    }

}

var Artists = function() {

    //ko.virtualElements.allowedBindings.group_id = true;

    Lookup('ryhma')

    var self = this;

    var group = ''

    if(GID) {
        var group = '&where=group_id=='+GID
    } 

    self.artists = ko.observableArray();

    Api('GET','toimijat?sort=sukunimi,etunimi'+group, null, function(data){

        //console.log(data)

        //self.artists.pushAll(data._items)

            $.each(data, function(index, item) {
                self.artists.push(
                    ko.mapping.fromJS(item)
                )
            })     
    })


    self.saveRow = function(row) {

        patch = {group_id:parseInt(row.group_id())}

        Api('PATCH','users/'+row._id(), patch, function() {
                console.log('saved')
            })

        // var user = $.ajax({
        //     method: 'PATCH',
        //     url: baseURL + 'users/'+row._id(),
        //     //async: false,
        //     dataType: "json",
        //     contentType: "application/json",
        //     data: JSON.stringify(patch),
        //     success: function (user) {
        //         },
        //     error: function (xhr, type, exception) {
        //         console.log(xhr)

        //         }
        //     });

        return true;
    }

    self.delRow = function(row) {

        Api('DELETE','users/'+row._id(), null, function(){
            console.log('Deleted')
        })

        // var user = $.ajax({
        //     method: 'DELETE',
        //     url: baseURL + 'users/'+row._id(),
        //     //async: false,
        //     dataType: "json",
        //     contentType: "application/json",
        //     //data: JSON.stringify(patch),
        //     success: function (user) {
        //         console.log('del:'+row._id())
        //         },
        //     error: function (xhr, type, exception) {
        //         console.log(xhr)

        //         }
        //     });

        return true;
    }


   self.addRow = function() {

   }

    self.saveRow = function () {

    }

}

$(document).ready(function() {

        //ko.applyBindings(new Artists(), $('#artists')[0]);

        ko.applyBindings(new Artist(), $('#artist')[0]);
});

    

 


 