

/**** Sirkuslajit ****/

var Types = function() {
    ko.virtualElements.allowedBindings.group_id = true;

    var self = this;
        
    if(UGID == AGIG) {
        self.admin = ko.observable(true)
    } else {
        return false;
    }
        
    self.lajit = ko.observableArray([
        ]);


    Api('GET','sirkuslajit/',null,function(data){
        self.lajit.pushAll(data)

    })

    self.uusi = {
        fi: ko.observable(''),
        en: ko.observable('')
    }

    self.addRow = function(row) {
        var uus = {
            fi:row.fi(),
            en:row.en()
        }
        
        console.log(uus);

        Api('POST','sirkuslajit/',uus ,function(data) {
                console.log(data);

                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
        })
    }

    self.saveRow = function(row) {
        var data = {
            fi: row.fi,
            en: row.en
        }
        Api('PATCH','sirkuslajit/'+row._id, data, function(data) {

                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
        })
    }

    self.delRow = function(row) {
        Api('DELETE','sirkuslajit/'+row.id, null, function(data) {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
           
        })
 
    }

}


ko.applyBindings(new Types(), $('#types')[0]);





 
 