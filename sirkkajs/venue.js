


/**** VENUET ****/


var Venues = function() {

    var self = this;

    if(UGID == AGID) {
        self.admin = ko.observable(true)
    } else {
        return false;
    }           

    self.venuet = ko.observableArray([
        ]);
    
    Api('GET','venue/', null, function(data){
        self.venuet.pushAll(data)
    } 
        )


    self.delRow = function(row) {


        Api('DELETE',venue/+row._id(), function() {
            console.log('deleted')
        })

        return true;
    }

}




var Venue = function() {

    var ID = getUrlVars()['ID']; 


    if(ID != null) {
    
        Api('GET','venue/'+ID, null, function(data){
                        self.maa=ko.observable(data.maa);
                        self.kaupunki=ko.observable(data.kaupunki);
                        self.paikka=ko.observable(data.paikka);
                        // self.pvm=ko.observable(data.pvm);
                        // self.ensiilta=ko.observable(data.ensiilta);
                        self.url=ko.observable(data.url);
                        self.lon=ko.observable(data.lon);
                        self.lat=ko.observable(data.lat);
                        self.osoite=ko.observable(data.osoite);
                        self.postinro=ko.observable(data.postinro);
        })

    } else {

            self.maa=ko.observable('Suomi');
            self.kaupunki=ko.observable();
            self.paikka=ko.observable();
            self.url=ko.observable();
            self.lon=ko.observable();
            self.lat=ko.observable();
            self.osoite=ko.observable();
            self.postinro=ko.observable();
            // self.ensiilta = ko.observable();

    }

    // self.retype.extend( areSame: { params: self.password, 
    //     message: "Salasanat eivät täsmää" })

    //ko.mapping.fromJS(data, {}, self.esitys);


    self.Save = function () {

            //https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=YOUR_API_KEY

    var geocoder = new google.maps.Geocoder();
    var address = self.osoite()+','+self.postinro()+','+self.kaupunki()+','+self.maa();

   if (geocoder) {

      geocoder.geocode({ 'address': address }, function (results, status) {

         if (status == google.maps.GeocoderStatus.OK) {

            console.log(results[0].geometry.location.lat)
         }
         else {

            console.log("Geocoding failed: " + status);
         }
      });
   }    

            // var latlon = $.ajax({
            //         method: 'get',
            //         url: 'https://maps.googleapis.com/maps/api/geocode/json?address='+self.osoite()+','+self.postinro()+','+self.kaupunki()+','+self.maa(),
            //         //async: false,
            //         dataType: "json",
            //         contentType: "application/json",
            //         //data: JSON.stringify(patch),
            //         success: function (data) {
            //             console.log(data)
            //             },
            //         error: function (xhr, type, exception) {
            //             console.log(url)
            //             console.log(xhr)

            //             }
            //         });

            self.uusi = {
                maa:self.maa(),
                kaupunki:self.kaupunki(),
                paikka:self.paikka(),
                // pvm:self.pvm(),
                // ensiilta:self.ensiilta(),
                url:self.url(),
                lon:self.lon(),
                lat:self.lat(),
                osoite:self.osoite(),
                postinro:self.postinro(),
            }


        if(ID != null) {

            Api('PATCH','venue/'+ID, self.uusi, function() {
                    location.reload();
            })

            // var data = $.ajax({
            //     method: 'PATCH',
            //     url: baseURL + 'ryhma/'+ID,
            //     async: false,
            //     dataType: "json",
            //     contentType: "application/json",
            //     data: ko.toJSON(self.uusi),
            //     success: function (data) {
            //         console.log(data);
            //         location.reload();
            //         },
            //     error: function (xhr, type, exception) {
            //             // Do your thing
            //             console.log(xhr)

            //         }
            //     })

        } else {

            Api('POST','venue/', self.uusi, function() {
                    location.reload();  
            })

            // var data = $.ajax({
            //     method: 'POST',
            //     url: baseURL + 'ryhma/',
            //     async: false,
            //     dataType: "json",
            //     contentType: "application/json",
            //     data: ko.toJSON(self.uusi),
            //     success: function (data) {
            //         console.log(data);
            //         location.reload();
            //         },
            //     error: function (xhr, type, exception) {
            //             // Do your thing
            //             console.log(xhr)

            //         }
            //     })
            
        }
    }


}

if (/venuet/.test(self.location.href)) {
    ko.applyBindings(new Venues(), $('#venues')[0]);

} else {
    ko.applyBindings(new Venue(), $('#venue')[0]);
}





 
 