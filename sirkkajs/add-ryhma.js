


/**** ADD RYHMÄ *****/

var Group = function() {

    Lookup('toimijat')
    
    GID = 0

     $(document).on('focusin','input.organisaatio',
        function() {
            var org = new Array([
                    'Yhdistys',
                    'Yritys',
                    'Osuuskunta',
                    'Avoin yhtiö',
                    'Toiminimi',
                    'Yleishyödyllinen yhteisö',
                    'Työryhmä'])

            $('input.organisaatio').typeahead( { 
            source:org,
            updater: function(item) {
                return this.$element.val().replace(/[^,]*$/,'')+item+' ';
            },
            matcher: function (item) {
              var tquery = extractor(this.query);
              console.log(item)
              if(!tquery) return false;
              console.log(~item.indexOf(tquery))
              return ~item.toLowerCase().indexOf(tquery.toLowerCase())
            },
            highlighter: function (item) {
              var query = extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
              return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
                return '<strong>' + match + '</strong>'
                })
              }
             }                                
           );
    })


    /* Taitelijalista */
    self.Artists = ko.observableArray();


    Api('GET','toimijat?where=group_id=='+GID+'&sort=sukunimi', null, function(list) {
        self.Artists.pushAll(list)
    })

    self.rm = function(row) {
        Api('PATCH','toimijat/'+row._id,{
            group_id:null
        }, function(data) {
            self.Artists.remove(row)
        })
    }

    self.added = ko.observable();

    self.add = function() {
        var name = $('input.toimijat').val().split(',')
        Api('GET', 'toimijat?where=sukunimi=='+name[0]+'&where=etunimi=='+name[1], null, function(data){
            console.log(data)
            Api('PATCH','toimijat/'+data[0]._id,
                {group_id: GID}, function(res){

                    Api('GET','toimijat?where=group_id=='+GID+'&sort=sukunimi', null, function(list) {
                        self.Arists.removeAll();
                        self.Arists.pushAll(list)
                    })
                })
        })

            }

        /* Teoslista */

    self.Shows = ko.observableArray();

    
    Api('GET','teos?where=group_id=='+GID+'&sort=-sensiilta', null, function(list) {
        console.log(list)
        self.Shows.pushAll(list)
    })

            self.ryhma_nimi=ko.observable();
            self.ryhma_nimi_en=ko.observable();
            self.ryhma_kuva=ko.observable('');
            self.ryhma_fileInput=ko.observable('');
            self.ryhma_updated=ko.observable();
            self.ryhma_verkkosivu=ko.observable();
            self.ryhma_kuvassa=ko.observable();
            self.ryhma_kuvaaja=ko.observable();
            self.ryhma_perustettu=ko.observable();
            self.ryhma_organisaatiomuoto=ko.observable();
            self.ryhma_yhteyshenkilo=ko.observable();
            self.ryhma_kuvaus_fi=ko.observable();
            self.ryhma_kuvaus_en=ko.observable();
            self.ryhma_jasenet=ko.observable();
            self.ryhma_tyylilaji=ko.observable();
            self.ryhma_nykysirkus=ko.observable();
            self.ryhma_perinteinen=ko.observable();
            self.ryhma_kokoperhe=ko.observable();
            self.ryhma_muu=ko.observable();
            self.ryhma_muumika_fi=ko.observable();
            self.ryhma_muumika_en=ko.observable();
            self.ryhma_tuotannot=ko.observable();
            self.ryhma_muitatietoja_fi=ko.observable();
            self.ryhma_muitatietoja_en=ko.observable();
            self.ryhma_kuva=ko.observable('');
            self.ryhma_userid=ko.observable(0);
            self.ryhma_julkaistu=ko.observable(false);
            self.ryhma_loppunut=ko.observable();
    

    self.saveRow = function () {


            self.uusi = {
                nimi:self.ryhma_nimi(),
                nimi:self.ryhma_nimi_en(),
                verkkosivu:self.ryhma_verkkosivu(),
                kuva:self.ryhma_fileInput(),
                kuvassa:self.ryhma_kuvassa(),
                kuvaaja:self.ryhma_kuvaaja(),
                perustettu:self.ryhma_perustettu().toString(),
                organisaatiomuoto:self.ryhma_organisaatiomuoto(),
                yhteyshenkilo:self.ryhma_yhteyshenkilo(),
                //kuvaus:'{"fi":"'+self.kuvaus_fi()+'","en":"'+self.kuvaus_en()+'"}',
                kuvaus: ko.toJSON({fi: self.ryhma_kuvaus_fi(), en: self.ryhma_kuvaus_en()}),
                jasenet:self.ryhma_jasenet(),
                tyylilaji:self.ryhma_tyylilaji(),
                nykysirkus:self.ryhma_nykysirkus(),
                perinteinen:self.ryhma_perinteinen(),
                kokoperhe:self.ryhma_kokoperhe(),
                muu:self.ryhma_muu(),
                //muumika:'{"fi":"'+self.muumika_fi()+'","en":"'+self.muumika_en()+'""}',
                muumika: ko.toJSON({fi: self.ryhma_muumika_fi(), en:self.ryhma_muumika_en()}),
                tuotannot:self.ryhma_tuotannot(),
                //muitatietoja:'{"fi":"'+self.muitatietoja_fi()+'","en":"'+self.muitatietoja_en()+'"}',
                muitatietoja: ko.toJSON({fi: self.ryhma_muitatietoja_fi(), en:ryhma_self.muitatietoja_en()}),
                kuva:self.ryhma_kuva(),
                userid:UID,
                julkaistu:self.ryhma_julkaistu(),
                loppunut:self.ryhma_loppunut(),
            }

        console.log(ko.toJSON(self.uusi))

            Api('POST','ryhma/', self.uusi, function(data) {
                $('.alert').toggle();
                    window.setTimeout(function() {
                        location.reload();
                    }, 2000)

                location.reload()
            } )

         
    }
}


    // ko.applyBindings(new Groups(), $('#groups')[0]);

$(document).one('click','#ui-id-3', function() {
            ko.applyBindings(new Group(), $('#group')[0]);
            });
    




 
 