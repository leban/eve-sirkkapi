

var Teos = function() {

    Lookup('ryhma');
    Lookup('toimijat');
    Lookup('sirkuslajit');


    if(UGID == AGID) {
        self.admin = ko.observable(true)

    } else {
        self.admin = ko.observable(false)
    }

    self.Shows = ko.observableArray([]);

    self.teos_nimi=ko.observable();
    self.teos_updated = ko.observable();
    self.teos_nimi_en=ko.observable();
    self.teos_esittaja=ko.observable();
    self.teos_kuva=ko.observable('');
    self.teos_fileInput = ko.observable();
    self.teos_kuvaaja=ko.observable();
    self.teos_kuva2=ko.observable('');
    self.teos_fileInput2 = ko.observable();
    self.teos_kuvaaja2=ko.observable('');
    self.teos_kuva3=ko.observable('');
    self.teos_fileInput3 = ko.observable();
    self.teos_kuvaaja3 =ko.observable();
    self.teos_markkinointi_fi=ko.observable();
    self.teos_markkinointi_en=ko.observable();
    self.teos_tyoryhma_fi=ko.observable();
    self.teos_tyoryhma_en=ko.observable();
    self.teos_teoksenkuvaus_fi=ko.observable();
    self.teos_teoksenkuvaus_en=ko.observable();
    self.teos_esitystyyppi=ko.observable();
    self.teos_kesto=ko.observable();
    self.teos_valiaika=ko.observable();
    self.teos_kantaesitys=ko.observable();
    self.teos_sensiilta=ko.observable();
    self.teos_sirkuslajit_fi=ko.observable();
    self.teos_sirkuslajit_en=ko.observable();
    self.teos_kieli_fi=ko.observable();
    self.teos_kieli_en=ko.observable();
    self.teos_musiikki=ko.observable();
    self.teos_vapaatxt_fi=ko.observable();
    self.teos_vapaatxt_en=ko.observable();
    self.teos_nykysirkus=ko.observable();
    self.teos_perinteinen=ko.observable();
    self.teos_kokoperhe=ko.observable();
    self.teos_muu=ko.observable();
    self.teos_muumika_fi=ko.observable();
    self.teos_muumika_en=ko.observable();
    self.teos_sirkka=ko.observable(0);
    self.teos_esitys=ko.observable(0);
    self.teos_muutjasenet=ko.observable();
    self.teos_julkaistu=ko.observable(0);
    self.teos_kunta=ko.observable();
    self.teos_koreografi=ko.observable();
    self.teos_tuottaja=ko.observable();
    self.teos_paikka=ko.observable();
    self.teos_group_id=ko.observable(UGID);
    self.teos_promo=ko.observable(0);
    self.teos_limited=ko.observable(0) 

    

    self.saveUusi = function () {

            self.uusi = {
                user_id: parseInt(UID),    
                nimi:self.teos_nimi(),
                nimi_en: self.teos_nimi_en(),
                esittaja:self.teos_esittaja(),
                kuva:self.teos_fileInput(),
                kuvaaja:self.teos_kuvaaja(),
                kuva2:self.teos_fileInput2(),
                kuvaaja2:self.teos_kuvaaja2(),
                kuva2:self.teos_fileInput3(),
                kuvaaja3:self.teos_kuvaaja3(),
                markkinointi:ko.toJSON({fi:self.teos_markkinointi_fi(), en: self.teos_markkinointi_en()}),
                tyoryhma:ko.toJSON({fi:self.teos_tyoryhma_fi(), en: self.teos_tyoryhma_en()}),
                // tyoryhma:self.teos_tyoryhma(),
                //teoksenkuvaus:ko.toJSON({fi:self.teos_teoksenkuvaus_fi(), en: self.teos_teoksenkuvaus_en()}),
                esitystyyppi:self.teos_esitystyyppi(),
                kesto:self.teos_kesto(),
                valiaika:self.teos_valiaika(),
                kantaesitys:self.teos_kantaesitys(),
                sensiilta:self.teos_sensiilta(),
                sirkuslajit:ko.toJSON({fi:self.teos_sirkuslajit_fi(), en: self.teos_sirkuslajit_en()}),
                kieli:ko.toJSON({fi:self.teos_kieli_fi(), en: self.teos_kieli_en()}),
                musiikki:self.teos_musiikki(),
                vapaatxt:ko.toJSON({fi:self.teos_vapaatxt_fi(), en: self.teos_vapaatxt_en()}),
                nykysirkus:self.teos_nykysirkus(),
                perinteinen:self.teos_perinteinen(),
                kokoperhe:self.teos_kokoperhe(),
                muu:self.teos_muu(),
                muumika:ko.toJSON({fi:self.teos_muumika_fi(), en: self.teos_muumika_en()}),
                sirkka:self.teos_sirkka(),
                esitys:self.teos_esitys(),
                muutjasenet:self.teos_muutjasenet(),
                julkaistu:self.teos_julkaistu(),
                kunta:self.teos_kunta(),
                koreografi:self.teos_koreografi(),
                tuottaja:self.teos_tuottaja(),
                paikka:self.teos_paikka(),
                group_id:parseInt(self.teos_group_id()),
                promo:self.teos_promo(),
                limited:parseInt(self.teos_limited)
            }

            // var data = new FormData();
            // data.append('file',self.fileInput.data)
            // data.append('json', JSON.stringify(self.uusi))

            console.log(JSON.stringify(self.uusi))

        if(ID > 0) {

            Api('PATCH','teos/'+TID, self.uusi, function(data){
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
            })

        } else {

      //   $.ajax({
      //   method: 'PUT',
      //   url: baseURL+'teos',
      //   async: false,
      //   //dataType: "json",
      //   contentType: "multipart/form-data; charset='utf-8'",
      //   //data: data,
      //   data: JSON.stringify(self.uusi),
      //   success: function (data) {
      //       console.log(data)
            
      //   },
      //   error: function (xhr, type, exception) {
      //       console.log(xhr.responseText)
      //   }
      // });


            Api('POST','teos/',self.uusi, function(data){
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.href = 'teokset.html';
                }, 2000)
            })            
            
        }
    }

}




var Teokset = function() {

    //ko.virtualElements.allowedBindings.group_id = true;

    Lookup('ryhma')

    var self = this;


    if(UGID == AGID ) {
        self.admin = ko.observable(true)
    } else {
        self.admin = ko.observable(false)
    }

    var group = ''

    if(GID) {
        var group = '&where={"group_id":'+GID+',"julkaistu":1}'
    }

    self.teokset = ko.observableArray();

    Api('GET','teos?sort=nimi'+group, null, function(data){

        //console.log(data)

        //self.artists.pushAll(data._items)

            $.each(data, function(index, item) {
                self.teokset.push(
                    ko.mapping.fromJS(item)
                )
            })     
    })


    self.saveRow = function(row) {

        patch = {group_id:parseInt(row.group_id())}

        Api('PATCH','teos/'+row._id(), patch, function() {
                console.log('saved')
            })

        // var user = $.ajax({
        //     method: 'PATCH',
        //     url: baseURL + 'users/'+row._id(),
        //     //async: false,
        //     dataType: "json",
        //     contentType: "application/json",
        //     data: JSON.stringify(patch),
        //     success: function (user) {
        //         },
        //     error: function (xhr, type, exception) {
        //         console.log(xhr)

        //         }
        //     });

        return true;
    }

    self.delRow = function(row) {

        Api('DELETE','teos/'+row._id(), null, function(){
            console.log('Deleted')
        })

        // var user = $.ajax({
        //     method: 'DELETE',
        //     url: baseURL + 'users/'+row._id(),
        //     //async: false,
        //     dataType: "json",
        //     contentType: "application/json",
        //     //data: JSON.stringify(patch),
        //     success: function (user) {
        //         console.log('del:'+row._id())
        //         },
        //     error: function (xhr, type, exception) {
        //         console.log(xhr)

        //         }
        //     });

        return true;
    }


   self.addRow = function() {

   }

    self.saveRow = function () {

    }

}

$(document).one('click','#ui-id-2', function() {
    // ko.applyBindings(new Teokset(), $('#teokset')[0]);

    ko.applyBindings(new Teos(), $('#teos')[0]);


})
 


 