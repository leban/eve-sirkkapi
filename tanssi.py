# -*- coding: utf-8 -*-

"""
    Eve Demo
    ~~~~~~~~

    A demostration of a simple API powered by Eve REST API.

    The live demo is available at eve-demo.herokuapp.com. Please keep in mind
    that the it is running on Heroku's free tier using a free MongoHQ
    sandbox, which means that the first request to the service will probably
    be slow. The database gets a reset every now and then.

    :copyright: (c) 2016 by Nicola Iarocci.
    :license: BSD, see LICENSE for more details.
"""

import os
from pprint import pprint
 
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import Session
from sqlalchemy import create_engine, MetaData, Table, Column, ForeignKey
from sqlalchemy import * #Table, models.Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship

from eve import Eve
# import settings

from eve_sqlalchemy import SQL
from eve_sqlalchemy.decorators import registerSchema

# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText

from flask import Response

from openpyxl import Workbook

import datetime

wb = Workbook()

SIRKKA = create_engine(
                "mysql+pymysql://sirkusinfo:Cr2xvm8Pzs4KPDjA@localhost/tanssidb",
            )

session = Session(SIRKKA)

metadata = MetaData()

metadata.reflect(SIRKKA)

Base = automap_base(metadata=metadata)

# +--------------------------+
# | Tables_in_sirkka_dev     |
# +--------------------------+
# | esiintyja                |
# | ryhma                    |
# | sirkka_esitys_agp        |
# | sirkka_esitys_esiintyjat |
# | sirkka_esitys_esityspv   |
# | sirkka_esitys_esityspvm  |
# | sirkka_esitys_main       |
# | sirkka_esitys_sosmed     |
# | sirkka_esitys_verkko     |
# | users                    |
# +--------------------------+

class User(Base):
    __tablename__ = 'users'
    @hybrid_property
    def _id(self):
        """
        Eve backward compatibility
        """
        return self.id

class Toimijat(Base):
    __tablename__ = 'esiintyja'
    @hybrid_property
    def _id(self):
        """
        Eve backward compatibility
        """
        return self.id

class Ryhma(Base):
    __tablename__ = 'ryhma'
    # @hybrid_property
    # def _id(self):
    #     """
    #     Eve backward compatibility
    #     """
    #     return self.id

class Teos(Base):
    __tablename__ = 'sirkka_esitys_main'
    # @hybrid_property
    # def _id(self):
    #     """
    #     Eve backward compatibility
    #     """
    #     return self.id

class Esitys(Base):
    __tablename__ = 'sirkka_esitys_esityspvm'

    #@hybrid_property
    # def _id(self):
    #     """
    #     Eve backward compatibility
    #     """
    #     return self.id   

class Venue(Base):
    __tablename__ = 'sirkka_esitys_esityspv'
    # @hybrid_property
    # def _id(self):
    #     """
    #     Eve backward compatibility
    #     """
    #     return self.id 

# class Esiintyjat(Base):
#     __tablename__ = 'sirkka_esitys_esiintyjat'
#     @hybrid_property
#     def _id(self):
#         """
#         Eve backward compatibility
#         """
#         return self.id 

# class Roolit(Base):
#     __tablename__ = 'sirkka_esitys_agp'
#     @hybrid_property
#     def _id(self):
#         """
#         Eve backward compatibility
#         """
#         return self.id 

# class Some(Base):
#     __tablename__ = 'sirkka_esitys_sosmed'
#     @hybrid_property
#     def _id(self):
#         """
#         Eve backward compatibility
#         """
#         return self.id 

# class WWW(Base):
#     __tablename__ = 'sirkka_esitys_verkko'
#     @hybrid_property
#     def _id(self):
#         """
#         Eve backward compatibility
#         """
#         return self.id 

class Talous(Base):
    __tablename__ = 'talous'
    # @hybrid_property
    # def _id(self):
    #     """
    #     Eve backward compatibility
    #     """
    #     return self.id 

Table('sirkka_esitys_main', metadata, 
                Column('_id', Integer, primary_key=True),
                extend_existing=True,
            )

Table('sirkka_esitys_esityspv', metadata, 
                Column('_id', Integer, primary_key=True, unique=False),
                extend_existing=True,
            )

Table('sirkka_esitys_esityspvm', metadata, 
                Column('_id', Integer, primary_key=True, unique=False),
                # Column('main_id',Integer, ForeignKey('sirkka_esitys_main._id')),
                # Column('esid',Integer, ForeignKey('sirkka_esitys_esityspv._id')),
                extend_existing=True,
            )

# Table('sirkka_esitys_esiintyjat', metadata, 
#                 Column('id', Integer, primary_key=True),
#                 extend_existing=True,
#             )

# Table('sirkka_esitys_agp', metadata, 
#                 Column('id', Integer, primary_key=True),
#                 extend_existing=True,
#             )

# Table('sirkka_esitys_sosmed', metadata, 
#                 Column('id', Integer, primary_key=True),
#                 extend_existing=True,
#             )

# Table('sirkka_esitys_verkko', metadata, 
#                 Column('id', Integer, primary_key=True),
#                 extend_existing=True,
#             )

Table('talous', metadata, 
                Column('_id', Integer, primary_key=True),
                extend_existing=True,
            )

Base.prepare(SIRKKA, reflect=True)

# User = Base.classes.users
# Esiintyja = Base.classes.esiintyja
# Ryhma = Base.classes.ryhma
# Esitys = Base.classes.sirkka_esitys_main
# Esitys_esiintyjat = Base.classes.sirkka_esitys_esiintyjat
# Esitys_esitykset = Base.classes.sirkka_esitys_esityspv
# Esitys_venue = Base.classes.sirkka_esitys_esityspvm
# Esitys_roolit = Base.classes.sirkka_esitys_agp
# Esitys_some = Base.classes.sirkka_esitys_sosmed
# Esitys_www = Base.classes.sirkka_esitys_verkko

registerSchema('users')(User)
registerSchema('toimijat')(Toimijat)
registerSchema('ryhmat')(Ryhma)
registerSchema('teos')(Teos)
registerSchema('esitys')(Esitys)
# registerSchema('esiintyjat')(Esiintyjat)
registerSchema('esitys_venue')(Venue)
# registerSchema('esitys_roolit')(Roolit)
# registerSchema('esitys_some')(Some)
# registerSchema('esitys_www')(WWW)
registerSchema('talous')(Talous)

# Heroku support: bind to PORT if defined, otherwise default to 5000.
if 'PORT' in os.environ:
    port = int(os.environ.get('PORT'))
    # use '0.0.0.0' to ensure your REST API is reachable from all your
    # network (and not only your computer).
    host = '0.0.0.0'
else:
    port = 5001
    host = '0.0.0.0'

SETTINGS = {
    'DEBUG': True,
    'SQLALCHEMY_DATABASE_URI': "mysql+pymysql://sirkusinfo:Cr2xvm8Pzs4KPDjA@localhost/tanssidb",
    'DOMAIN': {
        'users': User._eve_schema['users'],
        'toimijat': Toimijat._eve_schema['toimijat'],
        'ryhma': Ryhma._eve_schema['ryhmat'],
        'teos': Teos._eve_schema['teos'],
        'esitys': Esitys._eve_schema['esitys'],
        # 'esiintyjat': Esiintyjat._eve_schema['esiintyjat'],
        # 'roolit': Roolit._eve_schema['esitys_roolit'],
        'venue': Venue._eve_schema['esitys_venue'],
        # 'esitys_some': Some._eve_schema['esitys_some'],
        # 'esitys_www': WWW._eve_schema['esitys_www'],
        'talous': Talous._eve_schema['talous'],
        },
    'RESOURCE_METHODS': ['GET', 'POST'],
    'ITEM_METHODS': ['GET', 'PATCH', 'PUT', 'DELETE'],
    #'ID_FIELD':'_id',
    #'ITEM_LOOKUP_FIELD': '_id',
    'EXTRA_RESPONSE_FIELDS': [],
    'X_DOMAINS' : '*',
    'X_HEADERS' : ['Content-Type','Access-Control-Allow-Origin'],
    'XML': False,
    'IF_MATCH' : False,
    'TRANSPARENT_SCHEMA_RULES':True,
    'PAGINATION ': False,
    'PAGINATION_DEFAULT':999  
}    

SETTINGS['DOMAIN']['esitys']['schema'].update({
           u'_id': {'required': False,
                   'type': 'integer',
                   'unique': False},
           },
    )


SETTINGS['DOMAIN']['esitys']['schema'].update({
              'main_id': {
                 'type': 'integer',
                 'data_relation': {
                     'resource': 'teos',
                     'field': '_id',
                     'embeddable': True,
                 },
              },
           },
    )

SETTINGS['DOMAIN']['esitys']['schema'].update({
              'esid': {
                 'type': 'integer',
                 'data_relation': {
                     'resource': 'venue',
                     'field': '_id',
                     'embeddable': True,
                 },
              },
           },
    )

SETTINGS['DOMAIN']['esitys']['schema'].update({
              'group_id': {
                 'type': 'integer',
                 'data_relation': {
                     'resource': 'ryhma',
                     'field': '_id',
                     'embeddable': True
                 },
              },
           },
    )

SETTINGS['DOMAIN']['talous']['schema'].update({
           u'_id': {'required': False,
                   'type': 'integer',
                   'unique': False},
           },
    )

SETTINGS['DOMAIN']['talous']['schema'].update({
              'group_id': {
                 'type': 'integer',
                 'data_relation': {
                     'resource': 'ryhma',
                     'field': '_id',
                     'embeddable': True
                 },
              },
           },
    )

SETTINGS['DOMAIN']['teos']['schema'].update({
           u'_id': {'required': False,
                   'type': 'integer',
                   'unique': False},
           },
    )

SETTINGS['DOMAIN']['venue']['schema'].update({
           u'_id': {'required': False,
                   'type': 'integer',
                   'unique': False},
           },
    )

# SETTINGS['DOMAIN']['esiintyjat']['schema'].update({
#               'esid': {
#                  'type': 'objectid',
#                  'data_relation': {
#                      'resource': 'teos',
#                      'field': '_id',
#                      'embeddable': True
#                  },
#               },
#            },
#     )

# SETTINGS['DOMAIN']['esiintyjat']['schema'].update({
#            u'id': {'required': False,
#                    'type': 'integer',
#                    'unique': False},
#            },
#     )

# SETTINGS['DOMAIN']['roolit']['schema'].update({
#               'esid': {
#                  'type': 'objectid',
#                  'data_relation': {
#                      'resource': 'teos',
#                      'field': '_id',
#                      'embeddable': True
#                  },
#               },
#            },
#     )

# SETTINGS['DOMAIN']['roolit']['schema'].update({
#            u'id': {'required': False,
#                    'type': 'integer',
#                    'unique': False},
#            },
#     )

# SETTINGS['DOMAIN']['venue']['schema'].update({
#               'main_id': {
#                  'type': 'objectid',
#                  'data_relation': {
#                      'resource': 'teos',
#                      'field': 'id',
#                      'embeddable': True
#                  },
#               },
#            },
#     )

# class Eve(Eve):
#     def validate_schema(self, resource, schema):
#         pass

# app = EveSQL(auth=None, validator=ValidatorSQL, data=SQL, settings=SETTINGS)

app = Eve(data=SQL,settings=SETTINGS)


@app.route('/mail/<email>')
def password_mail(email):

    user = db.session.query(Ryhma).filter(Ryhma.email == email)
    if(user):
        key = user[0].password.split(':')[0]

    password_message = u"Vaihda salasanasi: http://tanssi.sirkusinfo.fi/toimija.html?email=%s&key=%s"%(email,key)
    msg = MIMEText(password_message, 'plain', _charset='utf-8')
    msg['Subject'] = 'Uusi salasana tilastot.danceinfo.fi' 
    msg['From'] = me = 'info@danceinfo.fi'
    msg['To'] = email

    # Send the message via our own SMTP server, but don't include the
    # envelope header.
    s = smtplib.SMTP('localhost')
    s.sendmail(me, [email], msg.as_string())
    s.quit()
    return 'OK'



@app.route('/tilastot/<year>')
def generate(year):

    year = int(year)

    ws0 = wb.active
    ws0.title=u'Esitykset ja katsojat'
    ws1 = wb.create_sheet(title=u'Yleisötyö ja yhteisötanssi')
    ws2 = wb.create_sheet(title=u'Tanssielokuvanäytäntö')
    ws3 = wb.create_sheet(title=u'Kv esitykset maittain')
    ws4 = wb.create_sheet(title=u'Talous-tulot')
    ws5 = wb.create_sheet(title=u'Talous-menot')

    tyypit = [u"Kotimaan esitys",
            u"Kv esitys",
            u"Kotimainen vieras",
            u"Ulkom. vieras",
            u"Yhteisötanssi",
            u"Yleisötyö",
            u"Tanssielokuvanäytäntö"]


    ws0.append(['Organisaation nimi',  
            'Kantaesit.',
            'Kot. esit.','','',   
            'Kv esit.','','', 
            'Kot. ja kv esit. yht.','','',    
            'Kot. esitysten kats.','','',  
            'Kv esitysten kats.','','',   
            'Kot. ja kv esit. kats. yht.','','',    
            'Kotim. vierailuesit.','','', 
            'Ulkom. vierailuesit.','','', 
            'Koti- ja ulkomaiset vierailut. yht.','','',   
            'Kotim. vier.es. kats.','','',    
            'Ulkom. vier.es. kats.','','',    
            'Kotim. ja ulkom. vier. es. kats. yht.','','',    
            'KAIKKI ESITYKSET','','',
            'KAIKKI KATSOJAT''','',
            ])

    ws0.append(['','','Maks.','Ilm.','Yht.','Maks.','Ilm.','Yht.','Maks.','Ilm.','Yht.','Maks.','Ilm.','Yht.',
        'Maks.','Ilm.','Yht.','Maks.','Ilm.','Yht.','Maks.','Ilm.','Yht.','Maks.','Ilm.','Yht.','Maks.'
        ,'Ilm.','Yht.','Maks.','Ilm.','Yht.','Maks.','Ilm.','Yht.','Maks.','Ilm.','Yht.','Maks.','Ilm.'
        ,'Yht.','Maks.','Ilm.','Yht.'])

    ws1.append(['Organisaation nimi','Yleisötyö','Yhteisötanssi'])
    ws1.append(['','Tapahtumakerrat','Osallistujamäärä'])

    ws2.append(['Organisaation nimi','Maksulliset näytännöt','Ilmaisnäytännöt', 'Näytäntökerrat yhteensä', 'Myydyt liput','Ilmaiskatsojat','Katsojat yhteensä'])

    stat = db.session.execute(
        "select e.tyyppi as tyyppi, r.nimi as nimi, count(e.kantaes) as kanta, sum(e.kpl) as makses, sum(e.ilmkpl) as ilmes, sum(e.ilmaiset) as ilm, sum(e.myyty) as myyty\
        from sirkka_esitys_esityspvm as e, ryhma as r \
        where e.group_id = r._id and vuosi = %s group by e.group_id,e.tyyppi;"%(year)
    )

    prev = ''
    item = []

    for row in stat:
        if row[0] not in [u'Yleisötyö',u'Yhteisötanssi',u'Tanssielokuvanäytäntö']:
            if prev != row[1]:
                ws0.append(item)
                item = []
                for i in range(0,44):
                    item.append(0)

                item[0] = row[1]
                item[1] = row[2]

            if row[0] == u'Kotimaan esitys':
                item[2] = (row[3]) 
                item[3] = (row[4])
                item[4] = (row[3]+row[4])
                item[11] = (row[5])
                item[12] = (row[6])
                item[13] = (row[5]+row[6])
            if row[0] == u'Kv esitys':
                item[5] = (row[3])
                item[6] = (row[4])
                item[7] = (row[3]+row[4])
                item[14] = (row[5])
                item[15] = (row[6])
                item[16] = (row[5]+row[6])

            #Kot. ja kv esit. yhteensä
                item[8] = item[2] + item[5]
                item[9] = item[3] + item[6]
                item[10] = item[4] + item[7]
                
            # Kot. ja kv esit. kats. yht.
                item[17] = item[11] + item[14]
                item[18] = item[12] + item[15]
                item[19] = item[13] + item[16] 

            if row[0] == u'Kotimainen vieras':
                item[20] = (row[3])
                item[21] = (row[4])
                item[22] = (row[3]+row[4])
                item[29] = (row[5])
                item[30] = (row[6])
                item[31] = (row[5]+row[6])
            if row[0] == u'Ulkom. vieras':
                item[23] = (row[3])
                item[24] = (row[4])
                item[25] = (row[3]+row[4])
                item[32] = (row[5])
                item[33] = (row[6])
                item[34] = (row[5]+row[6])

            # Koti- ja ulkomaiset vierailut. yht.    
                item[26] = item[20] + item[23]
                item[27] = item[21] + item[24]
                item[28] = item[22] + item[25]

            # Kotim. ja ulkom. vier. es. kats. yht.
                item[35] = item[29] + item[32]
                item[36] = item[30] + item[33]
                item[37] = item[31] + item[34]

            # KAIKKI ESITYKSET
                item[38] = item[8] + item[26]
                item[39] = item[9] + item[27]
                item[40] = item[10] + item[28]

            # KAIKKI KATSOJAT
                item[41] = item[17] + item[35]
                item[42] = item[18] + item[36]
                item[43] = item[19] + item[37]

        if row[0] in [u'Yleisötyö',u'Yhteisötanssi']:
            if prev != row[1]:
                ws1.append(item)
                item = []
                for i in range(0,5):
                    item.append(0)
                item[0] = row[1]
            if row[0] == u'Yleisötyö':
                item[1] = row[4]
                item[2] = row[6]
            if row[0] == u'Yhteisötanssi':
                item[3] = row[4]
                item[4] = row[6]

        if row[0] == u'Tanssielokuvanäytäntö':
            if prev != row[1]:
                ws2.append(item)
                item = []
                for i in range(0,5):
                    item.append(0)
                item[0] = row[1]
                item[1] = row[4]
                item[2] = row[5]
                item[3] = row[4]+row[5]
                item[4] = row[6]
                item[5] = row[7]
                item[6] = row[6]+row[7]

        prev = row[1]

    # ws0.append(['','=COUNT(B3:B)','=SUM(C:C)','=SUM(D:D)'])

    ##### Kv tilastot ####

    ws3.append(['Tanssiryhmä','','Esityskerrat','Katsojat',''])

    KV = db.session.execute(
        "select r.nimi as nimi ,v.maa as maa, sum(e.kpl)+sum(e.ilmkpl) as esitykset, sum(e.ilmaiset)+sum(e.myyty) as yleiso  \
            from  sirkka_esitys_esityspvm as e, ryhma as r, sirkka_esitys_esityspv as v  \
            where vuosi = %s and e.tyyppi = 'Kv esitys' and e.group_id = r._id and e.esid = v._id and v.maa NOT LIKE 'Suomi' group by e.group_id,v.maa;"%(year)
    )

    for row in KV:
        if prev != row[0]:
            ws3.append([row[0],'','','',''])
        ws3.append([row[1],row[2],'',row[3],''])

        prev = row[0]

    ###### tulot ######

    ws4.append(['Organisaation nimi','ALV','Omat tulot','','','','','','','','','','Avustukset','','','','','','','','Tulot ilman muita avustuksia','KAIKKI TULOT YHTEENSÄ'])
    ws4.append(['','','Pääsylipputulot','','','Esityspalkkiot','','','Muut tulot','','','Omat tulot yhteensä','Valton toiminta-avustus','Kunnan avustus','','','Muut avustukset','','','','',''])
    ws4.append(['','','Kotimaa','Ulkomaa','Yhteensä','Kotimaa','Ulkomaa','Yhteensä','Kotimaa','Ulkomaa','Yhteensä','','','Toiminta-avustus','Vuokra-avustus','Kunnan avustus yhteensä','Valtion ja kunnan muut avustukset','Muut avustukset (esim. säätiöt','Muut avustukset yhteensä','Kaikki avustukset yhteensä','',''])

    #### menot ####

    ws5.append(['Organisaation nimi','Henkilöstömeno','','','Kiinteistömenot','Toimintamenot','KAIKKI MENOT YHTEENSÄ','Poistot','Tilikauden tulos'])
    ws5.append(['','Palkat','Työnantajamaksut','Yhteensä','','','','','',])


    ecos = db.session.execute(
        "select r.nimi, t.alv, t.liput_kotim, t.liput_ulkom, t.palk_kotim, t.palk_ulkom, t.muut_kotim, t.muut_ulkom, \
                t.avustus_valtio, t.avustus_kunta, t.avustus_vuokra, t.avustus_avustus, t.avustus_muut, \
                t.palkat, t.maksut, t.vuokrat, t.kulut, t.hallinto, t.muut, t.poistot \
        from ryhma as r, talous as t where t.vuosi = %s and t.group_id = r._id; "%(year-1)
    )
     

    for row in ecos:
        ws4.append([
            row[0],         #0 nimi
            row[1],         #1 alv
            row[2],         #2 liput kotim
            row[3],         #3 liput ulkom
            row[2]+row[3],  #4 liput yht
            row[4],         #5 palk kotim
            row[5],         #6 palk ulkom
            row[4]+row[5],  #7 palk yht
            row[6],         #8 muut kotim
            row[7],         #9 muut ulkom
            row[6]+row[7],  #10 muut yht
            row[2]+row[3]+row[4]+row[5],         #11 omat yht
            row[8],         #12 valt av
            row[9],        #13 kunnan av
            row[10],        #14 vuokra av
            row[9]+row[10],        #15 kunta yht
            row[11],        #16 avustus avustus
            row[12],        #17 avustus muut
            row[11]+row[12], #18 muut yht
            row[8]+row[9]+row[10]+row[11]+row[12], #19 avustukset yhteensä
            row[2]+row[3]+row[4]+row[5]+row[6]+row[7], # 20 tulot ilman avust
            row[2]+row[3]+row[4]+row[5]+row[6]+row[7]+row[8]+row[9]+row[10]+row[11]+row[12], #21 kaikki tulot yht
            ])

        ws5.append([
            row[0], #nimi
            row[13], #palkat
            row[14], #maksut
            row[13]+row[14], #yht
            row[15], #kiinteistö (vuokrat)
            row[16]+row[17]+row[18], #toiminta (kulut+hallinto+muut)
            row[13]+row[14]+row[15]+row[16]+row[17]+row[18], #menot yht.
            row[19], #poistot
            row[2]+row[3]+row[4]+row[5]+row[6]+row[7]+row[8]+row[9]+row[10]+row[11]+row[12]-row[13]-row[14]-row[15]-row[16]-row[17]-row[18], #tulos
            ])



    ##### generate sheets #####

    
    wb.save('/tmp/tempwb.xls')

    file = open('/tmp/tempwb.xls').read()

    return Response(file, mimetype='application/vnd.ms-excel')


# def pre_insert_objectid(resource, items):
  
#   pprint(items)
  
#   if resource == 'esitys':
#     db = app.data.driver

#     main = db.session.query(Teos).filter(Teos._id == items[0].main_id)
#     venue = db.session.query(Venue).filter(Venue._id == items[0].esid)

#     items[0].main_id = main
#     items[0].esid = venue

#     return items
  
#app.on_insert += pre_insert_objectid

# pprint(Talous._eve_schema)

db = app.data.driver
Base.metadata.bind = db.engine
db.Model = Base
db.create_all()

if __name__ == '__main__':
    app.run(host=host, port=port, debug=True)
