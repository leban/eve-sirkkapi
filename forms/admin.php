	<div class="alert alert-success" role="alert" id="OK">
  		<span class="glyphicon glyphicon-check"  aria-hidden="true"></span>
	</div>
	<div id="admin" data-bind="visible:admin">
	        <h2>Sirkka-tietokannan hallinta</h2>
	       	<a href="#userlist">Käyttäjät</a>-<a href="#userform">uusi</a> | 
	        <a href="#venuelist">Esityspaikat</a>-<a href="#venueform">uusi</a> | 
	        <a href="#lajilist">Sirkuslajit</a>-<a href="#lajiform">uusi</a>

	        <h3>Käyttäjät</h3>

	        <div id='userlist'>
	        <ul data-bind="foreach:users">
	        <li><a data-bind="{text:name, click:selectUser}"></a>
	        <a data-bind="click:rmUser"><span class="glyphicons glyphicons-minus-sign"></span></a></li>
	        </ul>
	        </div>
	        <a href="#admin">Alkuun</a>

	        <div id="userform">
	        <h3>Käyttäjän tiedot</h3>
	            <label class="col-sm-2 control-label">Nimi: </label><input class="form-control" data-bind="value: user_name" />
	                    <div class='clear'></div>

	          	<label class="col-sm-2 control-label">Puhelin: </label><input class="form-control" data-bind="value: user_puhelin" />
	          	        <div class='clear'></div>

 				<label class="col-sm-2 control-label">Email: </label><input class="form-control" data-bind="value: user_email" />
 				        <div class='clear'></div>
 				<label class="col-sm-2 control-label">Sirkusryhma: </label><input type="hidden" class="form-control ryhma_id" data-bind="value: user_group_id" />
 				<input type="text" class="ryhma" />
 				        <div class='clear'></div>

	            <label class="col-sm-2 control-label">Käyttäjätunnus: </label><input class="form-control" data-bind="value: user_username" />
	                    <div class='clear'></div>

	            <label class="col-sm-2 control-label">Salasana: </label><input class="form-control" data-bind="value: user_password" />
	                    <div class='clear'></div>

	            <label class="col-sm-2 control-label">Salasana uudestaan: </label><input class="form-control" data-bind="value: user_retype" />
	                    <div class='clear'></div>

	            <button type="button" class="btn btn-primary" data-bind='click: saveUser,disable:!user_username.isValid()||!user_email.isValid()||!user_retype.isValid()'>OK</button>
	        </div>
	        <a href="#admin">Alkuun</a>
	        <div class='clear'></div>

	        
	        <h3>Venuet</h3>

	       	<div id="venuelist">
	        <ul data-bind="foreach:venue">
	        <li><a data-bind="{text:paikka, click:selectVenue}"></a>
	        <a data-bind="click:rmVenue"><span class="glyphicons glyphicons-minus-sign"></span></a></li>

	        </ul>
	        </div>
	        <a href="#admin">Alkuun</a>
	        <div id="venueform">
	        <label class='col-sm-2 control-label'>Paikka</label><input type='text' class='form-control' data-bind='value:venue_paikka' />
	        	                    <div class='clear'></div>
				<label class='col-sm-2 control-label'>Osoite</label><input type='text' class='form-control' data-bind='value:venue_osoite' />	                    <div class='clear'></div>
				<label class='col-sm-2 control-label'>Postinro</label><input type='text' class='form-control' data-bind='value:venue_postinro' />
					                    <div class='clear'></div>
				<label class='col-sm-2 control-label'>Kaupunki</label><input type='text' class='form-control' data-bind='value:venue_kaupunki' />
					                    <div class='clear'></div>
				<label class='col-sm-2 control-label'>Maa</label><input type='text' class='form-control country' data-bind='value:venue_maa' />	                    <div class='clear'></div>
				<label class='col-sm-2 control-label'>Url</label><input type='text' class='form-control' data-bind='value:venue_url' />
					                    <div class='clear'></div>
		    	<button class="btn btn-primary pull-xs-right" data-bind='click: saveVenue' type='submit'>OK
		    	</button>	    
			</div>
			<a href="#admin">Alkuun</a>



	        <h3>Sirkuslajit</h3>
	       	<div id="lajilist">
	        <ul data-bind="foreach:lajit">
	        <li><a data-bind="{text:fi,click:selectLaji}"></a>
	        <a data-bind="click:rmLaji"><span class="glyphicons glyphicons-minus-sign"></span></a></li>
	        </ul>
	        </div>
	       <a href="#admin">Alkuun</a>

	        <div id="lajiform">
	        <label class='col-sm-2 control-label'>FI</label><input type='text' class='form-control' data-bind='value:laji_fi' />
	        	                    <div class='clear'></div>
	        <label class='col-sm-2 control-label'>EN</label><input type='text' class='form-control' data-bind='value:laji_en' />
	        	                    <div class='clear'></div>
		    <div>
		   	<button class="btn btn-primary pull-xs-right" data-bind='click: saveLaji' type='submit'>OK
		    	</button>	
            <br>
            <a href="#" id="logout" class="btn btn-warning">Logout </a></div>
			</div>
	        <a href="#admin">Alkuun</a>

		</div>

<script src="http://maps.google.com/maps/api/js"></script>

<script type="text/javascript">

	var Admin = function() {

	Lookup('ryhma')

    // if(UGID == AGID ) {
    //     self.admin = ko.observable(true)
    // } else {
    //     self.admin = ko.observable(false)
    // }

    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghijklmnoprstuvwz";
    var string_length = 32;
    var salt = "";

    // Lookup('ryhma');

    for(var i=0; i<string_length; i++){
      var randomPos = Math.floor( Math.random() * chars.length );
      salt += chars.substr(randomPos, 1);
    }

    if(UGID == AGID) {
        self.admin = ko.observable(true)
        } else {
        self.admin = ko.observable(false)
      }

	

	/*** users ***/


	self.users = ko.observableArray([])

      self.user_name = ko.observable('').extend({ required: true })
      self.user_puhelin = ko.observable()
      self.user_username = ko.observable('').extend({ required: true })
      self.user_email  = ko.observable('').extend({email:true})
      self.user_password  = ko.observable('').extend(
                  { passwordComplexity:
                      { onlyIf: function() { return self.user_password != ''}}
                  })            
      self.user_retype = ko.observable('').extend({ areSame:self.user_password });
      self.user_group_id = ko.observable(0).extend({ required: true })
      self.user_id = ko.observable()

	ko.computed(function(){
		return Api('GET','users?sort=name', null, function(data) {
			self.users.pushAll(data)
		})
	},self)

	self.selectUser = function(row) {
		console.log(row)
		self.user_name(row.name)
		self.user_puhelin(row.puhelin)
		self.user_username(row.username)
		self.user_email(row.email)
		self.user_group_id(row.group_id)
		self.user_id(row._id)
		location.href = "#userform"

	}


	self.rmUser = function(row){
		Api('DELETE','users/'+row._id, null, function(){
			self.users.remove(row)
			console.log('deleted')
		})

	}

    self.saveUser = function () {

            // console.log(self.group_id())

            var md5pass = $.md5(self.user_password()+salt)

            self.newuser = {
                    name: self.user_name(),
                    puhelin: self.user_puhelin(),
                    username: self.user_username(),
                    email: self.user_email(),
                    password: md5pass+':'+salt,
                    group_id: parseInt(self.user_group_id())
            }

            if(self.user_id() > 0) {

            Api('PATCH','users/'+self.user_id(), self.newuser, function() {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.href= '#userlist';
                }, 2000)
            })

        } else {

        	Api('POST','users', self.newuser, function() {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.href= '#userlist';
                }, 2000)
            })


        }
        
    }


	/**** venuet ***/

	self.venue = ko.observableArray([])

	self.venue_maa=ko.observable('Suomi');
    self.venue_kaupunki=ko.observable('');
    self.venue_paikka=ko.observable('').extend({ required: true })
    self.venue_url=ko.observable('');
    self.venue_lon=ko.observable('');
    self.venue_lat=ko.observable('');
    self.venue_osoite=ko.observable('');
    self.venue_postinro=ko.observable('');
	self.venue_id = ko.observable()

	ko.computed(function(){
		return Api('GET','venue?sort=paikka', null, function(data) {
			self.venue.pushAll(data)
		})
	},self)

	self.selectVenue = function(row) {
		self.venue_maa(row.maa)
		self.venue_kaupunki(row.kaupunki)
		self.venue_paikka(row.paikka)
		self.venue_url(row.url)
		self.venue_lon(row.lat)
		self.venue_lat(row.lon)
		self.venue_osoite(row.osoite)
		self.venue_postinro(row.postinro)
		self.venue_id(row._id)
		location.href='#venueform'
	}

	self.rmVenue = function(row){
		Api('DELETE','venue/'+row._id, null, function(){
			self.venue.remove(row)
			console.log('deleted')
		})

	}


    self.saveVenue = function () {

            //https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=YOUR_API_KEY

    var geocoder = new google.maps.Geocoder();
    var address = self.venue_osoite()+','+self.venue_postinro()+','+self.venue_kaupunki()+','+self.venue_maa();

   if (geocoder && google) {

      geocoder.geocode({ 'address': address }, function (results, status) {

         if (status == google.maps.GeocoderStatus.OK) {

            console.log(results[0].geometry.location.lat)
         }
         else {

            console.log("Geocoding failed: " + status);
         }
      });
   	}    

            // var latlon = $.ajax({
            //         method: 'get',
            //         url: 'https://maps.googleapis.com/maps/api/geocode/json?address='+self.osoite()+','+self.postinro()+','+self.kaupunki()+','+self.maa(),
            //         //async: false,
            //         dataType: "json",
            //         contentType: "application/json",
            //         //data: JSON.stringify(patch),
            //         success: function (data) {
            //             console.log(data)
            //             },
            //         error: function (xhr, type, exception) {
            //             console.log(url)
            //             console.log(xhr)

            //             }
            //         });

            self.newvenue = {
                maa:self.venue_maa(),
                kaupunki:self.venue_kaupunki(),
                paikka:self.venue_paikka(),
                // pvm:self.pvm(),
                // ensiilta:self.ensiilta(),
                url:self.venue_url(),
                lon:self.venue_lon(),
                lat:self.venue_lat(),
                osoite:self.venue_osoite(),
                postinro:self.venue_postinro(),
            }


        if(self.venue_id()) {

            Api('PATCH','venue/'+self.venue_id(), self.newvenue, function() {
            	location.href="#venuelist"
	            })


        } else {

            Api('POST','venue/', self.newvenue, function() {
            })

            
        }
    }

	/*** sirkuslajit ***/

	self.lajit = ko.observableArray([])

	self.laji_fi = ko.observable();
	self.laji_en = ko.observable();
	self.laji_id = ko.observable();

	ko.computed(function(){
		return Api('GET','sirkuslajit?sort=fi', null, function(data) {
			self.lajit.pushAll(data)
		})
	},self)

	self.selectLaji = function(row) {
		self.laji_fi(row.fi)
		self.laji_en(row.en)
		self.laii_id(row._id)
		location.href="#lajiform"
	}

	self.rmLaji = function(row){
		Api('DELETE','sirkuslajit/'+row._id, null, function(){
			self.lajit.remove(row)
			console.log('deleted')
		})

	}

	self.saveLaji = function(row) {

		self.newlaji = {
			fi:self.laji_fi,
			en:self.laji_en
		}


        if(self.laji_id()) {

            Api('PATCH','sirkuslajit/'+self.laji_id(), self.newlaji, function() {
            	location.href="#lajilist"
	            })


        } else {

            Api('POST','sirkuslajit/', self.newlaji, function() {
            })

            
        }
	}


}

$(document).one('click','#ui-id-5', function() {

   ko.applyBindings(new Admin(), $('#admin')[0]);
})

</script>

