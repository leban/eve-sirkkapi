	<body>
		<div class="alert alert-success" role="alert" id="OK">
  		<span class="glyphicon glyphicon-check"  aria-hidden="true"></span>
	</div>
	<div class="container">
	        <span class="taho col-lg-8 table table-striped form form-horizontal" id="uusi"><br>
	        <a data-bind="attr: { 'href': 'toimija.html?ID='+UID }" class="addgroup btn btn-info">Oma profiili
	        </a>
	        <a data-bind="attr: { 'href': 'sirkka.html?GID='+group_id() }" class="addgroup btn btn-info">Esityskalenteri</a>	    
	        <a data-bind="attr: { 'href': 'teokset.html?GID='+group_id() }" class="addgroup btn btn-info">Teostiedot</a>
	        <a data-bind="attr: { 'href': 'ryhma.html?GID='+group_id(),'visible':UGID > 0 }" class="addgroup btn btn-info">Sirkusryhma</a>
	        <h3>Käyttäjän tiedot</h3>
	            <label class="control-label">Nimi(Name)* </label><input class="form-control" data-bind="value: name" />
	          	<label class="control-label">Puhelin(Phone)* </label><input class="form-control" data-bind="value: puhelin" />
				<label class="control-label">Sirkusryhmä(Circus company)</label><a href="#" data-toggle="tooltip" title="Minulla on muokkausoikeudet tämän ryhmän tietoihin Sirkka-tietokannassa."><span class="glyphicon glyphicon-question-sign"></span></a>
				<br>
 					<div data-bind="if:group_id._id > 0 && admin() ">
					<input type="hidden" data-bind="textInput:group_id._id" class="ryhma_id" />
					<input class="ryhma form-control" data-bind="value:group_id.nimi" /><br>
					</div>
 					<div data-bind="if:!group_id && admin() ">
					<input type="hidden" data-bind="textInput:group_id" class="ryhma_id" />
					<input class="ryhma form-control"/><br>
					</div>

 				<label class="control-label">Sähköposti(Email)* </label><input class="form-control" data-bind="value: email" />
	            <label class="control-label">Käyttäjätunnus(User name)* </label><input class="form-control" data-bind="value: username" />
	            <label class="control-label">Salasana(Password) </label><input class="form-control" data-bind="value: password" />
	            <label class="control-label">Salasana uudestaan(Retype passsword)</label><input class="form-control" data-bind="value: retype" /><br>
	            <button type="button" class="btn btn-primary" title="Tallenna(Save)" data-bind='click: saveUusi,disable:!username.isValid()||!email.isValid()||!retype.isValid()'>OK</button>
	            <br><br>
	            <div>
            	<a href="#" id="logout" title="Kirjaudu ulos" class="btn btn-warning">Logout </a></div>

	    </span>
	
	</div>