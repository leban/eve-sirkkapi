<?php
	$group_id = $_GET["group_id"];	
?>
	<div class="alert alert-success" role="alert" id="OK">
  		<span class="glyphicon glyphicon-check"  aria-hidden="true"></span>
	</div>

	<div class=" col-sm-12" id="group">
<!-- 	                  <a data-bind="attr:{ 'href':'user.html' }" class="addgroup btn btn-info">Käyttäjätili</a>	  
	                  <a data-bind="attr:{ 'href':'toimija.html?sirkka_id='+UID }" class="addgroup btn btn-info">Oma profiili</a>
          <a data-bind="attr:{ 'href':'ryhma.html?sirkka_gid='+group_id() }" class="addgroup btn btn-info">Sirkusryhma</a>
          <a data-bind="attr:{ 'href':'teokset.html?sirkka_gid='+UGID }" class="addgroup btn btn-info">Teokset</a>
          <a data-bind="attr:{ 'href':'sirkka.html?sirkka_gid='+UGID }" class="addgroup btn btn-info">Esityskalenteri</a>
 -->	
	        <h2>Sirkusryhmän profiili (Circus company's profile)</h2>
	       Pakolliset kentät on merkitty *:llä (Obligatory fields are marked with *)  <br>
           Tyhjäksi jätettyjä kenttiä ei julkaista (Empty fields are not published)<br>
           Viime päivitys (Last update):<span data-bind="text:ryhma_updated"></span>
			<br>
	        			<div class="checkbox">
			<!-- <input type='hidden' class='form-control' data-bind='value:ryhma_userid' /> -->
			<label><input type='checkbox' data-bind='checked:ryhma_julkaistu' />Julkaistu (Published)*</label></div>

	        <label class=' '>Ryhmän nimi (Name of the company)*</label><input type='text' class='form-control' data-bind='value:ryhma_nimi' />
			<label class=' '>Verkkosivut (Websites) <a href="#" data-toggle="tooltip" title="WWW, Youtube, Vimeo, Facebook, Twitter yms. linkit  (Links to social media) <br>
			 Erottele osoitteet omille riveille. Seprate into rows.">
			 <span class="glyphicon glyphicon-question-sign"></span></a>
			 </label>
			 <textarea class='form-control' data-bind='value:ryhma_verkkosivu' ></textarea>
			 <label class='  full'>Kuvat (Images)</label>
        <table><tr>
        <td>        <span data-bind="if:ryhma_kuva">
        <img data-bind="attr:{'src': 'http://sirkusinfo.fi/'+ ryhma_kuva().split('/var/www/sirkusinfo').pop() }" width="200px"/>
                <a data-bind="click:rmKuva1"><span class="glyphicons glyphicons-minus-sign"></span></a>
            </span>
        </td>
        <td>        <span data-bind="if:ryhma_kuva2">
        <img data-bind="attr:{'src': 'http://sirkusinfo.fi/'+ ryhma_kuva2().split('/var/www/sirkusinfo').pop() }" width="200px"/>
                <a data-bind="click:rmKuva2"><span class="glyphicons glyphicons-minus-sign"></span></a>
</span>
        </td>
        <td>        <span data-bind="if:ryhma_kuva3">

        <img data-bind="attr:{'src': 'http://sirkusinfo.fi/'+ ryhma_kuva3().split('/var/www/sirkusinfo').pop() }" width="200px"/>
                <a data-bind="click:rmKuva3"><span class="glyphicons glyphicons-minus-sign"></span></a>
</span>
        </td>
        </tr>
        <tr><td>
        <input type='file' class='form-control' data-bind='file:{data: ryhma_fileInput, name: ryhma_kuva}' />
        <label class='  full'>Kuvaaja (Photographer)</label><input type='text' class='form-control' data-bind='value:ryhma_kuvaaja' />
        </td>
        <td>
        <input type='file' class='form-control' data-bind='file:{data: ryhma_fileInput2, name: ryhma_kuva2}' />
        <label class='  full'>Kuvaaja (Photographer)</label><input type='text' class='form-control' data-bind='value:ryhma_kuvaaja2' />
        </td>
        <td>
        <input type='file' class='form-control' data-bind='file:{data: ryhma_fileInput3, name: ryhma_kuva3}' />
        <label class='  full'>Kuvaaja (Photographer)</label><input type='text' class='form-control' data-bind='value:ryhma_kuvaaja3' />
        </td></tr></table>
        
        <label class=' '>Kuvateksti (Caption) </label><textarea class='form-control' data-bind='textInput:ryhma_kuvassa' ></textarea>
			<label class=' '>Kuvaaja (Photographer)</label><input type='text' class='form-control' data-bind='value:ryhma_kuvaaja' />
			<label class=' '>Perustamisvuosi (Year of foundation)</label><input type='text' class='form-control' data-bind='value:ryhma_perustettu' />
			<label class=''>Lakannut toimimasta (Year of closing down)</label><input type='text' class='form-control' data-bind='value:ryhma_loppunut' />
			<label class=' '>Organisaatiomuoto (Organisational form)</label><input type='text' class='form-control organisaatio' data-bind='value:ryhma_organisaatiomuoto' />
			<label class=' '>Yhteyshenkilö (Contact person)</label><textarea class='form-control' data-bind='textInput:ryhma_yhteyshenkilo' ></textarea>

			<a href="#jasenet">Ryhmän jäsenet (Members of company)</a>

			<label class=' full'>Ryhmän kuvaus (Description of the company)</label>
			<div class="tabs-lang">

			<ul class="nav nav-tabs" role="tablist">
			  <li class="nav-item" role="presentation">
			    <a class="nav-link active" data-toggle="tab" href="#kuvaus_fi" role="tab">FI</a>
			  </li>
			  <li class="nav-item" role="presentation">
			    <a class="nav-link" data-toggle="tab" href="#kuvaus_en" role="tab">EN</a>
			  </li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
			  <div class="tab-pane active" id="kuvaus_fi" role="tabpanel">	
			<textarea class='form-control' data-bind='textInput:ryhma_kuvaus_fi' ></textarea></div>
			<div class="tab-pane" id="kuvaus_en" role="tabpanel">	
			<textarea class='form-control' data-bind='textInput:ryhma_kuvaus_en' ></textarea></div>
			</div></div>
			
			<label class=' '>Teosten tyylilajit (Genres of performances)</label>
			<br>
			<!-- <input type='text' class='form-control' data-bind='value:ryhma_tyylilaji' /> -->
				<div class="checks panel panel-default">  
				<div class="panel-body">
				<div class="checkbox-inline">
				<label> <input type='checkbox' data-bind='checked:ryhma_nykysirkus' />Nykysirkus</label></div>
				<div class="checkbox-inline">
				<label> <input type='checkbox' data-bind='checked:ryhma_perinteinen' />Perinteinen</label></div>
				<div class='checkbox-inline'>
				<label> <input type='checkbox' data-bind='checked:ryhma_kokoperhe' />Kokoperhe</label>
				</div>
				<div class='checkbox-inline'>
				<label> <input type='checkbox' data-bind='checked:ryhma_muu' />Muu</label>
				</div>
				</div></div>
				<span data-bind="visible:ryhma_muu">
			<label class='  full'>Mikä? (What?)</label>
			<div class="tabs-lang">
			<ul class="nav nav-tabs" role="tablist">
			  <li class="nav-item">
			    <a class="nav-link active" data-toggle="tab" href="#muumika_fi" role="tab">FI</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" data-toggle="tab" href="#muumika_en" role="tab">EN</a>
			  </li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
			  <div class="tab-pane active sirkuslajit" id="muumika_fi" role="tabpanel">	
			<input type='text' class='form-control' data-bind='value:ryhma_muumika_fi' />
				</div>
			<div class="tab-pane" id="muumika_en" role="tabpanel">	
			<input type='text' class='form-control' data-bind='value:ryhma_muumika_en' />
				</div>
			</div></div></span>
		<label>Tuotannot ja niiden kantaesitysvuosi (Productions and years of premiere)</label> 	
			<div data-bind="foreach:Shows">
				<a data-bind="click:editShow">
					>&nbsp;<span data-bind="text:nimi"></span>
					(<span data-bind="text:sensiilta"></span>)
				</a><br>
			</div>
			<label class=' '>Muut tuotannot</label><textarea class='form-control' data-bind='textInput:ryhma_tuotannot' ></textarea>
			<label class='  full'>Muuta tietoa (Additional information)</label>
			<div class="tabs-lang">
						<ul class="nav nav-tabs" role="tablist">
			  <li class="nav-item">
			    <a class="nav-link active" data-toggle="tab" href="#muita_fi" role="tab">FI</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" data-toggle="tab" href="#muita_en" role="tab">EN</a>
			  </li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
			  <div class="tab-pane active" id="muita_fi" role="tabpanel">	
				<textarea class='form-control' data-bind='textInput:ryhma_muitatietoja_fi' ></textarea></div>
			<div class="tab-pane" id="muita_en" role="tabpanel">	
				<textarea class='form-control' data-bind='textInput:ryhma_muitatietoja_en' ></textarea></div>
			</div>	</div>
			<button type="button" class="btn btn-primary" data-bind="click:saveRow" title="Tallenna (save)">OK</button>
			<br><br>
			<a name="jasenet"></a>
			<h3>Ryhmään kuuluvat jäsenet (Members added to this company)</h3>
			<div data-bind='foreach:Artists'>
			<span data-bind='text:sukunimi'></span>, <span data-bind='text:etunimi'></span>
		    	<a data-bind='click:rm'>
			<span class="glyphicons glyphicons-minus-sign"></span></a>
			</div>
			<label>Lisää uusi jäsen (add new)</label><br>
			<input class="toimijat" data-bind="value:added" />
		    	<a data-bind='click:add'>
			<span class="glyphicons glyphicons-plus-sign"></span></a>
			<br>
			<label class=' '>Muut jäsenet (Other members)</label>
			<textarea class='form-control toimijat' data-bind='textInput:ryhma_jasenet' ></textarea>

	</div>

<script type="text/javascript">
	
/**** EDIT RYHMÄ *****/

var Group = function() {

    Lookup('toimijat')
    
    if(getUrlVars()['group_id']) {
        var GID = parseInt(getUrlVars()['group_id']);
    }

     $(document).on('focusin','input.organisaatio',
        function() {
            var org = new Array([
                    'Yhdistys',
                    'Yritys',
                    'Osuuskunta',
                    'Avoin yhtiö',
                    'Toiminimi',
                    'Yleishyödyllinen yhteisö',
                    'Työryhmä'])

            $('input.organisaatio').typeahead( { 
            source:org,
            updater:function(item) {
                return this.$element.val().replace(/[^,]*$/,'')+item+' ';
            },
            matcher:function (item) {
              var tquery = extractor(this.query);
              console.log(item)
              if(!tquery) return false;
              console.log(~item.indexOf(tquery))
              return ~item.toLowerCase().indexOf(tquery.toLowerCase())
            },
            highlighter:function (item) {
              var query = extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
              return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
                return '<strong>' + match + '</strong>'
                })
              }
             }                                
           );
    })


    /* Taitelijalista */
    self.Artists = ko.observableArray();


    self.rm = function(row) {
        Api('PATCH','toimijat/'+row._id,{
            group_id:null
        }, function(data) {
            self.Artists.remove(row)
        })
    }

    self.added = ko.observable();

    self.add = function() {
        var name = $('input.toimijat').val().split(',')
        Api('GET', 'toimijat?where=sukunimi=='+name[0]+'&where=etunimi=='+name[1], null, function(data){
            console.log(data)
            Api('PATCH','toimijat/'+data[0]._id,
                {group_id:GID}, function(res){

                    Api('GET','toimijat?where=group_id=='+GID+'&sort=sukunimi', null, function(list) {
                        self.Arists.removeAll();
                        self.Arists.pushAll(list)
                    })
                })
        })

    }

    self.editShow = function(row) {
        location.href= '<?php echo site_url(); ?>/sirkus-suomessa/sirkka-tietokanta/muokkaa-teosta?work_id='+row._id;
    }

        /* Teoslista */

    self.Shows = ko.observableArray();
    
    self.ryhma_nimi=ko.observable();
    self.ryhma_nimi_en=ko.observable();
    self.ryhma_fileInput=ko.observable();
    self.ryhma_updated=ko.observable();
    self.ryhma_verkkosivu=ko.observable();
    self.ryhma_kuvassa=ko.observable();
    self.ryhma_kuvaaja=ko.observable();
    self.ryhma_perustettu=ko.observable();
    self.ryhma_organisaatiomuoto=ko.observable();
    self.ryhma_yhteyshenkilo=ko.observable();
    self.ryhma_kuvaus_fi=ko.observable();
    self.ryhma_kuvaus_en=ko.observable();
    self.ryhma_jasenet=ko.observable();
    self.ryhma_tyylilaji=ko.observable();
    self.ryhma_nykysirkus=ko.observable();
    self.ryhma_perinteinen=ko.observable();
    self.ryhma_kokoperhe=ko.observable();
    self.ryhma_muu=ko.observable();
    self.ryhma_muumika_fi=ko.observable();
    self.ryhma_muumika_en=ko.observable();
    self.ryhma_tuotannot=ko.observable();
    self.ryhma_muitatietoja_fi=ko.observable();
    self.ryhma_muitatietoja_en=ko.observable();
    self.ryhma_kuva=ko.observable('');
    self.ryhma_kuva2=ko.observable('');
    self.ryhma_kuva3=ko.observable('');
    self.ryhma_fileInput = ko.observable();
    self.ryhma_fileInput2 = ko.observable();
    self.ryhma_fileInput3 = ko.observable();
    self.ryhma_kuvaaaja=ko.observable('');
    self.ryhma_kuvaaja2=ko.observable('');
    self.ryhma_kuvaaja3=ko.observable('');
    self.ryhma_userid=ko.observable(0);
    self.ryhma_julkaistu=ko.observable(false);
    self.ryhma_loppunut=ko.observable();


    if(GID > 0) {

        Api('GET','toimijat?where=group_id=='+GID+'&sort=sukunimi', null, function(list) {
            self.Artists.pushAll(list)
        })

        Api('GET','teos?where=group_id=='+GID+'&sort=-sensiilta', null, function(list) {
            console.log(list)
            self.Shows.pushAll(list)
        })

    self.ryhma_original = ko.observable();

    ko.computed(function(){

        return Api('GET','ryhma/'+GID, null,function(data) {

            self.ryhma_original(data)

            self.ryhma_nimi(data.nimi);
            self.ryhma_nimi_en(data.nimi_en);
            self.ryhma_updated(data._updated);
            self.ryhma_kuva(data.kuva)
            self.ryhma_verkkosivu(data.verkkosivu);
            self.ryhma_kuvassa(data.kuvassa);
            self.ryhma_kuvaaja(data.kuvaaja);
            self.ryhma_perustettu(data.perustettu);
            self.ryhma_organisaatiomuoto(data.organisaatiomuoto);
            self.ryhma_yhteyshenkilo(data.yhteyshenkilo);
            self.ryhma_kuvaus_fi(JSON.parse(data.kuvaus).fi);
            self.ryhma_kuvaus_en(JSON.parse(data.kuvaus).en);
            self.ryhma_jasenet(data.jasenet);
            self.ryhma_tyylilaji(data.tyylilaji);
            self.ryhma_nykysirkus(data.nykysirkus);
            self.ryhma_perinteinen(data.perinteinen);
            self.ryhma_kokoperhe(data.kokoperhe);
            self.ryhma_muu(data.muu);
            self.ryhma_muumika_fi(JSON.parse(data.muumika).fi);
            self.ryhma_muumika_en(JSON.parse(data.muumika).en);
            self.ryhma_tuotannot(data.tuotannot);
            self.ryhma_muitatietoja_fi(JSON.parse(data.muitatietoja).fi);
            self.ryhma_muitatietoja_en(JSON.parse(data.muitatietoja).en);
            self.ryhma_kuva(data.kuva);
            self.ryhma_kuva2(data.kuva2);
            self.ryhma_kuva3(data.kuva3);
            self.ryhma_kuvaaja(data.kuvaaja);
            self.ryhma_kuvaaja2(data.kuvaaja2);
            self.ryhma_kuvaaja3(data.kuvaaja3);
            self.ryhma_userid(data.userid);
            self.ryhma_julkaistu(data.julkaistu);
            self.ryhma_loppunut(data.loppunut);

        })
    }, self)
}

    self.rmKuva1 = function() {
      self.teos_kuva('');
      self.teos_kuvaaja('')
    }

    self.rmKuva2 = function() {
      self.teos_kuva2('');
      self.teos_kuvaaja2('')
    }

    self.rmKuva3 = function() {
      self.teos_kuva3('');
      self.teos_kuvaaja3('')
    }


    self.saveRow = function () {


            self.uusi = {
                nimi:self.ryhma_nimi(),
                nimi:self.ryhma_nimi_en(),
                verkkosivu:self.ryhma_verkkosivu(),
                kuva:self.ryhma_fileInput(),
                kuvassa:self.ryhma_kuvassa(),
                kuvaaja:self.ryhma_kuvaaja(),
                perustettu:self.ryhma_perustettu(),
                organisaatiomuoto:self.ryhma_organisaatiomuoto(),
                yhteyshenkilo:self.ryhma_yhteyshenkilo(),
                //kuvaus:'{"fi":"'+self.ryhma_kuvaus_fi()+'","en":"'+self.ryhma_kuvaus_en()+'"}',
                kuvaus:ko.toJSON({fi:self.ryhma_kuvaus_fi(), en:self.ryhma_kuvaus_en()}),
                jasenet:self.ryhma_jasenet(),
                tyylilaji:self.ryhma_tyylilaji(),
                nykysirkus:self.ryhma_nykysirkus(),
                perinteinen:self.ryhma_perinteinen(),
                kokoperhe:self.ryhma_kokoperhe(),
                muu:self.ryhma_muu(),
                //muumika:'{"fi":"'+self.ryhma_muumika_fi()+'","en":"'+self.ryhma_muumika_en()+'""}',
                muumika:ko.toJSON({fi:self.ryhma_muumika_fi(), en:self.ryhma_muumika_en()}),
                tuotannot:self.ryhma_tuotannot(),
                //muitatietoja:'{"fi":"'+self.ryhma_muitatietoja_fi()+'","en":"'+self.ryhma_muitatietoja_en()+'"}',
                muitatietoja:ko.toJSON({fi:self.ryhma_muitatietoja_fi(), en:self.ryhma_muitatietoja_en()}),
                kuva:self.ryhma_kuva(),
                userid:parseInt(self.ryhma_userid()),
                julkaistu:self.ryhma_julkaistu(),
                loppunut:self.ryhma_loppunut(),
                kuva: self.ryhma_fileInput(),
                kuva2:self.ryhma_fileInput2(),
                kuva3:self.ryhma_fileInput3(),
                kuvaaja:self.ryhma_kuvaaja(),
                kuvaaja2:self.ryhma_kuvaaja2(),
                kuvaaja3:self.ryhma_kuvaaja3(),

            }

            for(key in self.uusi) {
              if(self.uusi[key] == self.ryhma_original()[key]) {
                delete self.uusi[key]
              }
            }


        if(GID > 0) {


            Api('PATCH','ryhma/'+GID, self.uusi, function(data) {
                $('.alert').toggle();
                    window.setTimeout(function() {
                        location.reload();
                    }, 2000)

                location.reload()
            } )

        }

    }
}


    // ko.applyBindings(new Groups(), $('#groups')[0]);


            ko.applyBindings(new Group(), $('#group')[0]);


 

</script>
