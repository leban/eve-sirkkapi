<?php
	$main_id = $_GET["work_id"];	
?>

      <div class="alert alert-success" role="alert" id="OK">
      <span class="glyphicon glyphicon-check"  aria-hidden="true"></span>
  </div>

	<div class="container col-sm-12">

	        <h2>Sirkusesityksen tiedot (Circus show)</h2>
        <div class="checkbox-inline">
        <label> <input type='checkbox' class="admin" data-bind="{'checked':teos_sirkka }" />Lisää Sirkka-tietokantaan (Add to Sirkka database)</label></div>
        <div class="checkbox-inline">
        <label> <input type='checkbox'  class='admin' data-bind='checked:teos_julkaistu' />Julkaistu (Published) </label></div>
        <span data-bind="visible:admin">
        <div class="checkbox-inline">
        <label> <input type='checkbox'  class='admin' data-bind='checked:teos_promo' />Artists and shows</label></div></span>
        <br>
        <div data-bind="visible:admin">
        <label> Esityskalenterin rajoitus</label>
            <select data-bind="options: limit_options,
                       value: teos_limited,
                       optionsCaption: 'Valitse'">    
                       </select>

      </div>       
        <label class='  full'>Esityksen nimi (Title of the show)*
         <a href="#" data-toggle="tooltip" title="Mikäli teoksella on käytössä  myös englanninkielinen nimi, kirjoita se tähän. Mutta älä käännä teosnimeä englanniksi vain tätä tietokantaa varten!">
       <span class="glyphicon glyphicon-question-sign"></span></a> 
    </label>
    <div class="tabs-lang">

        <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#nimi_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#nimi_en" role="tab">EN</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="nimi_fi" role="tabpanel">  
      <input class='form-control' type="text" data-bind='textInput:teos_nimi' /></div>
      <div class="tab-pane" id="nimi_en" role="tabpanel"> 
      <input class='form-control' type="text" data-bind='textInput:teos_nimi_en' /></div>
      </div></div>

      <input type="hidden" class="ryhma_id" data-bind="group_id" />
        <label class='  full'>Ryhmä tai esittäjä (Artist’s or company’s name)*
        <a href="#" data-toggle="tooltip" title=" Jos kyseessä yhteistuotanto, jokainen yhteistuottaja omalle rivilleen.
 In case of co-production, give name of each production partner separately.">
       <span class="glyphicon glyphicon-question-sign"></span></a> 
       </label>
       <input type="text" class='form-control ryhma' data-bind='value:teos_esittaja'/>
              <label>Sirkusteoksen WWW-osoitteet (Web links)</label>
       <textarea data-bind="textInput:teos_www"></textarea>
        <label>Video-linkki (Video link)</label>
       <input type="text" data-bind="textInput:teos_video" />

        <label class='  full'>Kuvat (Images)</label>
        <table><tr>
        <td ><span data-bind="if:teos_kuva">
        <img data-bind="attr:{'src':'http://sirkusinfo.fi/'+ teos_kuva().split('/var/www/sirkusinfo').pop() }" width="200px"/>
        <a data-bind="click:rmKuva1"><span class="glyphicons glyphicons-minus-sign"></span></a>
        </span>
        </td>
        <td>
        <span data-bind="if:teos_kuva2">
        <img data-bind="attr:{'src':'http://sirkusinfo.fi/'+ teos_kuva2().split('/var/www/sirkusinfo').pop() }" width="200px"/>
        <a data-bind="click:rmKuva2"><span class="glyphicons glyphicons-minus-sign"></span></a>
        </span>
        </td>
        <td>        
        <span data-bind="if:teos_kuva3">
        <img data-bind="attr:{'src':'http://sirkusinfo.fi/'+ teos_kuva3().split('/var/www/sirkusinfo').pop() }" width="200px"/>
        <a data-bind="click:rmKuva3"><span class="glyphicons glyphicons-minus-sign"></span></a>
        </span>
        </td>
        </tr>
        <tr><td>
        <input type='file' class='form-control' data-bind='file:{data:teos_fileInput, name:teos_kuva}' />
        <label class='  full'>Kuvaaja (Photographer)</label><input type='text' class='form-control' data-bind='value:teos_kuvaaja' />
        </td>
        <td >
        <input type='file' class='form-control' data-bind='file:{data:teos_fileInput2, name:teos_kuva2}' />
        <label class='  full'>Kuvaaja (Photographer)</label><input type='text' class='form-control' data-bind='value:teos_kuvaaja2' />
        </td>
        <td>
        <input type='file' class='form-control' data-bind='file:{data:teos_fileInput3, name:teos_kuva3}' />
        <label class='  full'>Kuvaaja (Photographer)</label><input type='text' class='form-control' data-bind='value:teos_kuvaaja3' />
        </td></tr></table>
        <label class='  full'>Taiteellinen työryhmä ja esiintyjät
        (Artistic working group and performers)</label><br>
        <div class="tabs-lang">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#tyoryhma_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#tyoryhma_en" role="tab">EN</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="tyoryhma_fi" role="tabpanel">  
      <textarea class='form-control' data-bind='textInput:teos_tyoryhma_fi' ></textarea></div>
      <div class="tab-pane" id="tyoryhma_en" role="tabpanel"> 
      <textarea class='form-control' data-bind='textInput:teos_tyoryhma_en' ></textarea></div>
      </div></div>

<!--         <label class=' full'>Esityksen lyhyt markkinointiteksti (Short marketing text of the show)*</label><br>

      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#markkinointi_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#markkinointi_en" role="tab">EN</a>
        </li>
      </ul> 
      <div class="tab-content">
        <div class="tab-pane active" id="markkinointi_fi" role="tabpanel">  
      <textarea class='form-control' data-bind='textInput:markkinointi_fi' ></textarea></div>
      <div class="tab-pane" id="markkinointi_en" role="tabpanel"> 
      <textarea class='form-control' data-bind='textInput:markkinointi_en' ></textarea></div>
      </div>
-->

      <label class=' full'>Esityksen kuvaus, esim. käsiohjelmateksti (Description of the show e.g. from hand out )</label>
              <div class="tabs-lang">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#kuvaus_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#kuvaus_en" role="tab">EN</a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="kuvaus_fi" role="tabpanel">  
      <textarea class='form-control' data-bind='textInput:teos_teoksenkuvaus_fi' ></textarea></div>
      <div class="tab-pane" id="kuvaus_en" role="tabpanel"> 
      <textarea class='form-control' data-bind='textInput:teos_teoksenkuvaus_en' ></textarea></div>
      </div></div>

         <!-- <label class='  full'>Esitystyyppi</label><input type='text' class='form-control' data-bind='value:esitystyyppi' /> -->
              <label>Esityksen tyylilaji (Genre of the show)</label><br>
<div class="checkbox-inline">
        <label> <input type='checkbox' 
         data-bind='checked:teos_nykysirkus' />Nykysirkus (Contemporary circus)</label>
        <div class="checkbox-inline"></div>
        <label> <input type='checkbox' data-bind='checked:teos_perinteinen' />Perinteinen sirkus (Classical circus)</label>
        <div class="checkbox-inline"></div>
        <label> <input type='checkbox' data-bind='checked:teos_kokoperhe' />Koko perheelle (For all ages)</label>
        <div class="checkbox-inline"></div>
        <label> <input type='checkbox'  data-bind='checked:teos_muu' />Muu (Other)</label></div>
        <div data-bind="visible:teos_muu" class="normal">
        <label class='  full'>Mikä? (What?)</label><br>
        <div class="tabs-lang">
          <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#muu_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#muu_en" role="tab">EN</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="muu_fi" role="tabpanel">  
      <input class='form-control' data-bind='textInput:teos_muumika_fi' /></div>
      <div class="tab-pane" id="muu_en" role="tabpanel"> 
      <input class='form-control' data-bind='textInput:teos_muumika_en' /></div>
      </div></div>
      </div>
        <label class='  full'>Kesto (Duration) </label><input type='text' class='form-control' data-bind='value:teos_kesto' />
        <div class="checkbox-inline">
        <label> <input type='checkbox' data-bind='checked:teos_valiaika' />Sisältää väliajan (Incl. intermission) </label></div>
        <label class='  full'>Kantaesityspäivämäärä ja -paikka (Place and date of the first premiere)</label><input type='text' class='form-control' data-bind='value:teos_kantaesitys' />
        <label class='  full'>Suomen ensi-iltavuosi (Premier year in Finland)</label><input type='text' class='form-control' data-bind='value:teos_sensiilta' />
      <label class='  full'>Sirkustekniikat esityksessä (Circus disciplines)</label>
      <div class="tabs-lang">
              <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#lajit_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#lajit_en" role="tab">EN</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="lajit_fi" role="tabpanel">  
      <input class='form-control sirkuslajit' data-bind='textInput:teos_sirkuslajit_fi' /></div>
      <div class="tab-pane" id="kieli_en" role="tabpanel"> 
      <input class='form-control ' data-bind='textInput:teos_sirkuslajit_en' /></div>
      </div></div>

        <label class='  full'>Kieli (Language)</label>
        <div class="tabs-lang">
              <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#kieli_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#kieli_en" role="tab">EN</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="kieli_fi" role="tabpanel">  
      <input class='form-control' data-bind='textInput:teos_kieli_fi' /></div>
      <div class="tab-pane" id="kieli_en" role="tabpanel"> 
      <input class='form-control' data-bind='textInput:teos_kieli_en' /></div>
      </div></div>

        <!-- <label class='  full'>Musiikki (music)</label><input type='text' class='form-control' data-bind='value:teos_musiikki' /> -->
        <label class='  full'>Esityshistoria:päivämäärä, näyttämö, paikkakunta, maa (Past performances:date, venue, city, country)</label>
        <div data-bind="foreach:Shows">
        
        <span data-bind="if:venue_id" style="font-size:90%">
        <br> >
        <a data-bind="click:selectEvent" title="Muokkaa">
        <span data-bind="text:moment.unix(pvm).format('DD. MM. YYYY')"> </span>
        <span data-bind="text:festivaali"> </span>  
        <span data-bind="text:venue_id.paikka"> </span> 
        <span data-bind="text:venue_id.kaupunki"> </span>, 
        <span data-bind="text:venue_id.maa"></span></a>
        <a data-bind="click:delEvent" title="poista/remove"><span class="glyphicons glyphicons-minus-sign"></span></a>
        </div>
        </span><br>
        <div style="border:1px solid #000;padding:6px" id="event">
        <h3>Lisää uusi esitys kalenteriin (Add new event)</h3>
         <label class='  full'>Tapahtuman päivämäärä (Event date)</label>
         <input type='text' class='form-control' data-bind="datepicker: {dateFormat:'d. m. yy', value:event_pvm}" />
        <label class='  full'>Kellonajat (Show times)</label>
        <input type='text' class='form-control' data-bind='value:event_aika' />
        <label class='  full'>Festivaali, esityssarja tms. (Festival, programme or other)</label>
        <input type='text' class='form-control' data-bind='value:event_festivaali' />
        <label class='  full'>Esityksen paikka</label>
        <input type='hidden' class='form-control venue_id' data-bind='value:event_venue_id'/>
    		<input type='text' class='form-control venue'  data-bind="value:event_venue_paikka" />
        <label class='  full'>Esityksen nettilinkit</label>
        <textarea type='text' class='form-control' data-bind='value:event_www' ></textarea>
        <a data-bind='click:saveEvent' title="tallenna/save"><span class="glyphicons glyphicons-plus-sign"></span></a>
        </div>
        <label class='  full'>Muut esitykset (Other performances)</label><br>

        <div class="tabs-lang">

        <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#txt_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#txt_en" role="tab">EN</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="txt_fi" role="tabpanel">  
      <textarea class='form-control' data-bind='textInput:teos_vapaatxt_fi' ></textarea></div>
      <div class="tab-pane" id="txt_en" role="tabpanel"> 
      <textarea class='form-control' data-bind='textInput:teos_vapaatxt_en' ></textarea></div>
      </div></div>


  
<!--       <label class='  full'>Muut jasenet</label><input type='text' class='form-control toimijat' data-bind='value:muutjasenet' />
        <label class='  full'>Kunta</label><input type='text' class='form-control' data-bind='value:kunta' />
        <label class='  full'>Koreografi</label><input type='text' class='form-control toimijat' data-bind='value:koreografi' />
        <label class='  full'>Tuottaja</label><input type='text' class='form-control toimijat' data-bind='value:tuottaja' />
        <label class='  full'>Venue</label><input type='text' class='form-control venue' data-bind='value:paikka' />
        <label class='  full'>Sirkusryhma</label>
        <input type='hidden' class='form-control ryhma_id' data-bind='value:group_id' />
        <input type='text' class='form-control ryhma'/>
 -->
        <br><br>

        <button type="button" class="btn btn-primary" data-bind='click:saveUusi'>OK</button>

	</div>

<script type="text/javascript">
	

var Teos = function() {

    Lookup('venue');
    Lookup('toimijat');
    Lookup('sirkuslajit');

    if(UGID == AGID ) {
        self.admin = ko.observable(true)
    } else {
        self.admin = ko.observable(false)
    }

   self.limit_options = ko.observableArray([0,1,3,5])

    self.teos_nimi=ko.observable();
    self.teos_updated = ko.observable();
    self.teos_www = ko.observable();
    self.teos_nimi_en=ko.observable();
    self.teos_esittaja=ko.observable();
    self.teos_kuva=ko.observable();
    self.teos_fileInput = ko.observable();
    self.teos_kuvaaja=ko.observable();
    self.teos_kuva2=ko.observable();
    self.teos_fileInput2 = ko.observable();
    self.teos_kuvaaja2=ko.observable('');
    self.teos_kuva3=ko.observable();
    self.teos_fileInput3 = ko.observable();
    self.teos_kuvaaja3 =ko.observable();
    self.teos_markkinointi_fi=ko.observable();
    self.teos_markkinointi_en=ko.observable();
    self.teos_tyoryhma_fi=ko.observable();
    self.teos_tyoryhma_en=ko.observable();
    self.teos_teoksenkuvaus_fi=ko.observable();
    self.teos_teoksenkuvaus_en=ko.observable();
    self.teos_esitystyyppi=ko.observable();
    self.teos_kesto=ko.observable();
    self.teos_valiaika=ko.observable();
    self.teos_kantaesitys=ko.observable();
    self.teos_sensiilta=ko.observable();
    self.teos_sirkuslajit_fi=ko.observable();
    self.teos_sirkuslajit_en=ko.observable();
    self.teos_kieli_fi=ko.observable();
    self.teos_kieli_en=ko.observable();
    self.teos_musiikki=ko.observable();
    self.teos_vapaatxt_fi=ko.observable();
    self.teos_vapaatxt_en=ko.observable();
    self.teos_nykysirkus=ko.observable();
    self.teos_perinteinen=ko.observable();
    self.teos_kokoperhe=ko.observable();
    self.teos_muu=ko.observable();
    self.teos_muumika_fi=ko.observable();
    self.teos_muumika_en=ko.observable();
    self.teos_sirkka=ko.observable(0);
    self.teos_esitys=ko.observable(0);
    self.teos_muutjasenet=ko.observable();
    self.teos_julkaistu=ko.observable(0);
    self.teos_kunta=ko.observable();
    self.teos_koreografi=ko.observable();
    self.teos_tuottaja=ko.observable();
    self.teos_paikka=ko.observable();
    self.teos_group_id=ko.observable();
    self.teos_promo=ko.observable(0);
    self.teos_limited=ko.observable(0) 
    self.teos_video=ko.observable();
    //self.teos_materiaali=ko.observable();

    self.Shows = ko.observableArray([]);

    self.teos_original = ko.observable();

    if(TID > 0 ) {

      Api('GET','esitys?where=main_id=='+TID+'&embedded={"venue_id":1}&sort=-pvm',null, 
              function(data){
                  self.Shows.pushAll(data)
              })


    ko.computed( function(){

            return Api('GET','teos/'+TID,null, function(data) {

                    self.teos_original((data))

                    self.teos_nimi(data.nimi)
                    self.teos_updated(data._updated)
                    self.teos_www(data.www)
                    self.teos_nimi_en(data.nimi_en)
                    self.teos_esittaja(data.esittaja);
                    self.teos_kuva(data.kuva);
                    self.teos_kuvaaja(data.kuvaaja);
                    self.teos_kuva2(data.kuva2);
                    self.teos_kuvaaja2(data.kuvaaja2);
                    self.teos_kuva3(data.kuva3);
                    self.teos_kuvaaja3(data.kuvaaja3);
                    var mark = JSON.parse(data.markkinointi)
                    self.teos_markkinointi_fi(mark.fi);
                    self.teos_markkinointi_en(mark.en);
                    try {
                        var tyoryh = JSON.parse(data.tyoryhma)
                        self.teos_tyoryhma_fi(tyoryh.fi)
                        self.teos_tyoryhma_en(tyoryh.en)
                    } catch(e) {
                        self.teos_tyoryhma_fi(data.tyoryhma);
                        self.teos_tyoryhma_en(data.tyoryhma)
                    }
                    var kuvaus = JSON.parse(data.teoksenkuvaus)
                    self.teos_teoksenkuvaus_fi(kuvaus.fi);
                    self.teos_teoksenkuvaus_en(kuvaus.en);
                    self.teos_esitystyyppi(data.esitystyyppi);
                    self.teos_kesto(data.kesto);
                    self.teos_valiaika(data.valiaika);
                    self.teos_kantaesitys(data.kantaesitys);
                    self.teos_sensiilta(data.sensiilta);
                    self.teos_sirkuslajit_fi(JSON.parse(data.sirkuslajit).fi);
                    self.teos_sirkuslajit_en(JSON.parse(data.sirkuslajit).en);
                    var kieli = JSON.parse(data.kieli)
                    self.teos_kieli_fi(kieli.fi);
                    self.teos_kieli_en(kieli.en);
                    self.teos_musiikki(data.musiikki);
                    var txt = JSON.parse(data.vapaatxt)
                    self.teos_vapaatxt_fi(txt.fi);
                    self.teos_vapaatxt_en(txt.en);
                    self.teos_nykysirkus(data.nykysirkus);
                    self.teos_perinteinen(data.perinteinen);
                    self.teos_kokoperhe(data.kokoperhe);
                    self.teos_muu(data.muu);
                    var muum = JSON.parse(data.muumika)
                    self.teos_muumika_fi(muum.fi);
                    self.teos_muumika_en(muum.en);
                    self.teos_sirkka(data.sirkka);
                    self.teos_esitys(data.esitys);
                    self.teos_muutjasenet(data.muutjasenet);
                    self.teos_julkaistu(data.julkaistu);
                    self.teos_kunta(data.kunta);
                    self.teos_koreografi(data.koreografi);
                    self.teos_tuottaja(data.tuottaja);
                    self.teos_paikka(data.paikka);
                    self.teos_group_id(data.group_id);
                    self.teos_promo(data.promo);
                    self.teos_limited(parseInt(data.limited));
                    self.teos_video(data.video)
            })
    	}, self)
	}


    self.rmKuva1 = function() {
      self.teos_fileInput(null);
      self.teos_kuva(null);
      self.teos_kuvaaja(null)
    }

    self.rmKuva2 = function() {
      self.teos_fileInput2(null);
      self.teos_kuva2(null);
      console.log('rm '+self.teos_kuva2())
      self.teos_kuvaaja2(null)
    }

    self.rmKuva3 = function() {
      self.teos_fileInput3(null);
      self.teos_kuva3(null);
      self.teos_kuvaaja3(null)
    }

    self.saveUusi = function () {

      self.uusi = {
          user_id: parseInt(UID),    
          nimi:self.teos_nimi(),
          nimi_en: self.teos_nimi_en(),
          esittaja:self.teos_esittaja(),
          www:self.teos_www(),
          kuva:self.teos_fileInput(),
          kuvaaja:self.teos_kuvaaja(),
          kuva2:self.teos_fileInput2(),
          kuvaaja2:self.teos_kuvaaja2(),
          kuva3:self.teos_fileInput3(),
          kuvaaja3:self.teos_kuvaaja3(),
          //markkinointi:ko.toJSON({fi:self.teos_markkinointi_fi(), en: self.teos_markkinointi_en()}),
          tyoryhma:ko.toJSON({fi:self.teos_tyoryhma_fi(), en: self.teos_tyoryhma_en()}),
          // tyoryhma:self.teos_tyoryhma(),
          teoksenkuvaus:ko.toJSON({fi:self.teos_teoksenkuvaus_fi(), en: self.teos_teoksenkuvaus_en()}),
          esitystyyppi:self.teos_esitystyyppi(),
          kesto:self.teos_kesto(),
          valiaika:self.teos_valiaika(),
          kantaesitys:self.teos_kantaesitys(),
          sensiilta:self.teos_sensiilta(),
          sirkuslajit:ko.toJSON({fi:self.teos_sirkuslajit_fi(), en: self.teos_sirkuslajit_en()}),
          kieli:ko.toJSON({fi:self.teos_kieli_fi(), en: self.teos_kieli_en()}),
          musiikki:self.teos_musiikki(),
          vapaatxt:ko.toJSON({fi:self.teos_vapaatxt_fi(), en: self.teos_vapaatxt_en()}),
          nykysirkus:self.teos_nykysirkus(),
          perinteinen:self.teos_perinteinen(),
          kokoperhe:self.teos_kokoperhe(),
          muu:self.teos_muu(),
          muumika:ko.toJSON({fi:self.teos_muumika_fi(), en: self.teos_muumika_en()}),
          sirkka:self.teos_sirkka(),
          esitys:self.teos_esitys(),
          muutjasenet:self.teos_muutjasenet(),
          julkaistu:self.teos_julkaistu(),
          kunta:self.teos_kunta(),
          koreografi:self.teos_koreografi(),
          tuottaja:self.teos_tuottaja(),
          paikka:self.teos_paikka(),
          // group_id:parseInt(self.teos_group_id()),
          promo:self.teos_promo(),
          limited:parseInt(self.teos_limited()),
          video:self.teos_video()
      }


            for(key in self.uusi) {
              if(self.uusi[key] == self.teos_original()[key]) {
                delete self.uusi[key]
              } 
          
            }

      console.log(self.uusi)
          

            // var data = new FormData();
            // data.append('file',self.fileInput.data)
            // data.append('json', JSON.stringify(self.uusi))


        if(TID > 0) {

            Api('PATCH','teos/'+TID, self.uusi, function(data){
                $('.alert').toggle();
                window.setTimeout(function() {
                  location.reload()
                // location.href = '<?php echo site_url(); ?>ajankohtaista/esityskalenteri/teos/?work_id='+TID
                }, 2000)
            })
        }
    }

	self.event_pvm = ko.observable().extend({ required: true });  
	self.event_aika = ko.observable();  
	self.event_venue_id = ko.observable();
  self.event_venue_paikka = ko.observable();  
	self.event_www = ko.observable();
  self.event_id=ko.observable(0);  
  self.event_festivaali=ko.observable()

	self.selectEvent = function(row){
  		self.event_pvm(moment.unix(row.pvm).format('DD. MM. YYYY'))
  		self.event_aika(row.aika)
  		self.event_venue_id(row.venue_id._id)
      self.event_venue_paikka(row.venue_id.paikka)
  		self.event_www(row.www)
      self.event_id(row._id)
      self.event_festivaali(row.festivaali)
      location.href = '#event'

	}

	self.saveEvent = function() {

    var pvm = moment(self.event_pvm()).valueOf()

		var thisevent = {
			pvm: (pvm/1000).toString(),
			aika: self.event_aika(),
			main_id: parseInt(TID),
			www: self.event_www(),
      venue_id:parseInt(self.event_venue_id()),
      group_id:parseInt(UGID),
      festivaali:self.event_festivaali()

		}



    if(self.event_id() == 0) {

      Api('POST','esitys/',thisevent, function() {
              $('.alert').toggle();
              window.setTimeout(function() {
                self.Shows.push(thisevent)
              }, 2000)
      })

      } else {

            Api('PATCH','esitys/'+self.event_id(),thisevent, function() {
              $('.alert').toggle();
              window.setTimeout(function() {
                self.Shows.push(thisevent)
              }, 2000)
            })

      }

	}

  self.delEvent = function(row) {
    Api('DELETE', 'esitys/'+row._id, null, function(){
            self.Shows.remove(row);
            $('.alert').toggle();
            window.setTimeout(function() {
            }, 2000)
    })

  }


}


var Teokset = function() {

    //ko.virtualElements.allowedBindings.group_id = true;

    Lookup('ryhma')

    var self = this;

    if(UGID == AGID ) {
        self.admin = ko.observable(true)
    } else {
        self.admin = ko.observable(false)
    }

    var group = ''

    if(GID) {
        var group = '&where=group_id=='+GID
    }

    self.teokset = ko.observableArray();

    Api('GET','teos?sort=nimi'+group, null, function(data){

        //console.log(data)

        //self.artists.pushAll(data._items)

            $.each(data, function(index, item) {
                self.teokset.push(
                    ko.mapping.fromJS(item)
                )
            })     
    })


    self.saveRow = function(row) {

        patch = {group_id:parseInt(row.group_id())}

        Api('PATCH','teos/'+row._id(), patch, function() {
                console.log('saved')
            })

        // var user = $.ajax({
        //     method: 'PATCH',
        //     url: baseURL + 'users/'+row._id(),
        //     //async: false,
        //     dataType: "json",
        //     contentType: "application/json",
        //     data: JSON.stringify(patch),
        //     success: function (user) {
        //         },
        //     error: function (xhr, type, exception) {
        //         console.log(xhr)

        //         }
        //     });

        return true;
    }

    self.delRow = function(row) {

        Api('DELETE','teos/'+row._id(), null, function(){
            console.log('Deleted')
        })

        // var user = $.ajax({
        //     method: 'DELETE',
        //     url: baseURL + 'users/'+row._id(),
        //     //async: false,
        //     dataType: "json",
        //     contentType: "application/json",
        //     //data: JSON.stringify(patch),
        //     success: function (user) {
        //         console.log('del:'+row._id())
        //         },
        //     error: function (xhr, type, exception) {
        //         console.log(xhr)

        //         }
        //     });

        return true;
    }


   self.addRow = function() {

   }

    self.saveRow = function () {

    }

}


    // ko.applyBindings(new Teokset(), $('#teokset')[0]);


    ko.applyBindings(new Teos(), $('#teos')[0]);


 
</script>