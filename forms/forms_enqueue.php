<?php
// //base.js

// bootstrap.js

 
// jquery.cookie.js


// ko-calendar.min.js


// jquery.datetimepicker.js

//  knockout-kendo.min.js

// npm.js

// tether.min.js
// // bootstrap.min.js
// jquery.datetimepicker.min.js
// knockout.date-picker.js
// knockout.mapping-latest.js
// // jquery-2.2.3.js
//  jquery.formatDateTime.min.js

// login.js


function sirkka_forms_scripts() {

// // 	//Yleiset
	wp_enqueue_script('jquery2.2.3','/wp-content/themes/sirkusinfo/js/sirkka/jquery-2.2.3.js');
	wp_enqueue_script('jquery-ui','/wp-content/themes/sirkusinfo/js/sirkka/jquery-ui.min.js');
	wp_enqueue_script('sirkka_bootstrap','/wp-content/themes/sirkusinfo/js/sirkka/bootstrap.min.js');
	wp_enqueue_script('sirkka_tether','/wp-content/themes/sirkusinfo/js/sirkka/tether.min.js');
	wp_enqueue_script('sirkka_typeadhead','/wp-content/themes/sirkusinfo/js/sirkka/bootstrap-typeahead.js');
	wp_enqueue_script('sirkka_ko','/wp-content/themes/sirkusinfo/js/sirkka/knockout-min.js');
	wp_enqueue_script('sirkka_ko_jq','/wp-content/themes/sirkusinfo/js/sirkka/knockout-jqueryui.min.js');
	wp_enqueue_script('sirkka_ko_file','/wp-content/themes/sirkusinfo/js/sirkka/knockout-file-bind.js');
	wp_enqueue_script('sirkka_moment','/wp-content/themes/sirkusinfo/js/sirkka/moment-with-locales.min.js');
	wp_enqueue_script('sirkka_cookie','/wp-content/themes/sirkusinfo/js/sirkka/js.cookie.js');
	wp_enqueue_script('sirkka_md5','/wp-content/themes/sirkusinfo/js/sirkka/jquery.md5.js');
	wp_enqueue_script('sirkka_valid','/wp-content/themes/sirkusinfo/js/sirkka/knockout.validation.js');
	wp_enqueue_script('sirkka_formatdate','/wp-content/themes/sirkusinfo/js/sirkka/jquery.formatDateTime.min.js');
	wp_enqueue_script('sirkka_base','/wp-content/themes/sirkusinfo/js/sirkka/base.js');

// // // 	//Login 
	// wp_enqueue_script('sirkka_login','/wp-content/themes/sirkusinfo/js/sirkka/login.js');
// 	// wp_enqueue_script('sirkka_cookie','/wp-content/themes/sirkusinfo/js/sirkka/js.cookie.js');
// 	// wp_enqueue_script('sirkka_md5','/wp-content/themes/sirkusinfo/js/sirkka/jquery.md5.js');
// 	// wp_enqueue_script('sirkka_valid','/wp-content/themes/sirkusinfo/js/sirkka/knockout.validation.js');

// // // 	//Users
// 	wp_enqueue_script('sirkka_users','/wp-content/themes/sirkusinfo/js/sirkka/users.js');
// // wp_enqueue_script('sirkka_cookie','/wp-content/themes/sirkusinfo/js/sirkka/js.cookie.js');
// // wp_enqueue_script('sirkka_md5','/wp-content/themes/sirkusinfo/js/sirkka/jquery.md5.js');
// // wp_enqueue_script('sirkka_valid','/wp-content/themes/sirkusinfo/js/sirkka/knockout.validation.js');

// // // 	//Teos
// 	wp_enqueue_script('sirkka_teos','/wp-content/themes/sirkusinfo/js/sirkka/teos.js');

// // // 	//Ryhmä
// 	wp_enqueue_script('sirkka_ryhma','/wp-content/themes/sirkusinfo/js/sirkka/ryhma.js');

// // // 	//Venue
// 	wp_enqueue_script('sirkka_venue','/wp-content/themes/sirkusinfo/js/sirkka/venue.js');

// // // 	//Kalenteri
// 	wp_enqueue_script('sirkka_sirkka','/wp-content/themes/sirkusinfo/js/sirkka/sirkka.js');
// 	wp_enqueue_script('sirkka_datetime','/wp-content/themes/sirkusinfo/js/sirkka/jquery.datetimepicker.min.js');

// // // 	//Sirkuslajit
// 	wp_enqueue_script('sirkka_lajit','/wp-content/themes/sirkusinfo/js/sirkka/lajit.js');

// // // 	//Toimijat
// 	wp_enqueue_script('sirkka_toimijat','/wp-content/themes/sirkusinfo/js/sirkka/toimijat.js');

// // // 	//Tilasto
// 	wp_enqueue_script('sirkka_lodash','/wp-content/themes/sirkusinfo/js/sirkka/lodash.min.js');
// 	wp_enqueue_script('sirkka_tilasto','/wp-content/themes/sirkusinfo/js/sirkka/tilasto.js');

 wp_enqueue_style( 'sirkka_css', '/wp-content/themes/sirkusinfo/forms/css/sirkka-forms.css' );
 // wp_enqueue_style( 'bootstrap', '/wp-content/themes/sirkusinfo/forms/css/bootstrap.min.css' );
}

add_action( 'wp_enqueue_scripts', 'sirkka_forms_scripts' );
?>