<?php

// returns user_id if logged in, false if not
function sirkka_getLoggedInUser() {
	if(isset($_COOKIE["UID"])) {
		return $_COOKIE["UID"];
	} else {
		return false; //166; // "circushelsinki"
	}
}

// admin group id == 0
function sirkka_getLoggedInGroup() {
	if(isset($_COOKIE["UGID"])) {
		return $_COOKIE["UGID"];
	} else {
		return false; //166; // "circushelsinki"
	}
}

function sirkka_isAdmin() {
	return sirkka_getLoggedInGroup() == 1;
}

function sirkka_signOut() {
	unset($_COOKIE['UID']);
	unset($_COOKIE['UGID']);
	header('Location: '.$_SERVER['REQUEST_URI']);
}

function sirkka_userCanWrite($groupid) {
	return (sirkka_isAdmin() || $groupid == sirkka_getLoggedInGroup());	
}

?>