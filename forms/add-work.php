      	    

      <div class="alert alert-success" role="alert" id="OK">
      <span class="glyphicon glyphicon-check"  aria-hidden="true"></span>
  </div>

	<div class="container col-sm-12" id="teos">
	      <!--   <span class="form teos form-horizontal" id="teos"><br>
                    <a data-bind="attr: { 'href': 'user.html' }" class="addgroup btn btn-info">Käyttäjätili</a>   
                    <a data-bind="attr: { 'href': 'toimija.html?sirkka_id='+UID }" class="addgroup btn btn-info">Oma profiili</a>
          <a data-bind="attr: { 'href': 'ryhma.html?sirkka_gid='+UGID }" class="addgroup btn btn-info">Sirkusryhma</a>
          <a data-bind="attr: { 'href': 'teokset.html?sirkka_gid='+UGID }" class="addgroup btn btn-info">Teokset</a>
           --><!-- <a data-bind="attr: { 'href': 'sirkka.html?sirkka_gid='+UGID }" class="addgroup btn btn-info">Esityskalenteri</a> -->

	        <h2>Sirkusesityksen tiedot (Circus show)</h2>
        <div class="checkbox-inline">
        <label> <input type='checkbox' class="admin" data-bind="{'checked':teos_sirkka}" />Lisää Sirkka-tietokantaan (Add to Sirkka database)</label></div>
        <div class="checkbox-inline">
        <label> <input type='checkbox'  class='admin' data-bind='checked:teos_julkaistu' />Julkaistu (Published) </label></div>
        <span data-bind="visible:admin">
        <div class="checkbox-inline">
        <label> <input type='checkbox'  class='admin' data-bind="{'checked':teos_promo, visible:admin}" />Artists and shows</label></div></span>
        <div data-bind="visible:admin">
        <label> Esityskalenterin rajoitus</label>
        <select class='admin' data-bind='value:teos_limited'>
        <option value="0">0</option>
        <option value="0">1</option>
        <option value="0">3</option>
        <option value="0">5</option>
        </select></div>       
        <label class='  full'>Esityksen nimi (Title of the show)*
         <a href="#" data-toggle="tooltip" title="Mikäli teoksella on käytössä  myös englanninkielinen nimi, kirjoita se tähän. Mutta älä käännä teosnimeä englanniksi vain tätä tietokantaa varten!">
       <span class="glyphicon glyphicon-question-sign"></span></a> 
    </label>
    <div class="tabs-lang">

        <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#nimi_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#nimi_en" role="tab">EN</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="nimi_fi" role="tabpanel">  
      <input class='form-control' type="text" data-bind='textInput:teos_nimi' /></div>
      <div class="tab-pane" id="nimi_en" role="tabpanel"> 
      <input class='form-control' type="text" data-bind='textInput:teos_nimi_en' /></div>
      </div></div>

      <input type="hidden" class="ryhma_id" data-bind="group_id" />
        <label class='  full'>Ryhmä tai esittäjä (Artist’s or company’s name)*
        <a href="#" data-toggle="tooltip" title=" Jos kyseessä yhteistuotanto, jokainen yhteistuottaja omalle rivilleen.
 In case of co-production, give name of each production partner separately.">
       <span class="glyphicon glyphicon-question-sign"></span></a> 
       </label>
       <input type="text" class='form-control ryhma' data-bind='value:teos_esittaja'/>
       <label>WWW-osoitteet (Web links)</label>
       <textarea data-bind="textInput:teos_www"></textarea>
        <label>Video-linkki (Video link)</label>
       <input type="text" data-bind="textInput:teos_video" />
        <label class='  full'>Kuvat (Images)</label>
        <table><tr>
        <td>        <span data-bind="if:teos_kuva">
        <img data-bind="attr:{'src': 'http://sirkusinfo.fi/'+ teos_kuva().split('/var/www/').pop() }" width="200px"/></span>
        </td>
        <td>        <span data-bind="if:teos_kuva2">
        <img data-bind="attr:{'src': 'http://sirkusinfo.fi/'+ teos_kuva2().split('/var/www/').pop() }" width="200px"/></span>
        </td>
        <td>        <span data-bind="if:teos_kuva3">

        <img data-bind="attr:{'src': 'http://sirkusinfo.fi/'+ teos_kuva3().split('/var/www/').pop() }" width="200px"/></span>
        </td>
        </tr>
        <tr><td>
        <input type='file' class='form-control' data-bind='file:{data: teos_fileInput, name: teos_kuva}' />
        <label class='  full'>Kuvaaja (Photographer)</label><input type='text' class='form-control' data-bind='value:teos_kuvaaja' />
        </td>
        <td>
        <input type='file' class='form-control' data-bind='file:{data: teos_fileInput2, name: teos_kuva2}' />
        <label class='  full'>Kuvaaja (Photographer)</label><input type='text' class='form-control' data-bind='value:teos_kuvaaja2' />
        </td>
        <td>
        <input type='file' class='form-control' data-bind='file:{data: teos_fileInput3, name: teos_kuva3}' />
        <label class='  full'>Kuvaaja (Photographer)</label><input type='text' class='form-control' data-bind='value:teos_kuvaaja3' />
        </td></tr></table>
        <label class='  full'>Taiteellinen työryhmä ja esiintyjät
        (Artistic working group and performers)</label><br>

        <div class="tabs-lang">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#tyoryhma_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#tyoryhma_en" role="tab">EN</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="tyoryhma_fi" role="tabpanel">  
      <textarea class='form-control' data-bind='textInput:teos_tyoryhma_fi' ></textarea></div>
      <div class="tab-pane" id="tyoryhma_en" role="tabpanel"> 
      <textarea class='form-control' data-bind='textInput:teos_tyoryhma_en' ></textarea></div>
      </div></div>
<!--         <label class=' full'>Esityksen lyhyt markkinointiteksti (Short marketing text of the show)*</label><br>

      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#markkinointi_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#markkinointi_en" role="tab">EN</a>
        </li>
      </ul>
 -->      <!-- Tab panes -->
<!--       <div class="tab-content">
        <div class="tab-pane active" id="markkinointi_fi" role="tabpanel">  
      <textarea class='form-control' data-bind='textInput:teos_markkinointi_fi' ></textarea></div>
      <div class="tab-pane" id="markkinointi_en" role="tabpanel"> 
      <textarea class='form-control' data-bind='textInput:markkinointi_en' ></textarea></div>
      </div>

-->
      <label class=' full'>Esityksen kuvaus, esim. käsiohjelmateksti (Description of the show e.g. from hand out )</label>
              <div class="tabs-lang">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#kuvaus_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#kuvaus_en" role="tab">EN</a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="kuvaus_fi" role="tabpanel">  
      <textarea class='form-control' data-bind='textInput:teos_teoksenkuvaus_fi' ></textarea></div>
      <div class="tab-pane" id="kuvaus_en" role="tabpanel"> 
      <textarea class='form-control' data-bind='textInput:teos_teoksenkuvaus_en' ></textarea></div>
      </div></div>

         <!-- <label class='  full'>Esitystyyppi</label><input type='text' class='form-control' data-bind='value:esitystyyppi' /> -->
              <label>Esityksen tyylilaji (Genre of the show)</label><br>
<div class="checkbox-inline">
        <label> <input type='checkbox' 
         data-bind='checked:teos_nykysirkus' />Nykysirkus (Contemporary circus)</label>
        <div class="checkbox-inline"></div>
        <label> <input type='checkbox' data-bind='checked:teos_perinteinen' />Perinteinen sirkus (Classical circus)</label>
        <div class="checkbox-inline"></div>
        <label> <input type='checkbox' data-bind='checked:teos_kokoperhe' />Koko perheelle (For all ages)</label>
        <div class="checkbox-inline"></div>
        <label> <input type='checkbox'  data-bind='checked:teos_muu' />Muu (Other)</label></div>
        <div data-bind="visible:teos_muu" class="normal">
        <label class='  full'>Mikä? (What?)</label><br>

        <div class="tabs-lang">
          <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#muu_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#muu_en" role="tab">EN</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="muu_fi" role="tabpanel">  
      <input class='form-control' data-bind='textInput:teos_muumika_fi' /></div>
      <div class="tab-pane" id="muu_en" role="tabpanel"> 
      <input class='form-control' data-bind='textInput:teos_muumika_en' /></div>
      </div>
      </div></div>
        <label class='  full'>Kesto (Duration) </label><input type='text' class='form-control' data-bind='value:teos_kesto' />
        <div class="checkbox-inline">
        <label> <input type='checkbox' data-bind='checked:teos_valiaika' />Sisältää väliajan (Incl. intermission) </label></div>
        <label class='  full'>Kantaesityspäivämäärä ja -paikka (Place and date of the first premiere)</label><input type='text' class='form-control' data-bind='value:teos_kantaesitys' />
        <label class='  full'>Suomen ensi-iltavuosi (Premier year in Finland)</label><input type='text' class='form-control' data-bind='value:teos_sensiilta' /><br>
      <label class='  full'>Sirkustekniikat esityksessä (Circus disciplines)</label>
      <div class="tabs-lang">
              <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#lajit_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#lajit_en" role="tab">EN</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="lajit_fi" role="tabpanel">  
      <input class='form-control' data-bind='textInput:teos_sirkuslajit_fi' /></div>
      <div class="tab-pane" id="kieli_en" role="tabpanel"> 
      <input class='form-control' data-bind='textInput:teos_sirkuslajit_en' /></div>
      </div></div>

        <label class='  full'>Kieli (Language)</label>
        <div class="tabs-lang">
              <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#kieli_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#kieli_en" role="tab">EN</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="kieli_fi" role="tabpanel">  
      <input class='form-control' data-bind='textInput:teos_kieli_fi' /></div>
      <div class="tab-pane" id="kieli_en" role="tabpanel"> 
      <input class='form-control' data-bind='textInput:teos_kieli_en' /></div>
      </div></div>

        <!-- <label class='  full'>Musiikki (music)</label><input type='text' class='form-control' data-bind='value:musiikki' /> -->
        <label class='  full'>Esityshistoria: päivämäärä, näyttämö, paikkakunta, maa (Past performances: date, venue, city, country)</label>
      <div data-bind="foreach:Shows">
        <div>
        >&nbsp; 
        <a data-bind="click:selecetEvent">
        <span data-bind="text:moment.unix(pvm).format('DD. MM. YYYY')"></span> 
        <span data-bind="text:venue_id.paikka"></span> 
        <span data-bind="text:venue_id.kaupunki"></span>, 
        <span data-bind="text:venue_id.maa"></span></a>
        </div></div>

        <label class='  full'>Muut esitykset (Other performances)</label><br>
        <div class="tabs-lang">
        <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-toggle="tab" href="#txt_fi" role="tab">FI</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-toggle="tab" href="#txt_en" role="tab">EN</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="txt_fi" role="tabpanel">  
      <textarea class='form-control' data-bind='textInput:teos_vapaatxt_fi' ></textarea></div>
      <div class="tab-pane" id="txt_en" role="tabpanel"> 
      <textarea class='form-control' data-bind='textInput:teos_vapaatxt_en' ></textarea></div>
      </div></div>


  
<!--       <label class='  full'>Muut jasenet</label><input type='text' class='form-control toimijat' data-bind='value:muutjasenet' />
        <label class='  full'>Kunta</label><input type='text' class='form-control' data-bind='value:kunta' />
        <label class='  full'>Koreografi</label><input type='text' class='form-control toimijat' data-bind='value:koreografi' />
        <label class='  full'>Tuottaja</label><input type='text' class='form-control toimijat' data-bind='value:tuottaja' />
        <label class='  full'>Venue</label><input type='text' class='form-control venue' data-bind='value:paikka' />
        <label class='  full'>Sirkusryhma</label>
        <input type='hidden' class='form-control ryhma_id' data-bind='value:group_id' />
        <input type='text' class='form-control ryhma'/>
 -->
        <br><br>

        <button type="button" class="btn btn-primary" data-bind='click: saveUusi'>OK</button>

	</div>

<script type="text/javascript">
	

var Teos = function() {

    Lookup('ryhma');
    Lookup('toimijat');
    Lookup('sirkuslajit');


    if(UGID == AGID) {
        self.admin = ko.observable(true)

    } else {
        self.admin = ko.observable(false)
    }

    self.Shows = ko.observableArray([]);

    self.teos_nimi=ko.observable();
    self.teos_updated = ko.observable();
    self.teos_nimi_en=ko.observable();
    self.teos_esittaja=ko.observable();
    self.teos_www=ko.observable();
    self.teos_kuva=ko.observable('');
    self.teos_fileInput = ko.observable();
    self.teos_kuvaaja=ko.observable();
    self.teos_kuva2=ko.observable('');
    self.teos_fileInput2 = ko.observable();
    self.teos_kuvaaja2=ko.observable('');
    self.teos_kuva3=ko.observable('');
    self.teos_fileInput3 = ko.observable();
    self.teos_kuvaaja3 =ko.observable();
    self.teos_markkinointi_fi=ko.observable();
    self.teos_markkinointi_en=ko.observable();
    self.teos_tyoryhma_fi=ko.observable();
    self.teos_tyoryhma_en=ko.observable();
    self.teos_teoksenkuvaus_fi=ko.observable();
    self.teos_teoksenkuvaus_en=ko.observable();
    self.teos_esitystyyppi=ko.observable();
    self.teos_kesto=ko.observable();
    self.teos_valiaika=ko.observable();
    self.teos_kantaesitys=ko.observable();
    self.teos_sensiilta=ko.observable();
    self.teos_sirkuslajit_fi=ko.observable();
    self.teos_sirkuslajit_en=ko.observable();
    self.teos_kieli_fi=ko.observable();
    self.teos_kieli_en=ko.observable();
    self.teos_musiikki=ko.observable();
    self.teos_vapaatxt_fi=ko.observable();
    self.teos_vapaatxt_en=ko.observable();
    self.teos_nykysirkus=ko.observable();
    self.teos_perinteinen=ko.observable();
    self.teos_kokoperhe=ko.observable();
    self.teos_muu=ko.observable();
    self.teos_muumika_fi=ko.observable();
    self.teos_muumika_en=ko.observable();
    self.teos_sirkka=ko.observable(0);
    self.teos_esitys=ko.observable(0);
    self.teos_muutjasenet=ko.observable();
    self.teos_julkaistu=ko.observable(0);
    self.teos_kunta=ko.observable();
    self.teos_koreografi=ko.observable();
    self.teos_tuottaja=ko.observable();
    self.teos_paikka=ko.observable();
    self.teos_group_id=ko.observable(UGID);
    self.teos_promo=ko.observable(0);
    self.teos_limited=ko.observable() 
    self.teos_video=ko.observable();

    

    self.saveUusi = function () {

            self.uusi = {
                user_id: parseInt(UID),    
                nimi:self.teos_nimi(),
                nimi_en: self.teos_nimi_en(),
                www:self.teos_www(),
                esittaja:self.teos_esittaja(),
                kuva:self.teos_fileInput(),
                kuvaaja:self.teos_kuvaaja(),
                kuva2:self.teos_fileInput2(),
                kuvaaja2:self.teos_kuvaaja2(),
                kuva2:self.teos_fileInput3(),
                kuvaaja3:self.teos_kuvaaja3(),
                markkinointi:ko.toJSON({fi:self.teos_markkinointi_fi(), en: self.teos_markkinointi_en()}),
                tyoryhma:ko.toJSON({fi:self.teos_tyoryhma_fi(), en: self.teos_tyoryhma_en()}),
                // tyoryhma:self.teos_tyoryhma(),
                teoksenkuvaus:ko.toJSON({fi:self.teos_teoksenkuvaus_fi(), en: self.teos_teoksenkuvaus_en()}),
                esitystyyppi:self.teos_esitystyyppi(),
                kesto:self.teos_kesto(),
                valiaika:self.teos_valiaika(),
                kantaesitys:self.teos_kantaesitys(),
                sensiilta:self.teos_sensiilta(),
                sirkuslajit:ko.toJSON({fi:self.teos_sirkuslajit_fi(), en: self.teos_sirkuslajit_en()}),
                kieli:ko.toJSON({fi:self.teos_kieli_fi(), en: self.teos_kieli_en()}),
                musiikki:self.teos_musiikki(),
                vapaatxt:ko.toJSON({fi:self.teos_vapaatxt_fi(), en: self.teos_vapaatxt_en()}),
                nykysirkus:self.teos_nykysirkus(),
                perinteinen:self.teos_perinteinen(),
                kokoperhe:self.teos_kokoperhe(),
                muu:self.teos_muu(),
                muumika:ko.toJSON({fi:self.teos_muumika_fi(), en: self.teos_muumika_en()}),
                sirkka:self.teos_sirkka(),
                esitys:self.teos_esitys(),
                muutjasenet:self.teos_muutjasenet(),
                julkaistu:self.teos_julkaistu(),
                kunta:self.teos_kunta(),
                koreografi:self.teos_koreografi(),
                tuottaja:self.teos_tuottaja(),
                paikka:self.teos_paikka(),
                group_id:parseInt(self.teos_group_id()),
                promo:self.teos_promo(),
                limited:parseInt(self.teos_limited),
                video:self.teos_video()
            }

            // var data = new FormData();
            // data.append('file',self.fileInput.data)
            // data.append('json', JSON.stringify(self.uusi))

            console.log(JSON.stringify(self.uusi))


            Api('POST','teos/',self.uusi, function(data){
            	console.log(data._id)
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.href = '<?php echo site_url(); ?>sirkus-suomessa/sirkka-tietokanta/muokkaa-teosta?work_id='+data._id;
                }, 2000)
            })            
            

    }

}




var Teokset = function() {

    //ko.virtualElements.allowedBindings.group_id = true;

    Lookup('ryhma')

    var self = this;


    if(UGID == AGID ) {
        self.admin = ko.observable(true)
    } else {
        self.admin = ko.observable(false)
    }

    var group = ''

    if(GID) {
        var group = '&where={"group_id":'+GID+',"julkaistu":1}'
    }

    self.teokset = ko.observableArray();

    Api('GET','teos?sort=nimi'+group, null, function(data){

        //console.log(data)

        //self.artists.pushAll(data._items)

            $.each(data, function(index, item) {
                self.teokset.push(
                    ko.mapping.fromJS(item)
                )
            })     
    })


    self.saveRow = function(row) {

        patch = {group_id:parseInt(row.group_id())}

        Api('PATCH','teos/'+row._id(), patch, function() {
                console.log('saved')
            })

        // var user = $.ajax({
        //     method: 'PATCH',
        //     url: baseURL + 'users/'+row._id(),
        //     //async: false,
        //     dataType: "json",
        //     contentType: "application/json",
        //     data: JSON.stringify(patch),
        //     success: function (user) {
        //         },
        //     error: function (xhr, type, exception) {
        //         console.log(xhr)

        //         }
        //     });

        return true;
    }

    self.delRow = function(row) {

        Api('DELETE','teos/'+row._id(), null, function(){
            console.log('Deleted')
        })

        // var user = $.ajax({
        //     method: 'DELETE',
        //     url: baseURL + 'users/'+row._id(),
        //     //async: false,
        //     dataType: "json",
        //     contentType: "application/json",
        //     //data: JSON.stringify(patch),
        //     success: function (user) {
        //         console.log('del:'+row._id())
        //         },
        //     error: function (xhr, type, exception) {
        //         console.log(xhr)

        //         }
        //     });

        return true;
    }


   self.addRow = function() {

   }

    self.saveRow = function () {

    }

}

$(document).one('click','#ui-id-2', function() {
    // ko.applyBindings(new Teokset(), $('#teokset')[0]);

    ko.applyBindings(new Teos(), $('#teos')[0]);


})
 


 
</script>
