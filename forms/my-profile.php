
  <div class="alert alert-success" role="alert" id="OK">
      <span class="glyphicon glyphicon-check"  aria-hidden="true"></span>
  </div>

  <div class="container col-sm-12" id="artist">

          <h2>Oma profiili (My profile)
          <a href="#" data-toggle="tooltip" title="Suomalaiset tai Suomessa toimivat sirkustaiteilijat ja opettajat voivat lisätä profiilinsa Sirkka-tietokantaan tällä lomakkeella. Profiilin voivat täyttää myös sirkuskentässä työskentelevät muut taiteelliset ja tekniset ammattilaiset. 
          Circus artists and teachers settled in Finland can add their profile to the Sirkka database of Finnish circus. Also other artistic and technical professionals working frequently in the field of Finnish circus are welcomed to the database. "><span class="glyphicon glyphicon-question-sign"></span></a>
          </h2>
          Suomalaiset tai Suomessa toimivat sirkustaiteilijat ja opettajat voivat lisätä profiilinsa Sirkka-tietokantaan tällä lomakkeella. Profiilin voivat täyttää myös sirkuskentässä työskentelevät muut taiteelliset ja tekniset ammattilaiset. Tyhjäksi jätettyjä kenttiä ei julkaista.<br>
          Circus artists and teachers settled in Finland can add their profile to the Sirkka database of Finnish circus. Also other artistic and technical professionals working frequently in the field of Finnish circus are welcomed to the database. Empty fields are not published.<br>
          Viime päivitys (Last update):<span data-bind="text:prf_updated"></span>
          <br>
          <br>
          Pakolliset kentät on merkitty *:llä (Obligatory fields are marked with *)<br>
                    
<!-- <input type='hidden' class='form-control' data-bind='value:_id' /> -->
<div class="">
<input type='checkbox' class='' data-bind='value:prf_julkaistu' /> <label>Julkaistu (Published)*</label>
</div>
<label >Etunimi (First Name)* / Sukunimi (Family name)*</label><div class='clear'></div>
<input type='input' class='form-control' data-bind='value:prf_etunimi' style="width:30%;display:inline" />
<input type='input' class='form-control' data-bind='value:prf_sukunimi' style="width:69%;display:inline"/><br>
<label >Taiteilijanimet (Artist names)</label><input type='input' class='form-control' data-bind='value:prf_taiteilijanimet' /><br>
<!-- <label class=''>Sirkusryhmä (Circus company)</label>
<input type='hidden' class='group_id' data-bind='textInput:prf_group_id' />
<input type="text" name="ryhma" class="form-control ryhma" />
 -->
<label class=''>Syntymävuosi (Year of birth)</label>
 <span class="glyphicon glyphicon-question-sign"></span><div class='clear'></div>
<input type='input' class='form-control' data-bind='value:prf_syntymaaika' /><div class='clear'></div>

<label>Kansalaisuus (Nationality)</label><a href="#" data-toggle="tooltip" title="Tietoa ei julkaista, se näkyy vain tetokannan ylläpitäjälle. Will not be published, is shown only to the admin.">
 <span class="glyphicon glyphicon-question-sign"></span></a>
<div class='clear'></div>
<!-- Nav tabs -->

<div class="tabs-lang">
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#kansallisuus_fi" role="tab">FI</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#kansallisuus_en" role="tab">EN</a>
  </li>
</ul>
<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="kansallisuus_fi" role="tabpanel">  
  <input type='input' class='form-control' data-bind='value:prf_kansallisuus_fi' /></div>
  <div class="tab-pane" id="kansallisuus_en" role="tabpanel">
      <input type='input' class='form-control' data-bind='value:prf_kansallisuus_en' /></div>
</div></div>

 <label class=''>Verkkosivut (Websites)</label><a href="#" data-toggle="tooltip" title="WWW, Youtube, Vimeo, Facebook, Twitter yms. linkit  (Links to social media) <br>
 Erottele osoitteet omille riveille. Seprate into rows.">
 <span class="glyphicon glyphicon-question-sign"></span></a>
 <textarea class='form-control' data-bind='value:prf_verkkosivu' /></textarea>
<div>
  Anna ne yhteystietosi, joiden julkaisemisen avoimella verkkosivulla hyväksyt. (Please add the information that you agree in publishing on line.)  
</div>
<label class=''>Sähkopostiosoite (Email)</label><a href="#" data-toggle="tooltip" title="Sähköpostiosoite on suojattu roskapostiroboteilta.">
 <span class="glyphicon glyphicon-question-sign"></span></a>
<input type='input' class='form-control' data-bind='value:prf_sahkoposti' /><br>
<label class=''>Puhelinnumero (Phone number)</label><input type='input' class='form-control' data-bind='value:prf_puhelinnumero' /><br>
<label>Kuvat (Images)</label>
    <table><tr>
    <td><span data-bind="if:prf_kuva">
    <img data-bind="attr:{'src':'http://sirkusinfo.fi/'+ prf_kuva().split('/var/www/sirkusinfo/')[1] }" width="200px"/>
            <a data-bind="click:rmKuva1"><span class="glyphicons glyphicons-minus-sign"></span></a>
  </span>
    </td>
    <td><span data-bind="if:prf_kuva2">
    <img data-bind="attr:{'src':'http://sirkusinfo.fi/'+ prf_kuva2().split('/var/www/sirkusinfo/')[1] }" width="200px"/>
            <a data-bind="click:rmKuva2"><span class="glyphicons glyphicons-minus-sign"></span></a>
</span>
    </td>
    <td><span data-bind="if:prf_kuva3">
    <img data-bind="attr:{'src':'http://sirkusinfo.fi/'+ prf_kuva3().split('/var/www/sirkusinfo/')[1] }" width="200px"/>
            <a data-bind="click:rmKuva3"><span class="glyphicons glyphicons-minus-sign"></span></a>
</span>
    </td>
    </tr>
    <tr><td>
    <input type='file' class='form-control' data-bind='file:{data:prf_fileInput, name:prf_kuva}' />
    <label>Kuvaaja (Photographer)</label>
    <input class='form-control' data-bind='value:prf_kuvaaja' />
    </td>
    <td>
    <input type='file' class='form-control' data-bind='file:{data:prf_fileInput2, name:prf_kuva2}' />
    <label>Kuvaaja (Photographer)</label>
    <input class='form-control' data-bind='value:prf_kuvaaja2' />
    </td>
    <td>
    <input type='file' class='form-control' data-bind='file:{data:prf_fileInput3, name:prf_kuva3}' />
        <label>Kuvaaja (Photographer)</label>
    <input class='form-control' data-bind='value:prf_kuvaaja3' />
    </td></tr></table>

<label>Ammatti (Profession)</label>
<div class="tabs-lang">
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#ammatti_fi" role="tab">FI</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#ammatti_en" role="tab">EN</a>
  </li>
</ul>
<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="ammatti_fi" role="tabpanel"> 
  <input type='input' class='form-control' data-bind='value:prf_ammatti_fi' /></div>
  <div class="tab-pane " id="ammatti_en" role="tabpanel"> 
  <input type='input' class='form-control' data-bind='value:prf_ammatti_en' /></div>
</div></div>
<label>Toimenkuvat (Activities)</label>
<div class='clear'></div>
<span class="checks" style="display:inline-block">
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_esiintyja' />Esiintyjä (Performer)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_ohjaaja' /> Ohjaaja (Director)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_koreografi' />Koreografi (Choreographer)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_saveltaja' />Säveltäjä (Composer)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_aanisuunnittelija' />Äänisuunnittelija (Sound designer)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_lavastaja' />Lavastaja (Set designer)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_pukusuunnittelija' />Pukusuunnittelija (Costume designer)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_valosuunnittelija' />Valosuunnittelija (Light designer)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_videomediataiteilija' />Video- tai mediataiteilija (Video or media artist)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_tuottaja' />Tuottaja (Producer)
</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_sosiaalinen' />Sosiaalisen sirkuksen toimija (Social circus)
</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_sirkusopettaja' />Sirkusopettaja (Circus teacher)
</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_muutyo' />Muu (Other)</label></div>
</span>
<div class='clear'></div>

<div data-bind="visible:prf_muutyo">
<label >Muu, mikä? (Other, what?)</label>
<div class="tabs-lang">
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#muutyo_fi" role="tab">FI</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#muutyo_en" role="tab">EN</a>
  </li>
</ul>
<div class="tab-content">
  <div class="tab-pane active" id="muutyo_fi" role="tabpanel"> 
<input type='input' class='form-control' data-bind='value:prf_tyomika_fi' /></div>
  <div class="tab-pane" id="muutyo_en" role="tabpanel">  
<input type='input' class='form-control' data-bind='value:prf_tyomika_en' /></div>
</div></div>
</div>
<div class='clear'></div>

<label>Sirkuslajit (Circus skills)
<a href="#" data-toggle="tooltip" title="Voit lisätä omat lajisi kohtaan Muu. Jos opetat, kerro sirkustaitosi lajikohtaisesti.  / Please add your discipline to  field 'Other'. If you teach, please add your circus skills in more detail.">
 <span class="glyphicon glyphicon-question-sign"></span></a></label>
 <div class='clear'></div>

<span class="checks" style="display:inline-block">
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_ilmaakrobaatti' /> Ilma-akrobatia (Aerial acrobatics)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_akrobaatti' /> Akrobatia (Acrobatics)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_jongloori' /> Jongleeraus (Juggling)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_klovni' /> Klovneria (Clownery)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_taikuri' /> Taikuus (Magic)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_tulitaiteilija' /> Tulitaide (Fire Art)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_tasapainoilu' /> Tasapainoilu (Balancing)</label></div>
<div class='checkbox-inline'><label> <input type='checkbox' class='' data-bind='checked:prf_muu' />Muu</label></div>
</span>
<span data-bind="visible:prf_muu">
<label >Muu, mikä? (Other, what?)</label>
<div class="tabs-lang">
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#muumika_fi" role="tab">FI</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#muumika_en" role="tab">EN</a>
  </li>
</ul>
<div class="tab-content">
  <div class="tab-pane active" id="muumika_fi" role="tabpanel"> 
<input type='input' class='form-control' data-bind='value:prf_muumika_fi' /></div>
  <div class="tab-pane" id="muumika_en" role="tabpanel">  
<input type='input' class='form-control' data-bind='value:prf_muumika_en' /></div>
</div>
</div></span>

<label class='full'>Kerro itsestäsi taitelijana. Esittele itsesi mahdolliselle yhteistyökumppanille (Introduce yourself as an artists to possible new partners) 
<a href="#" data-toggle="tooltip" title="Vapaa tekstikenttä vapaamuotoiselle esittelylle. ">
 <span class="glyphicon glyphicon-question-sign"></span></a>
</label>

<div class="tabs-lang">
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#vapaateksti_fi" role="tab">FI</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#vapaateksti_en" role="tab">EN</a>
  </li>
</ul>
<div class="tab-content">
  <div class="tab-pane active" id="vapaateksti_fi" role="tabpanel"> 
  <textarea class='form-control' data-bind='textInput:prf_vapaateksti_fi' ></textarea></div>
  <div class="tab-pane" id="vapaateksti_en" role="tabpanel">  
  <textarea class='form-control' data-bind='textInput:prf_vapaateksti_en' ></textarea></div>
</div></div>

<label >Sirkusalan koulutus (Professional circus education)
<a href="#" data-toggle="tooltip" title="  Sirkusalan koulutus, muu oleellinen tai sirkusta tukeva koulutus
 Professional circus education, other relevant education ">
 <span class="glyphicon glyphicon-question-sign"></span></a></label>
<div class="tabs-lang">
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#koulutus_fi" role="tab">FI</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#koulutus_en" role="tab">EN</a>
  </li>
</ul>
<div class="tab-content">
  <div class="tab-pane active" id="koulutus_fi" role="tabpanel">
<textarea class='form-control' data-bind='textInput:prf_koulutus_fi' ></textarea></div>
  <div class="tab-pane " id="koulutus_fi" role="tabpanel">
<textarea class='form-control' data-bind='textInput:prf_koulutus_en' ></textarea></div>
</div></div>

<label >Taiteellinen työ (Artistic activity)
<a href="#" data-toggle="tooltip" title="Vapaa tekstikenttä vapaamuotoiselle esittelylle. ">
 <span class="glyphicon glyphicon-question-sign"></span></a></label>

<div class="tabs-lang">
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#tait_fi" role="tab">FI</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#tait_en" role="tab">EN</a>
  </li>
</ul>
<div class="tab-content">
  <div class="tab-pane active" id="tait_fi" role="tabpanel">
<textarea class='form-control' data-bind='textInput:prf_taiteellinentyo_fi' ></textarea></div>
  <div class="tab-pane " d="tait_en" role="tabpanel">
<textarea class='form-control' data-bind='textInput:prf_taiteellinentyo_en' ></textarea></div>
</div></div>

<label >Opetustyö<a href="#" data-toggle="tooltip" title="
* Opettamasi sirkuslajit? 
* Opetatko sirkuslajien perusteita vai vain omaa lajiasi – millä tasolla?
* Mnkä ikäisiä opetat?
* Opetatko  + harrastajia + ammattilaisia + erityisryhmiä, mitä?__________ 
* Opetuskielet:
* Pedagogiset opinnot:
* Kerro opetuskäsityksestäsi:
* Oletko kiinnostunut säännöllisestä vai keikkaluontoisesta opetustyöstä? 
* Kotipaikkakunta/ Missä voit opettaa (kaupungit, alueet)">
 <span class="glyphicon glyphicon-question-sign"></span></a>
 (Educational employment)<a href="#" data-toggle="tooltip" title="
 * Which disciplines do you teach?
* Do you also teach general circus skills or only your own circus dicipline - on what level?
* Which age groups do you teach?
* Do you teach + amateurs + professionals + people with special needs, which?__________
* In which languages do you teach: 
* Pedagogical education:
* Please tell more of your conception of pedagogy:
* Are you interested in getting a regular or short-term work as a circus teacher? 
* Hometown/ locations where you can teach? (cities, regions)">
 <span class="glyphicon glyphicon-question-sign"></span></a>
</label>

<div class="tabs-lang">
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#opetus_fi" role="tab">FI</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#opetus_en" role="tab">EN</a>
  </li>
</ul>
<div class="tab-content">
  <div class="tab-pane active" id="opetus_fi" role="tabpanel">
  <textarea class='form-control' data-bind='textInput:prf_opetustyo_fi' ></textarea></div>
<div class="tab-pane " id="opetus_en" role="tabpanel">
  <textarea class='form-control' data-bind='textInput:prf_opetustyo_en' ></textarea></div>
</div></div>

<div data-bind="visible:prf_sirkusopettaja">
<label >Sirkusopettajan valmiudet (Please tell more of yourself as a circus teacher)</label>
<label class='full'>Esittele itsesi mahdolliselle yhteistyökumppanille (Introduce yourself to possible new partners) </label>
<a href="#" data-toggle="tooltip" title="Vapaa tekstikenttä vapaamuotoiselle esittelylle. ">
 <span class="glyphicon glyphicon-question-sign"></span></a>

<div class="tabs-lang">
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#valmius_fi" role="tab">FI</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#valmius_en" role="tab">EN</a>
  </li>
</ul>
<div class="tab-content">
  <div class="tab-pane active" id="valmius_fi" role="tabpanel">
  <textarea class='form-control' data-bind='textInput:prf_opettajavalmius_fi' ></textarea></div>
<div class="tab-pane " id="valmius_en" role="tabpanel">
  <textarea class='form-control' data-bind='textInput:prf_opettajavalmius_en' ></textarea></div>
</div>
</div></div>
<br><br>
<button type="button" class="btn btn-primary" data-bind='click:saveUusi'>OK</button>

<div style="border:1px solid #000; padding: 6px">
      <h3>Käyttäjän tiedot (User account)</h3>
          Käyttäjätietoja ei julkaista (User information is not public).<br>
              <label class="control-label">Nimi (Name)* </label><input class="form-control" data-bind="value: user_name" /><div class='clear'></div>
              <label class="control-label">Puhelin (Phone)* </label><input class="form-control" data-bind="value: user_puhelin" /><div class='clear'></div>

        <label class="control-label">Sähköposti (Email)* </label><input class="form-control" data-bind="value: user_email" />
        <div class='clear'></div>
              <label class="control-label">Käyttäjätunnus (User name)* </label><input class="form-control" data-bind="value: user_username" /><div class='clear'></div>
              <label class="control-label">Salasana (Password) </label><input class="form-control" data-bind="value: user_password" /><div class='clear'></div>

              <label class="control-label">Salasana uudestaan (Retype passsword)</label><input class="form-control" data-bind="value: user_retype" /><div class='clear'></div>
              <button type="button" class="btn btn-primary" title="Tallenna(Save)" data-bind='click: saveUser,disable:!user_username.isValid()||!user_email.isValid()||!user_retype.isValid()'>OK</button>

      </div>
              <a href="#" id="logout" title="Kirjaudu ulos" class="btn btn-warning">Logout </a>
  </div> 
  
<script type="text/javascript">
  

var Artist = function() {


    Lookup('ryhma')
    Lookup('sirkuslajit')

    self.prf_etunimi=ko.observable();
    self.prf_sukunimi=ko.observable();
    self.prf_updated = ko.observable();
    self.prf_taiteilijanimet=ko.observable();
    self.prf_syntymaaika=ko.observable();
    self.prf_kansallisuus_fi=ko.observable();
    self.prf_kansallisuus_en=ko.observable();
    self.prf_verkkosivu=ko.observable();
    self.prf_sahkoposti=ko.observable();
    self.prf_puhelinnumero=ko.observable();
    self.prf_ammatti_fi=ko.observable();
    self.prf_ammatti_en=ko.observable();
    self.prf_ilmaakrobaatti=ko.observable();
    self.prf_akrobaatti=ko.observable();
    self.prf_jongloori=ko.observable();
    self.prf_klovni=ko.observable();
    self.prf_taikuri=ko.observable();
    self.prf_tasapainoilu=ko.observable();
    self.prf_tulitaiteilija=ko.observable();
    self.prf_muu=ko.observable();
    self.prf_muumika_fi=ko.observable();
    self.prf_muumika_en=ko.observable();
    self.prf_ammatti_fi=ko.observable();
    self.prf_ammatti_en=ko.observable();
    self.prf_esiintyja=ko.observable();
    self.prf_ohjaaja=ko.observable();
    self.prf_tuottaja=ko.observable();
    self.prf_sosiaalinen=ko.observable();
    self.prf_koreografi=ko.observable();
    self.prf_saveltaja=ko.observable();
    self.prf_aanisuunnittelija=ko.observable();
    self.prf_lavastaja=ko.observable();
    self.prf_pukusuunnittelija=ko.observable();
    self.prf_valosuunnittelija=ko.observable();
    self.prf_videomediataiteilija=ko.observable();
    self.prf_sirkusopettaja=ko.observable();
    self.prf_muutyo=ko.observable();
    self.prf_tyomika_fi=ko.observable();
    self.prf_tyomika_en=ko.observable();
    self.prf_vapaateksti_fi=ko.observable();
    self.prf_vapaateksti_en=ko.observable();
    // self.prf_furorpoeticus_fi=ko.observable();
    // self.prf_furorpoeticus_en=ko.observable();
    self.prf_koulutus_fi=ko.observable();
    self.prf_koulutus_en=ko.observable();
    self.prf_taiteellinentyo_fi=ko.observable();
    self.prf_taiteellinentyo_en=ko.observable();
    self.prf_opetustyo_fi=ko.observable();
    self.prf_opetustyo_en=ko.observable();
    self.prf_opettajavalmius_fi=ko.observable();
    self.prf_opettajavalmius_en=ko.observable();
    self.prf_kuva=ko.observable('');
    self.prf_fileInput=ko.observable();
    self.prf_kuva2=ko.observable('');
    self.prf_fileInput2=ko.observable();
    self.prf_kuva3=ko.observable('');
    self.prf_fileInput3=ko.observable();
    self.prf_kuvaaja=ko.observable('');
    self.prf_kuvaaja2=ko.observable('');
    self.prf_kuvaaja3=ko.observable('');
    self.prf_userid=ko.observable(UID);
    self.prf_julkaistu=ko.observable(0);
    self.prf_group_id=ko.observable(UGID);
    self.prf_id=ko.observable();
    
    self.prf_original = ko.observable();

    if(UID > 0) {

    ko.computed(function () { 


        return Api('GET','toimijat?where=userid=='+UID, null, function(res) {

            console.log(res[0])

            if(res.length > 0) {

              self.prf_original(res[0])

                var data = res[0]
                    self.prf_id(data._id)
                    self.prf_etunimi(data.etunimi);
                    self.prf_sukunimi(data.sukunimi);
                    self.prf_updated(data._updated);
                    self.prf_taiteilijanimet(data.taiteilijanimet);
                    self.prf_syntymaaika(data.syntymaaika);
                    self.prf_kansallisuus_fi(JSON.parse(data.kansallisuus).fi);
                    self.prf_kansallisuus_en(JSON.parse(data.kansallisuus).en);
                    self.prf_verkkosivu(data.verkkosivu);
                    self.prf_sahkoposti(data.sahkoposti);
                    self.prf_puhelinnumero(data.puhelinnumero);
                    self.prf_ammatti_fi(JSON.parse(data.ammatti).fi);
                    self.prf_ammatti_en(JSON.parse(data.ammatti).en);
                    self.prf_ilmaakrobaatti(data.ilmaakrobaatti);
                    self.prf_akrobaatti(data.akrobaatti);
                    self.prf_jongloori(data.jongloori);
                    self.prf_klovni(data.klovni);
                    self.prf_taikuri(data.taikuri);
                    self.prf_tasapainoilu(data.tasapainoilu)
                    self.prf_tulitaiteilija(data.tulitaiteilija);
                    self.prf_muu(data.muu);
                    self.prf_muumika_fi(JSON.parse(data.muumika).fi);
                    self.prf_muumika_en(JSON.parse(data.muumika).en);
                    self.prf_esiintyja(data.esiintyja);
                    self.prf_ohjaaja(data.ohjaaja);
                    self.prf_tuottaja(data.tuottaja);
                    self.prf_sosiaalinen(data.sosiaalinen);
                    self.prf_koreografi(data.koreografi);
                    self.prf_saveltaja(data.saveltaja);
                    self.prf_aanisuunnittelija(data.aanisuunnittelija);
                    self.prf_lavastaja(data.lavastaja);
                    self.prf_pukusuunnittelija(data.pukusuunnittelija);
                    self.prf_valosuunnittelija(data.valosuunnittelija);
                    self.prf_videomediataiteilija(data.videomediataiteilija);
                    self.prf_sirkusopettaja(data.sirkusopettaja);
                    self.prf_muutyo(data.muutyo);
                    self.prf_tyomika_fi(JSON.parse(data.tyomika).fi);
                    self.prf_tyomika_en(JSON.parse(data.tyomika).en);
                    self.prf_vapaateksti_fi(JSON.parse(data.vapaateksti).fi);
                    self.prf_vapaateksti_en(JSON.parse(data.vapaateksti).en);
                    // self.prf_furorpoeticus_fi(JSON.parse(data.furorpoeticus).fi);
                    // self.prf_furorpoeticus_en(JSON.parse(data.furorpoeticus).en);
                    self.prf_koulutus_fi(JSON.parse(data.koulutus).fi);
                    self.prf_koulutus_en(JSON.parse(data.koulutus).en);
                    self.prf_taiteellinentyo_fi(JSON.parse(data.taiteellinentyo).fi);
                    self.prf_taiteellinentyo_en(JSON.parse(data.taiteellinentyo).en);
                    self.prf_opetustyo_fi(JSON.parse(data.opetustyo).fi);
                    self.prf_opetustyo_en(JSON.parse(data.opetustyo).en);
                    self.prf_opettajavalmius_fi(JSON.parse(data.opettajavalmius).fi);
                    self.prf_opettajavalmius_en(JSON.parse(data.opettajavalmius).en);
                    if(data.kuva) {
                        self.prf_kuva(data.kuva);}
                    if(data.kuva2) {
                        self.prf_kuva2(data.kuva2);
                    }
                    if(data.kuva3) {
                        self.prf_kuva3(data.kuva3);
                    }
                    self.prf_kuvaaja(data.kuvaaja);
                    self.prf_kuvaaja2(data.kuvaaja2);
                    self.prf_kuvaaja3(data.kuvaaja3);
                    //self.prf_userid(data.userid);
                    self.prf_julkaistu(data.julkaistu);
                    self.prf_group_id(data.group_id);

                    }
                }
            )
        }, self)
    }
    
    self.rmKuva1 = function() {
      self.teos_fileInput(null);
      self.teos_kuva(null);
      self.teos_kuvaaja(null)
    }

    self.rmKuva2 = function() {
      self.teos_fileInput2(null);
      self.teos_kuva2(null);
      console.log('rm '+self.teos_kuva2())
      self.teos_kuvaaja2(null)
    }

    self.rmKuva3 = function() {
      self.teos_fileInput3(null);
      self.teos_kuva3(null);
      self.teos_kuvaaja3(null)
    }

    self.saveUusi = function () {

            self.prf = {
                    etunimi:self.prf_etunimi(),
                    sukunimi:self.prf_sukunimi(),
                    taiteilijanimet:self.prf_taiteilijanimet(),
                    syntymaaika:self.prf_syntymaaika(),
                    kansallisuus:ko.toJSON({fi:self.prf_kansallisuus_fi(), en:self.prf_kansallisuus_en()}),
                    verkkosivu:self.prf_verkkosivu(),
                    sahkoposti:self.prf_sahkoposti(),
                    puhelinnumero:self.prf_puhelinnumero(),
                    ammatti:ko.toJSON({fi:self.prf_ammatti_fi(), en:self.prf_ammatti_en()}),
                    ilmaakrobaatti:self.prf_ilmaakrobaatti(),
                    akrobaatti:self.prf_akrobaatti(),
                    tasapainoilu:self.prf_tasapainoilu(),
                    jongloori:self.prf_jongloori(),
                    klovni:self.prf_klovni(),
                    taikuri:self.prf_taikuri(),
                    tulitaiteilija:self.prf_tulitaiteilija(),
                    muu:self.prf_muu(),
                    muumika:ko.toJSON({fi:self.prf_muumika_fi(),en:self.prf_muumika_en()}),
                    esiintyja:self.prf_esiintyja(),
                    ohjaaja:self.prf_ohjaaja(),
                    tuottaja:self.prf_tuottaja(),
                    sosiaalinen:self.prf_sosiaalinen(),
                    sirkusopettaja:self.prf_sirkusopettaja(),
                    koreografi:self.prf_koreografi(),
                    saveltaja:self.prf_saveltaja(),
                    aanisuunnittelija:self.prf_aanisuunnittelija(),
                    lavastaja:self.prf_lavastaja(),
                    pukusuunnittelija:self.prf_pukusuunnittelija(),
                    valosuunnittelija:self.prf_valosuunnittelija(),
                    videomediataiteilija:self.prf_videomediataiteilija(),
                    sirkusopettaja:self.prf_sirkusopettaja(),
                    muutyo:prf_muutyo(),
                    tyomika:ko.toJSON({fi:self.prf_tyomika_fi(),en:self.prf_tyomika_en()}),
                    vapaateksti:ko.toJSON({fi:self.prf_vapaateksti_fi(),en:self.prf_vapaateksti_en()}),
                    //furorpoeticus:ko.toJSON({fi:self.prf_furorpoeticus_fi(),en:self.prf_furorpoeticus_en()}),
                    koulutus:ko.toJSON({fi:self.prf_koulutus_fi(),en:self.prf_koulutus_en()}),
                    taiteellinentyo:ko.toJSON({fi:self.prf_taiteellinentyo_fi(),en:self.prf_taiteellinentyo_en()}),
                    opetustyo:ko.toJSON({fi:self.prf_opetustyo_fi(),en:self.prf_opetustyo_en()}),
                    opettajavalmius:ko.toJSON({fi:self.prf_opettajavalmius_fi(),en:self.prf_opettajavalmius_en()}),
                    kuva:self.prf_fileInput(),
                    kuva2:self.prf_fileInput2(),
                    kuva3:self.prf_fileInput3(),
                    kuvaaja:self.prf_kuvaaja(),
                    kuvaaja2:self.prf_kuvaaja(),
                    kuvaaja3:self.prf_kuvaaja(),
                    userid:parseInt(UID),
                    julkaistu:self.prf_julkaistu(),
                    group_id:parseInt(self.prf_group_id())
            }


        for(key in self.prf) {
          if(self.prf[key] == self.prf_original()[key]) {
            delete self.prf[key]
          }
        }

        if(self.prf_id() > 0) {

            Api('PATCH','toimijat/'+self.prf_id(), self.prf, function(data){
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
            })

        } else {

            Api('POST','toimijat/',self.prf, function(data){
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
            })            
            
        }
    }

    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghijklmnoprstuvwz";
    var string_length = 32;
    var salt = "";

    // Lookup('ryhma');

    for(var i=0; i<string_length; i++){
      var randomPos = Math.floor( Math.random() * chars.length );
      salt += chars.substr(randomPos, 1);
    }

    if(UGID == AGID) {
        self.admin = ko.observable(true)
        } else {
        self.admin = ko.observable(false)
      }

      self.user_name = ko.observable('').extend({ required: true })
      self.user_puhelin = ko.observable()
      self.user_username = ko.observable('').extend({ required: true })
      self.user_email  = ko.observable('').extend({email:true})
      self.user_password  = ko.observable('').extend(
                  { passwordComplexity:
                      { onlyIf: function() { return self.user_password != ''}}
                  })            
      self.user_retype = ko.observable('').extend({ areSame:self.user_password });
      self.user_group_id = ko.observable(0).extend({ required: true })

     if(UID > 0) {

      ko.computed(function() {
        return Api('GET', 'users/'+UID+'?embedded={"group_id":1}', null, function(data) {
                    self.user_name(data.name)
                    self.user_puhelin(data.puhelin)
                    self.user_username(data.username)
                    self.user_email(data.email)
                    self.user_group_id(data.group_id)
        })

      },self)

    } 

    // self.retype.extend( areSame: { params: self.password, 
    //     message: "Salasanat eivät täsmää" })

    //ko.mapping.fromJS(data, {}, self.esitys);


    self.saveUser = function () {

            // console.log(self.group_id())

            var md5pass = $.md5(self.user_password()+salt)

            self.uusi = {
                    name: self.user_name(),
                    puhelin: self.user_puhelin(),
                    username: self.user_username(),
                    email: self.user_email(),
                    password: md5pass+':'+salt,
                    group_id: parseInt(self.user_group_id())
            }

      
            Api('PATCH','users/'+UID, self.uusi, function() {
                $('.alert').toggle();
                window.setTimeout(function() {
                    location.reload();
                }, 2000)
            })

        
    }

}

var Artists = function() {

    //ko.virtualElements.allowedBindings.group_id = true;

    Lookup('ryhma')

    var self = this;

    var group = ''

    if(GID) {
        var group = '&where=group_id=='+GID
    } 

    self.artists = ko.observableArray();

    Api('GET','toimijat?sort=sukunimi,etunimi'+group, null, function(data){

        //console.log(data)

        //self.artists.pushAll(data._items)

            $.each(data, function(index, item) {
                self.artists.push(
                    ko.mapping.fromJS(item)
                )
            })     
    })


    self.saveRow = function(row) {

        patch = {group_id:parseInt(row.group_id())}

        Api('PATCH','users/'+row._id(), patch, function() {
                console.log('saved')
            })


        return true;
    }

    self.delRow = function(row) {

        Api('DELETE','users/'+row._id(), null, function(){
            console.log('Deleted')
        })



        return true;
    }


   self.addRow = function() {

   }

    self.saveRow = function () {

    }

}

$(document).ready(function() {

        //ko.applyBindings(new Artists(), $('#artists')[0]);

        ko.applyBindings(new Artist(), $('#artist')[0]);
});

    

 


 

</script>
