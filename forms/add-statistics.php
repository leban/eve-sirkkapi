	<div class="alert alert-success" role="alert" id="OK">
  		<span class="glyphicon glyphicon-check"  aria-hidden="true"></span>
	</div>
	<div id="tilasto">
    <h2>Esitykset <span data-bind='text: esitys().length'>&nbsp;</span></h2>
    <table data-bind='visible: esitys().length > 0'>
        <thead>
            <tr>
            	<th>Tyyppi</th>
                <th>Esitys</th>
                <th>Teos</th>
                <th>Ryhmä</th>
                <th>Paikka</th>
                <th>Kaupunki</th>
                <th>Maa</th>
                <th>Ulkoilmaes.</th>
                <th>Näyttömö</th>
                <th>Muu es.</th>
                <th>Yhteise.</th>
                <th>Vieras</th>
                <th>Lukum.</th>
                <th>Myydyt</th>
                <th>Yleisö yht.</th>
                <th>Workshop</th>
            </tr>
        </thead>
        <tbody data-bind='foreach: esitys'>
            <tr>
            	<td>
                <select data-bind="value: tyyppi, selectmenu: { width: 100 }, options: availableType, optionsValue: 'typeName', optionsText: 'typeName', optionsCaption: 'Valitse...',event: { change: $parent.saveRow }">
            </select>
            </td>
                <td><input class='required date' data-bind="datepicker: {dateFormat:'d. m. yy', value:new Date(pvm * 1000)},  
                event: { change: $parent.saveDateRow } "/></td>
                <td><input class='required' data-bind='value: main_id.nimi, event: { change: $parent.saveRow }' /></td>
                <td><input class='required ' data-bind="value: main_id.esittaja, event: { change: $parent.saveRow }" /></td>
                <td><input class='required ' data-bind='value: esid.paikka, event: { change: $parent.saveRow }' /></td>
                <td><input class='required ' data-bind='value: esid.kaupunki, event: { change: $parent.saveRow }' /></td>
                <td><input class='required ' data-bind='value: esid.maa, event: { change: $parent.saveRow }' /></td>

                <td><input type="checkbox" value="ulko" class='number' data-bind='checked: ulko, event: { change: $parent.saveRow }' /></td>
                <td><input type="checkbox" value="nayttamo" data-bind='checked: nayttamo, event: { change: $parent.saveRow }' /></td>
                <td><input type="checkbox" value="muu" data-bind='checked: muu, event: { change: $parent.saveRow }' /></td>
                <td><input type="checkbox" value="yhteis" data-bind='checked: yhteis, event: { change: $parent.saveRow }' /></td>
                <td><input class='required ' data-bind='value: vieras, event: { change: $parent.saveRow },valueUpdate:"change"' /></td>
                <td><input class='required number' data-bind='value: kpl, event: { change: $parent.saveRow }' /></td>
                <td><input class='required number' data-bind='value: myyty, event: { change: $parent.saveRow }' /></td>
                <td><input class='required number' data-bind='value: yleiso, event: { change: $parent.saveRow }' /></td>
                <td><input type="checkbox" class='' data-bind='checked: workshop, event: { change: $parent.saveRow }' /></td>
                <td><a href='#' data-bind='click: $root.removeEsitys'>Delete</a></td>
            </tr>
        </tbody>
    </table>
 
    <button data-bind='click: addEsitys'>Add </button>
    <button data-bind='click: save, enable:esitys().length > 0' type='submit'>Submit</button>

</div>

<script type="text/javascript">
	
var Tilasto = function() {
    var self = this;

	var groupid = '&where=group_id=='+UGID;

    self.esitys = ko.observableArray([
        ]);
    
    ko.computed(function () { 

        return Api('GET','esitys/'+embed+groupid+pvmyear, null, function(data) {

            self.esitys.pushAll(data)

        })
    });

    //getAPI(baseURL+embed,'GET');
    
    items = ko.observableArray([]);

    var Type = function(name) {
        this.typeName = name;
    };

    availableType = ko.observableArray([
        new Type("Kotimaan esitys"),
        new Type("Kv esitys"),
        new Type("Kotimainen vieras"),
        new Type("Ulkom. vieras"),
        new Type("Sosiaalinen sirkus"),
        new Type("Yleisötyö"),
        ])
        



    //console.log(self.esitys)

    //ko.mapping.fromJS(data, {}, self.esitys);

    self.tallenna = ko.observableArray([])

    self.saveRow = function(row) {
        //console.log(row)

        self.tallenna.remove( function (item) { 
            return item._id == row._id;
            } )
        self.tallenna.push(row);
        return true;
    }

    self.saveDateRow = function() {
        self.tallenna.remove( function (item) { 
            //console.log(item.pvm)
            return item._id == this._id;
            } )
        self.tallenna.push(this)

    }

    self.addEsitys = function() {

        self.esitys.push({
            pvm: moment().format('DD. MM. Y'),
            kpl: 1,
            myyty:0,
            yleiso:0,
            tyyppi: "Kotimaan esitys",
            vieras:"",
            workshop: 0,
            yhteis: 0,
            ulko: 0,
            nayttamo: 0,
            muu: 0,
            main_id: {
                nimi:"",
                esittaja:""
            },
            esid: { 
                paikka:"",
                kaupunki:"",
                maa:"Suomi"
            }
        });
    };
 
    self.removeEsitys = function(data) {
        self.esitys.remove(data);
    };
 
    self.save = function(form) {

        $.each(self.tallenna(), function(i, val) {

            if(val.tyyppi == undefined) {
                tyyppi = "Kotimaan esitys"
            } else {
                tyyppi = val.tyyppi
            }

            if (!("_id" in val) ) {
                
                //create new items

            _teos = {
                // _id: val.main_id._id,
                nimi: val.main_id.nimi,
                esittaja: val.main_id.esittaja,
                sirkka: false,
                esitys: false,
                julkaistu: false,
                user_id: 42


            }

            var data = $.ajax({
                method: 'POST',
                url: baseURL + 'teos/',
                async: false,
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify(_teos),
                success: function (data) {
                    console.log(data)
                    main_ref = data._id
                    },
                error: function (xhr, type, exception) {
                        // Do your thing
                        console.log(xhr)

                    }
                });

            _venue = {
                // _id: val.esid._id,
                paikka: val.esid.paikka,
                kaupunki: val.esid.kaupunki,
                maa: val.esid.maa
            }

            var data = $.ajax({
                method: 'POST',
                url: baseURL + 'venue/',
                async: false,
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify(_venue),
                success: function (data) {
                    console.log(data)
                    esid_ref = data._id
                    //return "" JSON.stringify(data);
                    },
                error: function (xhr, type, exception) {
                        console.log(xhr)
                        // Do your thing
                    }
                });            

            _esitys = {
                pvm: new Date(val.pvm).valueOf(),
                kpl: parseInt(val.kpl),
                myyty: parseInt(val.myyty),
                yleiso: parseInt(val.yleiso),
                tyyppi: tyyppi,
                vieras: val.vieras,
                workshop: val.workshop,
                yhteis: val.yhteis,
                ulko:val.ulko,
                nayttamo:val.nayttamo,
                muu: val.muu,
                main_id: main_ref,
                esid: esid_ref


            }

            console.log(JSON.stringify(_esitys))


            Api('PATCH', 'esitys'+ID, _esitys, function() {

            })


            } else {

                //edit existing

            _esitys = {
                _id: val._id,
                pvm: new Date(val.pvm).valueOf(),
                kpl: parseInt(val.kpl),
                myyty: parseInt(val.myyty),
                yleiso: parseInt(val.yleiso),
                tyyppi: tyyppi,
                vieras: val.vieras,
                workshop: val.workshop,
                yhteis: val.yhteis,
                ulko:val.ulko,
                nayttamo:val.nayttamo,
                muu: val.muu
            }

            console.log(JSON.stringify(val))

                var esitys = $.ajax({
                    method: 'PATCH',
                    url: baseURL + 'esitys/'+_esitys._id,
                    //async: false,
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(_esitys),
                    success: function (data) {
                        },
                    error: function (xhr, type, exception) {
                        console.log(xhr)

                        }
                    });
             
            _teos = {
                _id: val.main_id._id,
                nimi: val.main_id.nimi,
                esittaja: val.main_id.esittaja,

            }

            console.log(JSON.stringify(_teos))

                var data = $.ajax({
                    method: 'PATCH',
                    url: baseURL + 'teos/'+_teos._id,
                    //async: false,
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(_teos),
                    success: function (data) {
                        },
                    error: function (xhr, type, exception) {
                            console.log(xhr)
                        }
                    });
            

            _venue = {
                _id: val.esid._id,
                paikka: val.esid.paikka,
                kaupunki: val.esid.kaupunki,
                maa: val.esid.maa
            }

             console.log(JSON.stringify(_venue))

                var data = $.ajax({
                    method: 'PATCH',
                    url: baseURL + 'venue/'+_venue._id,
                    //async: false,
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(_venue),
                    success: function (data) {
                            },
                    error: function (xhr, type, exception) {
                            console.log(xhr)
                        }
                    });            
                
            }
        });
    };
};


$(document).one('click','#ui-id-4', function() {

    ko.applyBindings(new Tilasto(), $('#tilasto')[0]);

})

    


</script>