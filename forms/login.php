
	<div class="" id="login">
	        <table class="taho form-horizontal col-sm-12" >
	        <tr class="row"><td>
	            <label class="col-sm-2 control-label">Käyttäjätunnus:</label> <input class="form-control" data-bind="textInput: login" /></td></tr>

        <tr class="row"><td>
		            <label class="col-sm-2 control-label">Salasana:</label> <input class="form-control" data-bind="textInput: password" /><br></td></tr>
        <tr class="row"><td>
		            <button type="button" class="btn btn-primary" data-bind='click: log, disable:!login.isValid()||!password.isValid()'>OK</button><br></td></tr>
        <tr class="row"><td>
                        <form name="input" action="http://api.sirkusinfo.fi/mail" method="get">
                        <label class="cold-sm-6 control-label">Unohtuiko salasana?</label><br>
						<input type="submit" class=" from-control btn btn-info" value="Lähetä">
						</form>

			</td></tr>
	    </table>
</div>

<script type="text/javascript">

	var Login = function() {

    ko.virtualElements.allowedBindings.group_id = true;

    var self = this;
    var pass = '';
    var md5pass = '';
    var salt = '';

    //getAPI(baseURL+embed,'GET');

    self.login = ko.observable('').extend({ required: true })
    self.password  = ko.observable('').extend({ required: true })
    self.email = ko.observable();    

    // self.retype.extend( areSame: { params: self.password, 
    //     message: "Salasanat eivät täsmää" })

    //ko.mapping.fromJS(data, {}, self.esitys);


    self.log = function () {


        Api('GET','users?projection={"password":1,"_id":1,"group_id":1}&where=username=='+self.login(),null, function(data) {

                    console.log(data)

                    if( data.length > 0 ) {
                    
                                        md5pass = data[0].password.split(':')[0]
                                        salt = data[0].password.split(':')[1]
                                        pass = $.md5(self.password()+salt)
                                        if( pass != md5pass) {
                                            console.log(pass+'!='+md5pass)
                                            alert('Väärä käyttäjätunnus tai salasana!')
                                        } else {
                                            var UID = data[0]._id
                                            var UGID = data[0].group_id
                                            Cookies.set('UID', UID)
                                            Cookies.set('UGID', UGID)
                                                                    
                                            location.href = '<?php echo site_url(); ?>/sirkus-suomessa/sirkka-tietokanta/oma-sirkka';
                                        }
                                } else {
                                    alert('Käyttäjätunnusta ei löydy')
                                    return false;
                                }            
                                
            })
        }


}




$(document).ready(function() {

    // ko.applyBindings(new Admin(), $('#login')[0]);


    ko.applyBindings(new Login(), $('#login')[0]);

})
 

</script>