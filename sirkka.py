# -*- coding: utf-8 -*-


import os, sys
#from PIL import Image
from wand.image import Image
from io import BytesIO
import base64
from pprint import pprint
import uuid
from mimetypes import guess_extension, guess_type

# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText

from sqlalchemy.ext.automap import automap_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import Session
from sqlalchemy import create_engine, MetaData, Table, Column, ForeignKey, or_
from sqlalchemy import * #Table, models.Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship

from bson import ObjectId
from flask import Flask
# from flask_cors import CORS, cross_origin
from eve.utils import str_type

from eve import Eve
# import settings

# from eve_elastic import Elastic

from eve_sqlalchemy import SQL
from eve_sqlalchemy.decorators import registerSchema
SIRKKA = create_engine(
        "mysql+pymysql://sirkusinfo:Cr2xvm8Pzs4KPDjA@localhost/sdb",
            )

session = Session(SIRKKA)

metadata = MetaData()

metadata.reflect(SIRKKA)

Base = automap_base(metadata=metadata)

# +--------------------------+
# | Tables_in_sirkka_dev     |
# +--------------------------+
# | esiintyja                |
# | ryhma                    |
# | sirkka_esitys_agp        |
# | sirkka_esitys_esiintyjat |
# | sirkka_esitys_esityspv   |
# | sirkka_esitys_esityspvm  |
# | sirkka_esitys_main       |
# | sirkka_esitys_sosmed     |
# | sirkka_esitys_verkko     |
# | users                    |
# +--------------------------+


class User(Base):
    __tablename__ = 'users'
    # @hybrid_property
    # def _id(self):
    #     """
    #     Eve backward compatibility
    #     """
    #     return self.id

class Toimijat(Base):
    __tablename__ = 'esiintyja'
    # @hybrid_property
    # def _id(self):
    #     """
    #     Eve backward compatibility
    #     """
    #     return self.id

class Ryhma(Base):
    __tablename__ = 'ryhma'
    # @hybrid_property
    # def _id(self):
    #     """
    #     Eve backward compatibility
    #     """
    #     return self.id

class Teos(Base):
    __tablename__ = 'sirkka_esitys_main'
    # @hybrid_property
    # def _id(self):
    #     """
    #     Eve backward compatibility
    #     """
    #     return self.id

class Esitys(Base):
    __tablename__ = 'sirkka_esitys_esityspvm'

    #@hybrid_property
    # def _id(self):
    #     """
    #     Eve backward compatibility
    #     """
    #     return self.id   

class Venue(Base):
    __tablename__ = 'sirkka_esitys_venue'
    # @hybrid_property
    # def _id(self):
    #     """
    #     Eve backward compatibility
    #     """
    #     return self.id 

class Esiintyjat(Base):
    __tablename__ = 'sirkka_esitys_esiintyjat'
    @hybrid_property
    def _id(self):
        """
        Eve backward compatibility
        """
        return self.id 

# class Roolit(Base):
#     __tablename__ = 'sirkka_esitys_agp'
#     @hybrid_property
#     def _id(self):
#         """
#         Eve backward compatibility
#         """
#         return self.id 

class Some(Base):
    __tablename__ = 'sirkka_esitys_sosmed'
    @hybrid_property
    def _id(self):
        """
        Eve backward compatibility
        """
        return self.id 

class WWW(Base):
    __tablename__ = 'sirkka_esitys_verkko'
    @hybrid_property
    def _id(self):
        """
        Eve backward compatibility
        """
        return self.id 

class Lajit(Base):
    __tablename__ = 'sirkuslajit'
    # @hybrid_property
    # def _id(self):
    #     """
    #     Eve backward compatibility
    #     """
    #     return self.id 

Table('sirkka_esitys_main', metadata, 
                Column('_id', Integer, primary_key=True),
                extend_existing=True,
            )

Table('sirkka_esitys_venue', metadata, 
                Column('_id', Integer, primary_key=True, unique=False),
                extend_existing=True,
            )

Table('sirkka_esitys_esityspvm', metadata, 
                Column('_id', Integer, primary_key=True, unique=False),
                # Column('main_id',Integer, ForeignKey('sirkka_esitys_main._id')),
                # Column('esid',Integer, ForeignKey('sirkka_esitys_esityspv._id')),
                extend_existing=True,
            )

Table('sirkka_esitys_esiintyjat', metadata, 
                Column('id', Integer, primary_key=True),
                extend_existing=True,
            )

# Table('sirkka_esitys_agp', metadata, 
#                 Column('id', Integer, primary_key=True),
#                 extend_existing=True,
#             )

Table('sirkka_esitys_sosmed', metadata, 
                Column('id', Integer, primary_key=True),
                extend_existing=True,
            )

Table('sirkka_esitys_verkko', metadata, 
                Column('id', Integer, primary_key=True),
                extend_existing=True,
            )

Table('sirkuslajit', metadata, 
                Column('_id', Integer, primary_key=True),
                extend_existing=True,
            )

Base.prepare(SIRKKA, reflect=True)

# User = Base.classes.users
# Esiintyja = Base.classes.esiintyja
# Ryhma = Base.classes.ryhma
# Esitys = Base.classes.sirkka_esitys_main
# Esitys_esiintyjat = Base.classes.sirkka_esitys_esiintyjat
# Esitys_esitykset = Base.classes.sirkka_esitys_esityspv
# Esitys_venue = Base.classes.sirkka_esitys_esityspvm
# Esitys_roolit = Base.classes.sirkka_esitys_agp
# Esitys_some = Base.classes.sirkka_esitys_sosmed
# Esitys_www = Base.classes.sirkka_esitys_verkko

registerSchema('users')(User)
registerSchema('toimijat')(Toimijat)
registerSchema('ryhmat')(Ryhma)
registerSchema('teos')(Teos)
registerSchema('esitys')(Esitys)
registerSchema('esiintyjat')(Esiintyjat)
registerSchema('esitys_venue')(Venue)
# registerSchema('esitys_roolit')(Roolit)
registerSchema('esitys_some')(Some)
registerSchema('esitys_www')(WWW)
registerSchema('sirkuslajit')(Lajit)

# Heroku support: bind to PORT if defined, otherwise default to 5000.
if 'PORT' in os.environ:
    port = int(os.environ.get('PORT'))
    # use '0.0.0.0' to ensure your REST API is reachable from all your
    # network (and not only your computer).
    host = '0.0.0.0'
else:
    port = 5000
    host = '0.0.0.0'

SETTINGS = {
    'DEBUG': True,
    'SQLALCHEMY_DATABASE_URI': "mysql+pymysql://sirkusinfo:Cr2xvm8Pzs4KPDjA@localhost/sdb",
    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
    'DOMAIN': {
        'users': User._eve_schema['users'],
        'toimijat': Toimijat._eve_schema['toimijat'],
        'ryhma': Ryhma._eve_schema['ryhmat'],
        'teos': Teos._eve_schema['teos'],
        'esitys': Esitys._eve_schema['esitys'],
        'esiintyjat': Esiintyjat._eve_schema['esiintyjat'],
        # 'roolit': Roolit._eve_schema['esitys_roolit'],
        'venue': Venue._eve_schema['esitys_venue'],
        'esitys_some': Some._eve_schema['esitys_some'],
        'esitys_www': WWW._eve_schema['esitys_www'],
        'sirkuslajit': Lajit._eve_schema['sirkuslajit'],
        },
    'RESOURCE_METHODS': ['GET', 'POST'],
    'ITEM_METHODS': ['GET', 'PATCH', 'PUT','DELETE'],
    #'ID_FIELD':'_id',
    #'ITEM_LOOKUP_FIELD': '_id',
    'EXTRA_RESPONSE_FIELDS': [],
    'X_DOMAINS' : '*',
    'X_HEADERS' : ['Content-Type','Access-Control-Allow-Origin','Access-Control-Allow-Methods'],
    'XML': False,
    'IF_MATCH' : False,
    'TRANSPARENT_SCHEMA_RULES':True,
    # 'ELASTICSEARCH_INDEX':'eve-elastic',
    'PAGINATION ': False,
    'PAGINATION_DEFAULT':999,
    'PAGINATION_LIMIT':9999  
}    

SETTINGS['DOMAIN']['esitys']['schema'].update({
           u'_id': {'required': False,
                   'type': 'integer',
                   'unique': False},
           },
    )


SETTINGS['DOMAIN']['esitys']['schema'].update({
              'main_id': {
                 'type': 'integer',
                 'data_relation': {
                     'resource': 'teos',
                     'field': '_id',
                     'embeddable': True,
                 },
              },
           },
    )

SETTINGS['DOMAIN']['esitys']['schema'].update({
              'venue_id': {
                 'type': 'integer',
                 'data_relation': {
                     'resource': 'venue',
                     'field': '_id',
                     'embeddable': True,
                 },
            },
        },
    )


SETTINGS['DOMAIN']['esitys']['schema'].update({
              'group_id': {
                 'type': 'integer',
                 'data_relation': {
                     'resource': 'ryhma',
                     'field': '_id',
                     'embeddable': True
                 },
              },
           },
    )

SETTINGS['DOMAIN']['users']['schema'].update({

# username and email should be unique!

                #u'username': {
                #  'type': 'string',
                   # 'unique': True,
                 #},
                 #u'email': {
                 #'type':'string',
                  # 'unique': True,
                 #},
                 u'group_id': {
                   'type': 'integer',
                   'data_relation': {
                       'resource': 'ryhma',
                       'field': '_id',
                       'embeddable': True
                   },
                },
           },
    )

#Elastics

# SETTINGS['DOMAIN']['esitys']['schema'].update({
#     'datasource': {'backend': 'elastic'}
#         }
#     )

# SETTINGS['DOMAIN']['teos']['schema'].update({
#     'datasource': {'backend': 'elastic'}
#         }
#     )

SETTINGS['DOMAIN']['teos']['schema'].update({
           u'_id': {'required': False,
                   'type': 'integer',
                   'unique': False},
           # u'kuva': {'nullable': True,
           #           'type': 'media'}
         }
    )

SETTINGS['DOMAIN']['venue']['schema'].update({
           u'_id': {'required': False,
                   'type': 'integer',
                   'unique': False},
           },
    )

SETTINGS['DOMAIN']['esiintyjat']['schema'].update({
              'esid': {
                 'type': 'objectid',
                 'data_relation': {
                     'resource': 'teos',
                     'field': '_id',
                     'embeddable': True
                 },
              },
           },
    )

SETTINGS['DOMAIN']['esiintyjat']['schema'].update({
           u'id': {'required': False,
                   'type': 'integer',
                   'unique': False},
           },
    )

# SETTINGS['DOMAIN']['roolit']['schema'].update({
#               'esid': {
#                  'type': 'objectid',
#                  'data_relation': {
#                      'resource': 'teos',
#                      'field': '_id',
#                      'embeddable': True
#                  },
#               },
#            },
#     )

# SETTINGS['DOMAIN']['roolit']['schema'].update({
#            u'id': {'required': False,
#                    'type': 'integer',
#                    'unique': False},
#            },
#     )

# SETTINGS['DOMAIN']['venue']['schema'].update({
#               'main_id': {
#                  'type': 'objectid',
#                  'data_relation': {
#                      'resource': 'teos',
#                      'field': 'id',
#                      'embeddable': True
#                  },
#               },
#            },
#     )

#pprint(Lajit._eve_schema)

# class Eve(Eve):
#     def validate_schema(self, resource, schema):
#         pass

# app = EveSQL(auth=None, validator=ValidatorSQL, data=SQL, settings=SETTINGS)

app = Eve(data=SQL, settings=SETTINGS)

def on_insert_media(res, items):
  
    print 'media insert'

    for img in items[0]:
        
        # pprint(img)
    
        if img in ['kuva','kuva1','kuva2','kuva3','kuva4','kuva5','pdf'] and items[0][img] != None:

            data = items[0][img].split(',')[-1]

            ext = guess_extension(guess_type(items[0][img])[0])

            if ext == '.jpe':
                ext = '.jpg'

            filename = "{}{}".format(uuid.uuid4(),ext)

            path = '/var/www/sirkusinfo/koodit/uploads/'+filename
            
            try:
                _file = open(path, 'wb') 
                _file.write(data.decode('base64'))
                _file.close()
            except:
                print 'SAVE ERROR'
                pass

            if img != 'pdf':
                sizes = {
                        'thumb':['210x',210,0],
                        'kuva':['650x',650,414],
                        'show':['640x',640,320],
                        'artist':['1200x',1200,630]
                        }

                with Image(filename=path) as full:
                    for key in sizes:
                        with full.clone() as i:
                            i.transform(resize=sizes[key][0])
                            if key != 'thumb' and i.size[1] > sizes[key][2]:
                                i.crop(0,0,width=sizes[key][1],height=sizes[key][2])
                            outfile = "%s_%s.%s"%(path.split('.')[0],key,path.split('.')[1])
                            i.save(filename=outfile)    

            items[0][img] = path           

    return items



def on_update_media(res, items, original):

    print 'media update'

    for img in items:
        
        pprint(img)
    
        if img in ['kuva','kuva1','kuva2','kuva3','kuva4','kuva5','pdf'] and items[img] != None:

            data = items[img].split(',')[-1]

            ext = guess_extension(guess_type(items[img])[0])

            if ext == '.jpe':
                ext = '.jpg'
                
            filename = "{}{}".format(uuid.uuid4(),ext)

            path = '/var/www/sirkusinfo/koodit/uploads/'+filename
            
            try:
                _file = open(path, 'wb') 
                _file.write(data.decode('base64'))
                _file.close()
            except:
                print 'SAVE ERROR'
                pass

            if img != 'pdf':
                sizes = {
                        'thumb':['210x',210,0],
                        'kuva':['650x',650,414],
                        'show':['640x',640,320],
                        'artist':['1200x',1200,630]
                        }

                with Image(filename=path) as full:
                    for key in sizes:
                        with full.clone() as i:
                            i.transform(resize=sizes[key][0])
                            if key != 'thumb' and i.size[1] > sizes[key][2]:
                                i.crop(0,0,width=sizes[key][1],height=sizes[key][2])
                            outfile = "%s_%s.%s"%(path.split('.')[0],key,path.split('.')[1])
                            i.save(filename=outfile)    

            items[img] = path           

    return items


app.on_insert += on_insert_media    
app.on_update += on_update_media
app.on_replace += on_update_media

app.config['MAX_CONTENT_LENGTH'] = 10 * 1024 * 1024

@app.route('/mail/<email>')
def password_mail(email):

    try:
        loser = db.session.query(User).filter(User.email == email)
        if(loser[0]):
            key = loser[0].password.split(':')[0]
    except:
        return 'fail'

    password_message = u"Vaihtaaksesi salasanasi klikkaa oheista linkkiä/To change your password please ckick the link: http://sirkusinfo.fi/sirkus-suomessa/sirkka-tietokanta/salasanan-palautus/?sirkka_email=%s&sirkka_key=%s"%(email,key)
    msg = MIMEText(password_message, 'plain', _charset='utf-8')
    msg['Subject'] = 'Uusi salasana Sirkka-tietokantaan/Your password to Sirkka-database' 
    msg['From'] = me = 'info@sirkusinfo.fi'
    msg['To'] = email

    # Send the message via our own SMTP server, but don't include the
    # envelope header.
    s = smtplib.SMTP('localhost')
    s.sendmail(me, [email], msg.as_string())
    s.quit()

    return 'OK'


db = app.data.driver
Base.metadata.bind = db.engine
db.Model = Base
db.create_all()

if __name__ == '__main__':
    app.run(host=host, port=port, debug=True)
