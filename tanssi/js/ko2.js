var baseURL = 'http://sirkusinfo.fi:5000/';
var embed = '?embedded={"main_id":1,"esid":1}';
var last = moment().format('YYYY') - 1;
var year = new Date('Jan 01, '+last);
var pvmyear = ';pvm>'+year.valueOf()/1000;

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
        });
    return vars;
}

var groupid = '&where=group_id=='+getUrlVars()['group'];
console.log(pvmyear+groupid)
var ID = '';
moment.locale('fi');  // Set the default/global locale

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    vars[key] = value;
    });
    return vars;
}



/**** GRID ****/

ko.observableArray.fn.pushAll = function(valuesToPush) {
    var underlyingArray = this();
    this.valueWillMutate();
    ko.utils.arrayPushAll(underlyingArray, valuesToPush);
    this.valueHasMutated();
    return this;
};

var esitysGrid = function() {
    var self = this;

    var data = $.ajax({
        method: 'GET',
        url: baseURL+'esitys/'+embed+groupid+pvmyear,
        async: false,
        dataType: "json",
        contentType: "multipart/form-data; charset='utf-8'",
        data: data,//JSON.stringify(data),
        success: function (data) {
            console.log(data)
            return JSON.stringify(data);
        },
        error: function (xhr, type, exception) {
            // Do your thing
        }
    });

    //getAPI(baseURL+embed,'GET');
    
    items = ko.observableArray([]);

    var Type = function(name) {
        this.typeName = name;
    };

    availableType = ko.observableArray([
        new Type("Kotimaan esitys"),
        new Type("Kv esitys"),
        new Type("Kotimainen vieras"),
        new Type("Ulkom. vieras"),
        new Type("Sosiaalinen sirkus"),
        new Type("Yleisötyö"),
        ])
        
    self.esitys = ko.observableArray([
        ]);

    self.esitys.pushAll(data.responseJSON._items)

    //console.log(self.esitys)

    //ko.mapping.fromJS(data, {}, self.esitys);

    self.tallenna = ko.observableArray([])

    self.saveRow = function(row) {
        console.log(row)

        self.tallenna.remove( function (item) { 
            return item.id == row.id;
            } )
        self.tallenna.push(row);
        return true;
    }

    self.saveDateRow = function() {
        self.tallenna.remove( function (item) { 
            console.log(item.pvm)
            return item.id == this.id;
            } )
        self.tallenna.push(this)

    }

    self.addEsitys = function() {

        self.esitys.push({
            pvm: moment().format('DD. MM. Y'),
            kpl: 1,
            myyty:0,
            yleiso:0,
            tyyppi: "Kotimaan esitys",
            vieras:"",
            workshop: 0,
            yhteis: 0,
            ulko: 0,
            nayttamo: 0,
            muu: 0,
            main_id: {
                nimi:"",
                esittaja:""
            },
            esid: { 
                paikka:"",
                kaupunki:"",
                maa:"Suomi"
            }
        });
    };
 
    self.removeEsitys = function(data) {
        self.esitys.remove(data);
    };
 
    self.save = function(form) {

        $.each(self.tallenna(), function(i, val) {

            if(val.tyyppi == undefined) {
                tyyppi = "Kotimaan esitys"
            } else {
                tyyppi = val.tyyppi
            }

            if (!("id" in val) ) {
                
                //create new items

            _esitys = {
                pvm: val.pvm,
                kpl: parseInt(val.kpl),
                myyty: parseInt(val.myyty),
                yleiso: parseInt(val.yleiso),
                tyyppi: tyyppi,
                vieras: val.vieras,
                workshop: val.workshop,
                yhteis: val.yhteis,
                ulko:val.ulko,
                nayttamo:val.nayttamo,
                muu: val.muu

            }

            var data = $.ajax({
                method: 'PUT',
                url: baseURL + 'esitys/',
                //async: false,
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify(_esitys),
                success: function (data) {
                    //return "" JSON.stringify(data);
                    },
                error: function (xhr, type, exception) {
                    console.log(xhr)

                    }
                });

            _teos = {
                id: val.main_id.id,
                nimi: val.main_id.nimi,
                esittaja: val.main_id.esittaja,

            }

            var data = $.ajax({
                method: 'PUT',
                url: baseURL + 'teos/',
                //async: false,
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify(_teos),
                success: function (data) {
                    //return "" JSON.stringify(data);
                    },
                error: function (xhr, type, exception) {
                        // Do your thing
                        console.log(xhr)

                    }
                });

            _venue = {
                id: val.esid.id,
                paikka: val.esid.paikka,
                kaupunki: val.esid.kaupunki,
                maa: val.esid.maa
            }

            var data = $.ajax({
                method: 'PUT',
                url: baseURL + 'venue/',
                //async: false,
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify(_venue),
                success: function (data) {
                    //return "" JSON.stringify(data);
                    },
                error: function (xhr, type, exception) {
                        console.log(xhr)
                        // Do your thing
                    }
                });            


            } else {
                //edit existing

            _esitys = {
                id: val.id,
                pvm: val.pvm,
                kpl: parseInt(val.kpl),
                myyty: parseInt(val.myyty),
                yleiso: parseInt(val.yleiso),
                tyyppi: tyyppi,
                vieras: val.vieras,
                workshop: val.workshop,
                yhteis: val.yhteis,
                ulko:val.ulko,
                nayttamo:val.nayttamo,
                muu: val.muu
            }

            console.log(JSON.stringify(_esitys))

                var esitys = $.ajax({
                    method: 'PATCH',
                    url: baseURL + 'esitys/'+_esitys.id,
                    //async: false,
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(_esitys),
                    success: function (data) {
                        },
                    error: function (xhr, type, exception) {
                        console.log(xhr)

                        }
                    });
             
            _teos = {
                id: val.main_id.id,
                nimi: val.main_id.nimi,
                esittaja: val.main_id.esittaja,

            }

            console.log(JSON.stringify(_teos))

                var data = $.ajax({
                    method: 'PATCH',
                    url: baseURL + 'teos/'+_teos.id,
                    //async: false,
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(_teos),
                    success: function (data) {
                        },
                    error: function (xhr, type, exception) {
                            console.log(xhr)
                        }
                    });
            

            _venue = {
                id: val.esid.id,
                paikka: val.esid.paikka,
                kaupunki: val.esid.kaupunki,
                maa: val.esid.maa
            }

             console.log(JSON.stringify(_venue))

                var data = $.ajax({
                    method: 'PATCH',
                    url: baseURL + 'venue/'+_venue.id,
                    //async: false,
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(_venue),
                    success: function (data) {
                            },
                    error: function (xhr, type, exception) {
                            console.log(xhr)
                        }
                    });            
                
            }
        });
    };
};

viewModel = new esitysGrid();

ko.applyBindings(viewModel, $('esityksetGrid')[0]);
 
// Activate jQuery Validation
//$("form#esityksetGrid").validate({ submitHandler: viewModel.save });