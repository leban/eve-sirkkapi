
var ryhmatGrid = function() {
    ko.virtualElements.allowedBindings.group_id = true;

    var self = this;
    self.uusi = ko.observableArray([]);

    self.uusi.push({
            nimi:'',
            koreografi:'',
            sensiilta:'',
            tuottaja:'',
            paikka:'',
            kunta:'',
            julkaistu:0,
            user_id:ID,
            group_id:ID,
            sirkka:0,
            esitys:0,
        })
    

    //ko.mapping.fromJS(data, {}, self.esitys);


    self.saveTeos = function () {

            console.log(ko.toJSON(self.uusi))

            // _ryhma = {
            //     nimi: val.nimi,
            //     oranisaatiomuoto: val.group_id.oranisaatiomuoto,
            //     yhteyshenkilo:val.group_id.yhteyshenkilo,
            //     muumika: val.group_id.muumika,
            // }

            var data = $.ajax({
                method: 'POST',
                url: baseURL + 'teos/',
                async: false,
                dataType: "json",
                contentType: "application/json",
                data: ko.toJSON(self.uusi),
                success: function (data) {
                    console.log(data);
                    self.uusi = ko.observableArray([]);         
                    location.reload();
                    },
                error: function (xhr, type, exception) {
                        // Do your thing
                        console.log(xhr)

                    }
                })
    }


}

viewModel = new ryhmatGrid();

ko.applyBindings(viewModel, $('#uusi')[0]);
 