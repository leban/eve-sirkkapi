var baseURL = 'http://api.sirkusinfo.fi:5001/';
var embed = '?embedded={"main_id":1,"esid":1,"group_id":1}';
var last = moment().format('YYYY') - 1;
var current = moment().format('YYYY');
var year = new Date('Jan 01, '+last);
var pvmyear = ';pvm>'+year.valueOf()/1000;

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
        });
    return vars;
}

var admin_ID = 0
var groupid = '&where=group_id=='+getUrlVars()['group'];
var groupname = getUrlVars()['nimi']

if(Cookies.get('UID')) {
  UID = Cookies.get('UID')
  console.log(UID)
} else {
  UID = 0
}

if(getUrlVars()['ID']) {
    var ID = parseInt(getUrlVars()['ID']);
} 

if(getUrlVars()['Y']) {
    var year = parseInt(getUrlVars()['Y']);
} 

moment.locale('fi');  // Set the default/global locale

var Api = function(method, path, data, cbk) {

        if(data) {
            var Content = ko.toJSON(data);
            var Type = "application/json; charset='utf-8'";
            console.log(Content)
        } else {
            var Content = null;
            Type = "multipart/form-data; charset='utf-8'"
        }

        $.ajax({
        method: method,
        url: baseURL+path,
        //async: false,
        dataType: "json",
        contentType: Type,
        data: Content,
        cache: false,
        //data: data,//JSON.stringify(data),
        success: function (data) {
            console.log(data)
            if(method == 'DELETE') {
                cbk()
            }
            else if(data._items) {
                cbk(data._items);
            } else {
                cbk(data);
            }
        },
        error: function (xhr, type, exception) {
            console.log(xhr.responseText)
        }
    });}



ko.observableArray.fn.pushAll = function(valuesToPush) {
    var underlyingArray = this();
    this.valueWillMutate();
    ko.utils.arrayPushAll(underlyingArray, valuesToPush);
    this.valueHasMutated();
    return this;
};

    ko.validation.init();

    ko.validation.rules['areSame'] = {

    getValue: function (o) {
        return (typeof o === 'function' ? o() : o);
    },
    validator: function (val, otherField) {
        return val === this.getValue(otherField);
        console.log(val)
    },
    message: 'Salasanat eivät täsmää'
    };

    ko.validation.rules['passwordComplexity'] = {
    validator: function (val) {
        return /(?=^[^\s]{6,128}$)((?=.*?\d)(?=.*?[A-Z])(?=.*?[a-z])|(?=.*?\d)(?=.*?[^\w\d\s])(?=.*?[a-z])|(?=.*?[^\w\d\s])(?=.*?[A-Z])(?=.*?[a-z])|(?=.*?\d)(?=.*?[A-Z])(?=.*?[^\w\d\s]))^.*/.test('' + val + '');
    },
    message: 'Salasanassa on oltava 6-128 merkkiä, ja kolme seuraavista:Iso kirjain, pieni kirjain, numero tai erikoismerkki.'
    };
//Password must be between 6 and 128 characters long and contain three of the following 4 items: upper case letter, lower case letter, a symbol, a number
    ko.validation.registerExtenders();

/*** Typeaheads ****/

    var _val = new Object();

function extractor(query) {
    var result = /([^,]+)$/.exec(query);
    if(result && result[1])
        return result[1].trim();
    return '';
}
    // var auto = function(path) {
    //   console.log(_val[path+'_val'])
    //   $(document).on('focusin','.'+path, function(){
    //     $('.'+path).autocomplete({
    //       source: _val[path+'_val']
    //     })
    //   })
    // }


    // var typeah = function(path) {
      
    //   $(document).on('focusin','.'+path, function(){
    //     $('.'+path).typeahead( { 
    //         hint: true,
    //         highlight: true,
    //         minLength: 3
    //       },
    //         {
    //           name: path+'_names',
    //           source: _val[path+'_val'],
    //         }
    //      )    
    //   } )
    // }



    var auto = function(path) {

          $(document).on('focusin','.'+path,
          function() {
            console.log(path)
              $(this).typeahead( { 
              source:_val[path+'_val'],
              updater: function(item) {
                if(path == 'venue') {
                    $('.'+path+'_id').val(_val[path+'_id'][item]).change();
                    $('.'+path+'_kaupunki').val(_val[path+'_kaupunki'][item]).change();
                    $('.'+path+'_maa').val(_val[path+'_maa'][item]).change();
                }
                return this.$element.val().replace(/[^,]*$/,'')+item+' ';
              },
              matcher: function (item) {
                var tquery = extractor(this.query);
                if(!tquery) return false;
                return ~item.toLowerCase().indexOf(tquery.toLowerCase());
              },
              highlighter: function (item) {
                var query = extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
                return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
                  return '<strong>' + match + '</strong>'
                  })
                }
               }                                
             );
          })
        }

var Lookup = function(path) {

  //sirkuslajit
  // toimijat
  // ryhma
  // venue

    _val[path+'_val'] = new Array()
    _val[path+'_id'] = new Object()
    _val[path+'_kaupunki'] = new Object()
    _val[path+'_maa'] = new Object()

    if(path == 'sirkuslajit'|| path=='venue') {
      julkaistu = ''
    } else if(path == 'teos') {
      julkaistu='?where={"julkaistu":1, group_id":'+UID+'}'
    } else {
      julkaistu=''
    }

    if(_val[path+'_val'].length > 0) {
    
          auto(path);

      } else {
        
        ko.computed(function() {

         return Api('GET',path+julkaistu, null, function(_data) {
                      $.each(_data, function ( index, item ) {
                if(item.sukunimi) {
                    _val[path+'_val'].push( item.sukunimi+', '+item.etunimi );
                    _val[path+'_id'][item.sukunimi] = item._id;

                } else if(item.nimi) {
                    _val[path+'_val'].push( item.nimi );
                    _val[path+'_id'][item.nimi] = item._id;
                } else if(item.paikka) {
                  if(_val[path+'_val'].indexOf(item.paikka) < 0){
                      _val[path+'_val'].push( item.paikka );
                      _val[path+'_id'][item.paikka] = item._id;
                      _val[path+'_kaupunki'][item.paikka] = item.kaupunki;
                      _val[path+'_maa'][item.paikka] = item.maa;
                      }
                  } else {
                    _val[path+'_val'].push( item.fi );
                    _val[path+'_id'][item.fi] = item._id;

                  }
            });
                  console.log(_val[path+'_val'])
              auto(path);
            });

        }, self)

      } 
    }


var country = [
      'Afganistan',
      'Algeria',
      'Andorra',
      'Antarktis',
      'Argentiina',
      'Armenia',
      'Azerbaidžan',
      'Bahama',
      'Bangladesh',
      'Barbados',
      'Belgia',
      'Benin',
      'Bhutan',
      'Bolivia',
      'Bosnia ja Hertsegovina',
      'Botswana',
      'Brasilia',
      'Brunei',
      'Bulgaria',
      'Burkina',
      'Burundi',
      'Chile',
      'Dominica',
      'Dominikaaninen tasavalta',
      'Ecuador',
      'Eesti ( Viro)',
      'Egypti',
      'Espanja',
      'Etelä-Korea',
      'Etelä-Sudan',
      'Etiopia',
      'Fidži',
      'Filippiinit',
      'Gabon',
      'Gambia',
      'Georgia',
      'Ghana',
      'Gibraltar (GI1)',
      'Grenada',
      'Guatemala',
      'Guinea',
      'Guinea-Bissau',
      'Guyana',
      'Honduras',
      'Hongkong (HK1)',
      'Indonesia',
      'Intia',
      'Irak',
      'Iran',
      'Irlanti',
      'Islanti',
      'Itä-Timor',
      'Jamaika',
      'Japani',
      'Jemen',
      'Jordania',
      'Kambodža',
      'Kanada',
      'Kap Verde',
      'Kazakstan',
      'Kenia',
      'Keski-Afrikan tasavalta',
      'Kiina',
      'Kirgisia',
      'Kiribati',
      'Kolumbia',
      'Komorit',
      'Kongo',
      'Kongon demokraattinen tasavalta',
      'Kreikka',
      'Kroatia',
      'Kuuba',
      'Kuwait',
      'Kypros',
      'Latvia',
      'Lesotho',
      'Libanon',
      'Liberia',
      'Libya',
      'Liechtenstein',
      'Liettua',
      'Luxemburg',
      'Länsi-Sahara (EH1)',
      'Macao (MO1)',
      'Madagaskar',
      'Makedonia ( vir. entinen Jugoslavian tasavalta Makedonia)',
      'Malawi',
      'Malediivit',
      'Mali',
      'Malta',
      'Marokko',
      'Marshallinsaaret',
      'Mauritania',
      'Mauritius',
      'Meksiko',
      'Mikronesia',
      'Moldova',
      'Monaco',
      'Mongolia',
      'Montenegro',
      'Mosambik',
      'Myanmar/Burma',
      'Namibia',
      'Nauru',
      'Nepal',
      'Nicaragua',
      'Niger',
      'Nigeria',
      'Norja',
      'Norsunluurannikko',
      'Oman',
      'Pakistan',
      'Panama',
      'Paraguay',
      'Peru',
      'Pohjois-Korea',
      'Portugali',
      'Puerto Rico (PR1)',
      'Puola',
      'Päiväntasaajan Guinea',
      'Qatar',
      'Ranska',
      'Ranskan Guayana (GF1)',
      'Ranskan Polynesia (PF1)',
      'Réunion (RE1)',
      'Romania',
      'Ruanda',
      'Ruotsi',
      'Saksa',
      'Salomonsaaret',
      'Sambia',
      'Samoa',
      'San Marino',
      'São Tomé ja Príncipe',
      'Saudi-Arabia',
      'Senegal',
      'Serbia',
      'Seychellit',
      'Sierra Leone',
      'Singapore',
      'Slovakia',
      'Slovenia',
      'Somalia',
      'Sri Lanka',
      'Sudan',
      'Suomi',
      'Suriname',
      'Sveitsi',
      'Swazimaa',
      'Syyria',
      'Tadžikistan',
      'Taiwan',
      'Tansania',
      'Tanska',
      'Thaimaa',
      'Timor ( Itä-Timor)',
      'Togo',
      'Tonga',
      'Trinidad ja Tobago',
      'Tšad',
      'Tšekki',
      'Tunisia',
      'Turkki',
      'Turkmenistan',
      'Tuvalu',
      'Uganda',
      'Ukraina',
      'Unkari',
      'Uruguay',
      'Uusi-Seelanti',
      'Uzbekistan',
      'Valko-Venäjä',
      'Vanuatu',
      'Vatikaanivaltio ( Pyhä istuin)',
      'Venezuela',
      'Venäjä',
      'Vietnam',
      'Viro',
      'Yhdistynyt kuningaskunta',
      'Yhdysvallat (USA)',
      'Zimbabwe'
];

$(document).on('focusin','input.country',
                function() {
                    $('input.country').typeahead( { 
                    source:country,
                    updater: function(item) {
                        return this.$element.val().replace(/[^,]*$/,'')+item;
                    },
                    matcher: function (item) {
                      var tquery = extractor(this.query);
                      if(!tquery) return false;
                      return ~item.toLowerCase().indexOf(tquery.toLowerCase())
                    },
                    highlighter: function (item) {
                      var query = extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
                      return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
                        return '<strong>' + match + '</strong>'
                        })
                      }
                     }                                
                   );
                })



$(document).on('click','#logout', function() {
    Cookies.remove('UID')
    window.location.href = 'index.html'
})    

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
          container: 'body',
          placement: 'right'
    }); 

});









