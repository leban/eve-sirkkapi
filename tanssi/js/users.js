

var Users = function() {
    ko.virtualElements.allowedBindings.group_id = true;

    var self = this;

    self.users = ko.observableArray([
        ]);


    var data = $.ajax({
        method: 'GET',
        url: baseURL+'users/',
        //async: false,
        dataType: "json",
        contentType: "multipart/form-data; charset='utf-8'",
        data: data,//JSON.stringify(data),
        success: function (data) {
            console.log(data._items)
            self.users.push(data._items)
        },
        error: function (xhr, type, exception) {
            // Do your thing
        }
    });

    var groups = new Array();
    var gid = new Object();

    var data2 = $.ajax({
        method: 'GET',
        url: baseURL+'users/',
        //async: false,
        dataType: "json",
        contentType: "multipart/form-data; charset='utf-8'",
        data: data2,//JSON.stringify(data),
        success: function (data2) {
            $.each( data2._items, function ( index, item ) {
            groups.push( item.nimi );
            gid[item.nimi] = item._id;
        } );
        
                $(document).on('focusin','input.ryhma',
                function() {
                    $('input.ryhma').typeahead( { 
                    source:groups,
                    updater: function(item) {
                        return this.$element.val().replace(/[^,]*$/,'');
                    },
                    matcher: function (item) {
                      var tquery = extractor(this.query);
                      if(!tquery) return false;
                      return ~item.toLowerCase().indexOf(tquery.toLowerCase())
                    },
                    highlighter: function (item) {
                      var query = extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
                      return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
                        return '<strong>' + match + '</strong>'
                        })
                      }
                     }                                
                   );
                })

        },
        error: function (xhr, type, exception) {
            // Do your thing
        }
    });

    //getAPI(baseURL+embed,'GET');
    

    // self.ryhmat.pushAll(data.responseJSON._items)


    //ko.mapping.fromJS(data, {}, self.esitys);


   self.addTaho = function() {

        self.ryhmat.push({
            nimi:'',
            oranisaatiomuoto:'Tanssiryhmä',
            yhteyshenkilo:'',
            muumika:'',
            email:'',
            login:'',
            password:''
        })
    
    console.log(self.ryhmat()) 
   }

    self.saveTaho = function () {

            console.log(ko.toJSON(self.ryhmat))

            // _ryhma = {
            //     nimi: val.nimi,
            //     oranisaatiomuoto: val.group_id.oranisaatiomuoto,
            //     yhteyshenkilo:val.group_id.yhteyshenkilo,
            //     muumika: val.group_id.muumika,
            // }

        $.each(self.ryhmat(), function(i, val) {

            if (!("_id" in val) ) {

            var data = $.ajax({
                method: 'POST',
                url: baseURL + 'ryhma/',
                async: false,
                dataType: "json",
                contentType: "application/json",
                data: ko.toJSON(val),
                success: function (data) {
                    console.log(data)
                    },
                error: function (xhr, type, exception) {
                        // Do your thing
                        console.log(xhr)

                    }
                })
            }
        });
    }


}

viewModel = new Users();

ko.applyBindings(viewModel);
 