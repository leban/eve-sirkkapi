


var ryhma = function() {

    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghijklmnoprstuvwz";
    var string_length = 32;
    var salt = "";

    for(var i=0; i<string_length; i++){
      var randomPos = Math.floor( Math.random() * chars.length );
      salt += chars.substr(randomPos, 1);
    }

    var Type = function(name) {
        this.typeName = name;
    };

    availableType = ko.observableArray([
        new Type("Aluekeskus"),
        new Type("Tanssiryhmä (Valtion toiminta-avustus"),
        new Type("Tanssiryhmä (Ei valtion toiminta-avustusta"),
        new Type("Yksittäinen koreografi"),
        new Type("Festivaali"),
        ])


    var self = this;

    //getAPI(baseURL+embed,'GET'); 


    var group_ID = getUrlVars()['group']; 

    console.log(group_ID)

    if(group_ID != null) {

        var data = $.ajax({
                method: 'GET',
                url: baseURL + 'ryhma/'+group_ID,
                async: false,
                dataType: "json",
                contentType: "application/json",
                data: data,
                success: function (data) {
                    //console.log(data);
                    self.nimi = ko.observable(data.nimi).extend({ required: true })
                    self.organisaatiomuoto = ko.observable(data.organisaatiomuoto)
                    self.yhteyshenkilo = ko.observable(data.yhteyshenkilo)
                    self.muumika = ko.observable(data.muumika)
                    self.muitatietoja =ko.observable(data.muitatietoja)
                    self.email  = ko.observable(data.email).extend({email:true})
                    self.login = ko.observable(data.login)
                    self.password  = ko.observable('')
                    self.retype = ko.observable('').extend( {areSame:self.password }).extend({passwordComplexity:true});
                    self.julkaistu = ko.observable(data.julkaistu)
                    self.userid = ko.observable(data.userid)

                    },
                error: function (xhr, type, exception) {
                        // Do your thing
                        console.log(xhr)

                    }
                })

    } else {

            self.nimi = ko.observable('').extend({ required: true })
            self.organisaatiomuoto = ko.observable('Tanssiryhmä')
            self.yhteyshenkilo = ko.observable('')
            self.muumika = ko.observable('')
            self.muitatietoja =ko.observable('Suomi')
            self.email  = ko.observable('').extend({email:true})
            self.login = ko.observable('')
            self.password  = ko.observable('')
            self.retype = ko.observable('').extend({ areSame:self.password }).extend({passwordComplexity:true});
            self.julkaistu = ko.observable(0)
            self.userid = ko.observable(0)
    }

    // self.retype.extend( areSame: { params: self.password, 
    //     message: "Salasanat eivät täsmää" })

    //ko.mapping.fromJS(data, {}, self.esitys);


    self.saveUusi = function () {


            // _ryhma = {
            //     nimi: val.nimi,
            //     oranisaatiomuoto: val.group_id.oranisaatiomuoto,
            //     yhteyshenkilo:val.group_id.yhteyshenkilo,
            //     muumika: val.group_id.muumika,
            // }

            // ota salasanat
            // jos on samat ->
            // hashaa md5+suola
            // korvaa password

            var md5pass = $.md5(self.password()+salt)

            self.uusi = {
                    nimi: self.nimi,
                    organisaatiomuoto: self.organisaatiomuoto,
                    yhteyshenkilo: self.yhteyshenkilo,
                    muumika: self.muumika,
                    muitatietoja: self.muitatietoja,
                    email: self.email,
                    login: self.login,
                    password: md5pass+':'+salt,
                    julkaistu: self.julkaistu,
                    userid: self.userid
            }


        if(group_ID != null) {

            var data = $.ajax({
                method: 'PATCH',
                url: baseURL + 'ryhma/'+group_ID,
                async: false,
                dataType: "json",
                contentType: "application/json",
                data: ko.toJSON(self.uusi),
                success: function (data) {
                    console.log(data);
                    location.reload();
                    },
                error: function (xhr, type, exception) {
                        // Do your thing
                        console.log(xhr)

                    }
                })

        } else {

            var data = $.ajax({
                method: 'POST',
                url: baseURL + 'ryhma/',
                async: false,
                dataType: "json",
                contentType: "application/json",
                data: ko.toJSON(self.uusi),
                success: function (data) {
                    console.log(data);
                    location.reload();
                    },
                error: function (xhr, type, exception) {
                        // Do your thing
                        console.log(xhr)

                    }
                })
            
        }
    }



}

viewModel = new ryhma();

ko.applyBindings(viewModel, $('#uusi')[0]);


 