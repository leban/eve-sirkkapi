
if (/esitystilasto/.test(self.location.href)) {
    var type = 'Kotimaan esitys';
} else {
    var type = 'Yleisötyö'
    }

var esitysGrid = function() {

    var self = this;

    // var data = {'responseJSON':{
    //             '_items':[]
    //             }} 

    self.esitys = ko.observableArray([]);

    Api('GET','esitys?where=tyyppi=="'+type+'"', null, function(data){
    
        self.esitys.pushAll(data)

    })

    // var data = $.ajax({
    //     method: 'GET',
    //     url: baseURL+'esitys/'+embed+groupid,
    //     async: false,
    //     dataType: "json",
    //     contentType: "multipart/form-data; charset='utf-8'",
    //     data: data,//JSON.stringify(data),
    //     success: function (data) {
    //         return JSON.stringify(data);
    //     },
    //     error: function (xhr, type, exception) {
    //         // Do your thing
    //     }
    // });

    //getAPI(baseURL+embed,'GET');
    
    self.groupname = ko.observable(groupname)

    var Type = function(name) {
        this.typeName = name;
    };

    availableType = ko.observableArray([
        new Type("Kotimaan esitys"),
        new Type("Kv esitys"),
        new Type("Kotimainen vieras"),
        new Type("Ulkom. vieras"),
        new Type("Yleisötyö"),
        new Type("Yhteisötanssi"),
        new Type("Tanssielokuvanäytäntö"),
        ])



    //ko.mapping.fromJS(data, {}, self.esitys);

    self.tallenna = ko.observableArray([])

    self.saveRow = function(row) {

        self.tallenna.remove( function (item) { 
            return item._id == row._id;
            } )
        self.tallenna.push(row);

        return true;
    }



    self.addEsitys = function() {

        self.save();

        self.esitys.push({
            pvm: moment().format('DD. MM. YYYY'),
            to: null,
            kantaes:0,
            kpl: 0,
            ilmkpl: 0,
            yhtkpl: '',
            myyty:0,
            yleiso:'',
            ilmaiset:0,
            tyyppi: type,
            vieras:"",
            jarj: "",
            tuottaja: "",
            workshop: 0,
            yhteis: 0,
            group_id:ID,
            lahde: groupname,
            main_id: {
                nimi:"",
                esittaja:"",
                koreografi:""
            },
            esid: { 
                paikka:"",
                kaupunki:"",
                maa:"Suomi"
            },

        });
    };
 
    self.cloneEsitys = function() {

        self.save();

        last = self.esitys().length -1 
        clone = self.esitys()[last]
        delete clone._id
        clone.kpl = 1
        clone.yhtpl = null
        clone.ilmkpl = 0
        clone.myyty = 0
        clone.ilmaiset = 0
        clone.yleisoyht = null

        self.esitys.push(
            clone
            )
        }

    self.removeEsitys = function(data) {
        self.esitys.remove(data);
    };
 
    self.uusi = ko.observableArray([])

   self.addTaho = function() {

        self.uusi.push({
            nimi:'',
            organisaatiomuoto:'Tanssiryhmä',
            yhteyshenkilo:'',
            muumika:'',
            muitatietoja:'Suomi',
            email:'',
            login:'',
            password:'',
            julkaistu:0,
            userid:0,
        })
        
   }

    self.saveUusi = function () {

            console.log(ko.toJSON(self.uusi))

            // _ryhma = {
            //     nimi: val.nimi,
            //     oranisaatiomuoto: val.group_id.oranisaatiomuoto,
            //     yhteyshenkilo:val.group_id.yhteyshenkilo,
            //     muumika: val.group_id.muumika,
            // }

            var data = $.ajax({
                method: 'POST',
                url: baseURL + 'ryhma/',
                async: false,
                dataType: "json",
                contentType: "application/json",
                data: ko.toJSON(self.uusi),
                success: function (data) {
                    console.log(data)
                    $('.alert').toggle();
                    window.setTimeout(function() {
                    location.reload();
                }, 2000)
                    },
                error: function (xhr, type, exception) {
                        // Do your thing
                        console.log(xhr)

                    }
                })
        self.uusi = ko.observableArray([])
        getRyhmat();
    
    }


    self.save = function(form) {
        
        $.each(self.tallenna(), function(i, val) {

            console.log(val)

            if (!("_id" in val) ) {
                
                //create new items
        if(val.tyyppi == undefined) {
            tyyppi = "Kotimaan esitys"
        } else {
            tyyppi = val.tyyppi
        }
        

            _teos = {
                // _id: val.main_id._id,
                nimi: val.main_id.nimi,
                koreografi: val.main_id.koreografi,
                esittaja: val.main_id.esittaja,
                sirkka: false,
                esitys: false,
                julkaistu: false,
                user_id: 0,
                group_id: ID,

            }

            var data = $.ajax({
                method: 'POST',
                url: baseURL + 'teos/',
                async: false,
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify(_teos),
                success: function (data) {
                    console.log(data)
                    main_ref = data._id
                    },
                error: function (xhr, type, exception) {
                        // Do your thing
                        console.log(xhr)

                    }
                });

            _venue = {
                // _id: val.esid._id,
                paikka: val.esid.paikka,
                kaupunki: val.esid.kaupunki,
                maa: val.esid.maa
            }

            var data = $.ajax({
                method: 'POST',
                url: baseURL + 'venue/',
                async: false,
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify(_venue),
                success: function (data) {
                    console.log(data)
                    esid_ref = data._id
                    //return "" JSON.stringify(data);
                    },
                error: function (xhr, type, exception) {
                        console.log(xhr)
                        // Do your thing
                    }
                });            


            _esitys = {
                pvm: val.pvm,//moment.unix(val.pvm).format("DD. MM. YYYY"),
                to: val.to,//moment.unix(val.to).format("DD. MM. YYYY"),
                kpl: parseInt(val.kpl),
                ilmkpl: parseInt(val.ilmkpl),
                yhtkpl: parseInt(val.kpl)+parseInt(val.ilmkpl),
                myyty: parseInt(val.myyty),
                ilmaiset: parseInt(val.ilmaiset),
                yleiso: parseInt(val.myyty)+parseInt(val.ilmaiset),
                tyyppi: tyyppi,
                tuottaja: val.tuottaja,
                jarj: val.jarj,
                vieras: val.vieras,
                workshop: val.workshop,
                yhteis: val.yhteis,
                main_id: main_ref,
                esid: esid_ref,
                group_id:ID,
                lahde:groupname

            }

            console.log("esitys:"+JSON.stringify(_esitys))

            var data = $.ajax({
                method: 'POST',
                url: baseURL + 'esitys/',
                //async: false,
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify(_esitys),
                success: function (data) {
                    console.log(data)

                    //return "" JSON.stringify(data);
                    },
                error: function (xhr, type, exception) {
                    console.log(xhr)

                    }
                });

            } else {

                //edit existing

            _esitys = {
                _id: val._id,
                pvm: val.pvm, //moment.unix(val.pvm).format("DD. MM. YYYY"),
                to: val.to, //moment.unix(val.to).format("DD. MM. YYYY"),
                kpl: parseInt(val.kpl),
                ilmkpl: parseInt(val.ilmkpl),
                yhtkpl: parseInt(val.kpl)+parseInt(val.ilmkpl),
                myyty: parseInt(val.myyty),
                ilmaiset: parseInt(val.ilmaiset),
                yleiso: parseInt(val.myyty)+parseInt(val.ilmaiset),
                tyyppi: tyyppi,
                vieras: val.vieras,
                workshop: val.workshop,
                yhteis: val.yhteis,
                jarj: val.jarj,
                tuottaja: val.tuottaja,
                vieras: val.vieras,
                group_id:ID,
                lahde:groupname
            }

            console.log('esitys: '+JSON.stringify(_esitys))

                var esitys = $.ajax({
                    method: 'PATCH',
                    url: baseURL + 'esitys/'+_esitys._id,
                    //async: false,
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(_esitys),
                    success: function (data) {
                        },
                    error: function (xhr, type, exception) {
                        console.log(xhr)

                        }
                    });
             
            _teos = {
                _id: val.main_id._id,
                nimi: val.main_id.nimi,
                koreografi: val.main_id.koreografi,
                esittaja: val.main_id.esittaja,

            }

            console.log(JSON.stringify(_teos))

                var data = $.ajax({
                    method: 'PATCH',
                    url: baseURL + 'teos/'+_teos._id,
                    //async: false,
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(_teos),
                    success: function (data) {
                        },
                    error: function (xhr, type, exception) {
                            console.log(xhr)
                        }
                    });
            

            _venue = {
                _id: val.esid._id,
                paikka: val.esid.paikka,
                kaupunki: val.esid.kaupunki,
                maa: val.esid.maa
            }

             console.log(JSON.stringify(_venue))

                var data = $.ajax({
                    method: 'PATCH',
                    url: baseURL + 'venue/'+_venue._id,
                    //async: false,
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(_venue),
                    success: function (data) {

                            },
                    error: function (xhr, type, exception) {
                            console.log(xhr)
                        }
                    });   
                
            }

    
        });
    };
};



viewModel = new esitysGrid();

ko.applyBindings(viewModel, $('#esityksetGrid')[0]);


/**** Extras ****/

function extractor(query) {
    var result = /([^,]+)$/.exec(query);
    if(result && result[1])
        return result[1].trim();
    return '';
}

var groupName = new Array();
var groupIds = new Object();
var grpCountry = new Object();

var getRyhmat = $.ajax({
    method: 'GET',
    url: 'http://api.sirkusinfo.fi:5001/ryhma',
    async: false,
    dataType: "json",
    contentType: "multipart/form-data; charset='utf-8'",
    data: getRyhmat,//JSON.stringify(data),
    success: function (data) {
        $.each( data._items, function ( index, item ) {
            groupName.push( item.nimi );
            groupIds[item.nimi] = item._id;
            grpCountry[item.nimi] = item.kotimaa
        } );

        $(document).on('focusin','input.tahot',
                function() {
                    $('input.tahot').typeahead( { 
                    source:groupName,
                    updater: function(item) {
                        return this.$element.val().replace(/[^,]*$/,'')+item+' / ';
                    },
                    matcher: function (item) {
                      var tquery = extractor(this.query);
                      if(!tquery) return false;
                      return ~item.toLowerCase().indexOf(tquery.toLowerCase())
                    },
                    highlighter: function (item) {
                      var query = extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
                      return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
                        return '<strong>' + match + '</strong>'
                        })
                      }
                     }                                
                   );
                })
         .on('focusout','input.esittaja', function() {
             var res = $('input.esittaja').val();
             $(this).next('input.kotimaa').val(grpCountry[res]);
             });

        return JSON.stringify(data._items);
    },
    error: function (xhr, type, exception) {
        // Do your thing
    }
});


var teosName = new Array();
var teosKore = new Object();
var mainid = new Object();

var getTeos = $.ajax({
    method: 'GET',
    url: 'http://api.sirkusinfo.fi:5001/teos?where=group_id=='+ID,
    async: false,
    dataType: "json",
    contentType: "multipart/form-data; charset='utf-8'",
    data: getTeos,//JSON.stringify(data),
    success: function (data) {
        $.each( data._items, function ( index, item ) {
            teosName.push( item.nimi );
            teosKore[item.nimi] = item.koreografi
            mainid[item.nimi] = item.main_id
        } );

        $(document).on('focusin','input.teos',
                function() {
                    $('input.teos').typeahead( { 
                    source:teosName,
                    updater: function(item) {
                        return this.$element.val().replace(/[^,]*$/,'')+item;
                    },
                    matcher: function (item) {
                      var tquery = extractor(this.query);
                      if(!tquery) return false;
                      return ~item.toLowerCase().indexOf(tquery.toLowerCase())
                    },
                    highlighter: function (item) {
                      var query = extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
                      return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
                        return '<strong>' + match + '</strong>'
                        })
                      }
                     }                                
                   );
                })
         .on('focusout','input.teos', function() {
             var res = $('input.teos').val();
             $(this).next('input.koreografi').val(teosKore[res]);
             $(this).next('input.main_id').val(mainid[res]);
             });

        return JSON.stringify(data._items);
    },
    error: function (xhr, type, exception) {
        // Do your thing
    }
});


// Activate jQuery Validation
//$("form#esityksetGrid").validate({ submitHandler: viewModel.save });