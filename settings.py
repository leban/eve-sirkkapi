# -*- coding = utf-8 -*-

"""
    eve-demo settings
    ~~~~~~~~~~~~~~~~~

    Settings file for our little demo.

    PLEASE NOTE = We don't need to create the two collections in MongoDB.
    Actually, we don't even need to create the database = GET requests on an
    empty/non-existant DB will be served correctly ('200' OK with an empty
    collection); DELETE/PATCH will receive appropriate responses ('404' Not
    Found), and POST requests will create database and collections when needed.
    Keep in mind however that such an auto-managed database will most likely
    perform poorly since it lacks any sort of optimized index.

     =copyright = (c) 2016 by Nicola Iarocci.
     =license = BSD, see LICENSE for more details.
"""

import os



DEBUG = True

SQLALCHEMY_DATABASE_URI = "mysql+pymysql =//sirkusinfo =Cr2xvm8Pzs4KPDjA@localhost/sirkka_dev"

DOMAIN = {
    'users' : User._eve_schema['users'],
    'esiintyjat' : Esiintyja._eve_schema['esiintyjas'],
    'ryhma' : Ryhma._eve_schema['ryhmas'],
    'teos' : Esitys._eve_schema['esitys'],
    'teos_esitys' : Esitys_esitykset._eve_schema['esitys_esitys'],
    'teos_esiintyjat' : Esitys_esiintyjat._eve_schema['esitys_esiintyjat'],
    'esitys_venue' : Esitys_venue._eve_schema['esitys_venue'],
    'esitys_some' : Esitys_some._eve_schema['esitys_some'],
    'esitys_www' : Esitys_www._eve_schema['esitys_www'],
    },

RESOURCE_METHODS = ['GET'], #['GET', 'POST', 'DELETE']

ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']

ID_FIELD ='id'

